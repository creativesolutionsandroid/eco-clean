package com.cs.ecoclean.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.cs.ecoclean.R;
import com.cs.ecoclean.model.CarModel;

import java.util.ArrayList;

/**
 * Created by CS on 27-02-2017.
 */

public class SpinnerModelAdapter extends BaseAdapter implements Filterable {

    private ArrayList<CarModel> originalData = null;
    public static ArrayList<CarModel> filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();

    public SpinnerModelAdapter(Context context, ArrayList<CarModel> data) {
        this.filteredData = data;
        this.originalData = data;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return filteredData.size();
    }

    public Object getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        SpinnerBrandAdapter.ViewHolder holder;

        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_dropdown, null);

            // Creates a ViewHolder and store references to the two children views
            // we want to bind data to.
            holder = new SpinnerBrandAdapter.ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.dropdown_text);

            // Bind the data efficiently with the holder.

            convertView.setTag(holder);
        } else {
            // Get the ViewHolder back to get fast access to the TextView
            // and the ImageView.
            holder = (SpinnerBrandAdapter.ViewHolder) convertView.getTag();
        }

        // If weren't re-ordering this you could rely on what you set last time
        holder.text.setText(filteredData.get(position).getModelName());
        holder.text.setTextColor(Color.parseColor("#000000"));

        return convertView;
    }

    static class ViewHolder {
        TextView text;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<CarModel> list = originalData;

            int count = list.size();
            final ArrayList<CarModel> nlist = new ArrayList<CarModel>();

            String filterableString;

            for (int i = 0; i < count; i++) {
                CarModel cars = new CarModel();
                filterableString = list.get(i).getModelName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    cars.setModelName(filterableString);
                    nlist.add(cars);
                }
            }


            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<CarModel>) results.values;
            notifyDataSetChanged();
        }

    }
}
