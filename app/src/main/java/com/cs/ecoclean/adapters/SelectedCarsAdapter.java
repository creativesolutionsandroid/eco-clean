package com.cs.ecoclean.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.ecoclean.R;
import com.cs.ecoclean.model.SelectedCars;
import com.cs.ecoclean.model.UserCars;

import java.util.ArrayList;

/**
 * Created by CS on 20-11-2016.
 */
public class SelectedCarsAdapter extends BaseAdapter {


    public Context context;
    public LayoutInflater inflater;
    ArrayList<SelectedCars> carsList = new ArrayList<>();
    int pos;
    public static UserCars userCars;
    String id;
    String language;
    //public ImageLoader imageLoader;
    private int selectedPosition = 0;

    public SelectedCarsAdapter(Context context, ArrayList<SelectedCars> carsList, String language) {
        this.context = context;
        this.carsList = carsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        userCars = new UserCars();
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return carsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView carName, serviceType1, serviceType2;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.payment_cars_item, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
//            }


            holder.carName = (TextView) convertView
                    .findViewById(R.id.confirm_car_name);
            holder.serviceType1 = (TextView) convertView.findViewById(R.id.confirm_service_type);
            holder.serviceType2 = (TextView) convertView.findViewById(R.id.confirm_service_type1);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.carName.setText( carsList.get(position).getCarName());

        if(carsList.get(position).getWashIN().equals("0") && carsList.get(position).getPolishIN().equals("0")){
            holder.serviceType1.setVisibility(View.GONE);
        }else if(carsList.get(position).getWashIN().equals("0") && !carsList.get(position).getPolishIN().equals("0")){
            holder.serviceType1.setVisibility(View.VISIBLE);
            holder.serviceType1.setText("Polish - Interoir");
        }else if(!carsList.get(position).getWashIN().equals("0") && carsList.get(position).getPolishIN().equals("0")){
            holder.serviceType1.setVisibility(View.VISIBLE);
            holder.serviceType1.setText("Wash - Interior");
        }



        if(carsList.get(position).getWashEX().equals("0") && carsList.get(position).getPolishEX().equals("0")){
            holder.serviceType2.setVisibility(View.GONE);
        }else if(carsList.get(position).getWashEX().equals("0") && !carsList.get(position).getPolishEX().equals("0")){
            holder.serviceType2.setVisibility(View.VISIBLE);
            holder.serviceType2.setText("Polish - Exterior");
        }else if(!carsList.get(position).getWashEX().equals("0") && carsList.get(position).getPolishEX().equals("0")){
            holder.serviceType2.setVisibility(View.VISIBLE);
            holder.serviceType2.setText("Wash - Exterior");
        }


//        if(carsList.get(position).getWashIN().equals("0")) {
//            holder.serviceType1.setVisibility(View.GONE);
//        }else {
//            holder.serviceType1.setVisibility(View.VISIBLE);
//            holder.serviceType1.setText("Wash - Interior");
//        }
//
//        if(carsList.get(position).getPolishIN().equals("0")){
//            holder.serviceType1.setVisibility(View.GONE);
//        }else{
//            holder.serviceType1.setVisibility(View.VISIBLE);
//            holder.serviceType1.setText("Polish - Interoir");
//        }
//
//
//        if(carsList.get(position).getWashEX().equals("0")) {
//            holder.serviceType2.setVisibility(View.GONE);
//        }else {
//            holder.serviceType2.setVisibility(View.VISIBLE);
//            holder.serviceType2.setText("Wash - Exterior");
//        }
//
//        if(carsList.get(position).getPolishEX().equals("0")){
//            holder.serviceType2.setVisibility(View.GONE);
//        }else{
//            holder.serviceType2.setVisibility(View.VISIBLE);
//            holder.serviceType2.setText("Polish - Exterior");
//        }


        return convertView;
    }


}

