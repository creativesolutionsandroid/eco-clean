package com.cs.ecoclean.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.ecoclean.Constants;
import com.cs.ecoclean.JSONParser;
import com.cs.ecoclean.R;
import com.cs.ecoclean.model.UserCars;
import com.cs.ecoclean.widgets.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by CS on 17-11-2016.
 */
public class CarProfileAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<UserCars> carsList = new ArrayList<>();
    int pos;
    public static UserCars userCars;
    String id;
    String language;
    //public ImageLoader imageLoader;
    private int selectedPosition = 0;

    public CarProfileAdapter(Context context, ArrayList<UserCars> carsList, String language) {
        this.context = context;
        this.carsList = carsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        userCars = new UserCars();
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return carsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView brandName, modelName, carCount;
        ImageView deleteCar;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.car_profile_listitem, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
//            }


            holder.brandName = (TextView) convertView
                    .findViewById(R.id.brand_name);
            holder.modelName = (TextView) convertView.findViewById(R.id.carmodel);
            holder.carCount = (TextView) convertView.findViewById(R.id.car_count);
            holder.deleteCar = (ImageView) convertView.findViewById(R.id.delete_car_btn);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.carCount.setText( String.format("%02d", (position+1)));
//        if(language.equalsIgnoreCase("En")) {
        holder.brandName.setText(carsList.get(position).getBrandName());
        holder.modelName.setText(carsList.get(position).getModelName());

        holder.deleteCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                new DeleteCars().execute(Constants.DELETE_CAR_URL+carsList.get(position).getUcID());
            }
        });

        return convertView;
    }


    public class DeleteCars extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = ProgressDialog.show(context, "",
                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

//                            try{
////                                JSONArray ja = jo.getJSONArray("Success");
//
//
//                            }catch (JSONException je){
//                                je.printStackTrace();
//                                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
//                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            carsList.remove(selectedPosition);
            notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }


}
