package com.cs.ecoclean.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.ecoclean.Activities.RatingActivity;
import com.cs.ecoclean.R;
import com.cs.ecoclean.model.RatingDetails;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class CommentsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<RatingDetails> orderList = new ArrayList<>();
    String language;

    public CommentsAdapter(Context context, ArrayList<RatingDetails> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title;
        RelativeLayout grid_layout;
//        ArrayList<String> list = new ArrayList<>();
//        ArrayList<String> left_list = new ArrayList<>();
//        ArrayList<String> right_list = new ArrayList<>();
        //ImageButton plus;
//        LinearLayout additionalsLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.grid_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.title = (TextView) convertView.findViewById(R.id.reasons_text);
            holder.grid_layout = (RelativeLayout) convertView.findViewById(R.id.grid_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(orderList.get(position).getRemarks());
        if(holder.title.getLineCount()==2){
            holder.title.setTextSize(8);
        }
        holder.grid_layout.setBackgroundResource((R.drawable.grid_shape));
        holder.title.setTextColor(Color.parseColor("#000000"));

        if (RatingActivity.str != null && RatingActivity.str.size()>0) {
            for(int i=0; i<RatingActivity.str.size(); i++) {
                if (RatingActivity.str.get(i).equals(holder.title.getText().toString())) {
                    holder.grid_layout.setBackgroundResource((R.drawable.grid_shape_selected));
                    holder.title.setTextColor(Color.parseColor("#FFFFFF"));
                }
            }
        }

        holder.grid_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(RatingActivity.str==null) {
                    RatingActivity.str.add(orderList.get(position).getRemarks());
                }
                else {
                    if (!RatingActivity.str.contains(orderList.get(position).getRemarks())) {
                        RatingActivity.str.add(orderList.get(position).getRemarks());
                    }
                    else{
                        RatingActivity.str.remove(orderList.get(position).getRemarks());
                    }

                }
                if(RatingActivity.ratingId==null) {
                    RatingActivity.ratingId.add(orderList.get(position).getCommentId());
                }
                else {
                    if (!RatingActivity.ratingId.contains(orderList.get(position).getCommentId())) {
                        RatingActivity.ratingId.add(orderList.get(position).getCommentId());
                    }
                    else{
                        RatingActivity.ratingId.remove(orderList.get(position).getCommentId());
                    }
                }
                if (RatingActivity.str.contains("Other")){
                    RatingActivity.comment.setVisibility(View.VISIBLE);
                    if(orderList.get(position).getRating().equals("5")){
                        RatingActivity.comment.setHint("Write a thank you note");
                    }
                    else{
                        RatingActivity.comment.setHint("Write your valuable feedback");
                    }
                }
                else{
                    RatingActivity.comment.setVisibility(View.GONE);
                }
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public void updateResults(ArrayList<RatingDetails> results) {
        orderList = results;
        //Triggers the list update
        notifyDataSetChanged();
    }

}
