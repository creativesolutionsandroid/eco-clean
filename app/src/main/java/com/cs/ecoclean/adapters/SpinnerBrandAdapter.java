package com.cs.ecoclean.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.cs.ecoclean.R;
import com.cs.ecoclean.model.Cars;

import java.util.ArrayList;

public class SpinnerBrandAdapter extends BaseAdapter implements Filterable {

    private ArrayList<Cars>originalData = null;
    public static ArrayList<Cars>filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();

    public SpinnerBrandAdapter(Context context, ArrayList<Cars> data) {
        this.filteredData = data ;
        this.originalData = data ;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return filteredData.size();
    }

    public Object getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_dropdown, null);

            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.dropdown_text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // If weren't re-ordering this you could rely on what you set last time
        holder.text.setText(filteredData.get(position).getBrandName());
        holder.text.setTextColor(Color.parseColor("#000000"));

//        Log.i("TEST","filtered data "+filteredData.get(position).getBrandName());

        return convertView;
    }

    static class ViewHolder {
        TextView text;
    }
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<Cars> list = originalData;

            int count = list.size();
            final ArrayList<Cars> nlist = new ArrayList<Cars>();

            String filterableString ;
            for (int i = 0; i < count; i++) {
                Cars cars = new Cars();
                filterableString = list.get(i).getBrandName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    cars.setBrandName(filterableString);
                    nlist.add(cars);
//                    Log.i("TEST","filtered string "+nlist.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Cars>) results.values;
            notifyDataSetChanged();
        }

    }
}