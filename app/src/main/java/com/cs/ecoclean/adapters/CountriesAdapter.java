package com.cs.ecoclean.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.ecoclean.R;
import com.cs.ecoclean.model.Countries;

import java.util.ArrayList;

/**
 * Created by SKT on 29-12-2015.
 */
public class CountriesAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Countries> cityList = new ArrayList<>();

    public CountriesAdapter(Context context, ArrayList<Countries> cityList) {
        this.context = context;
        this.cityList = cityList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return cityList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView countryName;
        ImageView countryFlag;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.countries_list_item, null);

            holder.countryName = (TextView) convertView
                    .findViewById(R.id.country_name);
            holder.countryFlag = (ImageView) convertView
                    .findViewById(R.id.country_flag);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.countryName.setText(cityList.get(position).getCountryName());
        holder.countryFlag.setImageResource(cityList.get(position).getCountryFlag());

        return convertView;
    }

}
