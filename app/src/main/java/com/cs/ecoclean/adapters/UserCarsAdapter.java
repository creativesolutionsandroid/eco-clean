package com.cs.ecoclean.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.cs.ecoclean.R;
import com.cs.ecoclean.model.UserCars;

import java.util.ArrayList;

/**
 * Created by CS on 10-11-2016.
 */
public class UserCarsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<UserCars> carsList = new ArrayList<>();
    int pos;
    public static UserCars userCars;
    String id;
    String language;
    //public ImageLoader imageLoader;
    public static int selectedPosition = -1;

    public UserCarsAdapter(Context context, ArrayList<UserCars> carsList, String language) {
        this.context = context;
        this.carsList = carsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        userCars = new UserCars();
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return carsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        CheckBox carName;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.user_cars_item, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
//            }


            holder.carName = (CheckBox) convertView
                    .findViewById(R.id.user_car_name);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.carName.setTag(position);
//        if(language.equalsIgnoreCase("En")) {
        holder.carName.setText(carsList.get(position).getBrandName()+" "+ carsList.get(position).getModelName());
        holder.carName.setOnClickListener(onStateChangedListener(holder.carName, position));
//        }else{
//            holder.flatNo.setText(""+addressList.get(position).getHouseNo());
//            holder.landmark.setText(""+addressList.get(position).getLandmark());
//            holder.address.setText(""+addressList.get(position).getAddress());
//            holder.addressName.setText("" + addressList.get(position).getAddressName());
//        }
        if (position == selectedPosition) {
            holder.carName.setChecked(true);
        } else holder.carName.setChecked(false);

        return convertView;
    }


    private View.OnClickListener onStateChangedListener(final CheckBox checkBox, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    selectedPosition = position;
                    userCars = carsList.get(position);
                } else {
                    selectedPosition = -1;
                    userCars = null;
                }
                notifyDataSetChanged();
            }
        };
    }
}

