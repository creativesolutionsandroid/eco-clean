package com.cs.ecoclean.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.cs.ecoclean.R;
import com.cs.ecoclean.widgets.Utils;

import java.util.ArrayList;

/**
 * Created by admin on 14-08-2015.
 */
public class ProfilePhotosAdapter extends BaseAdapter {
    private Context mContext;

    ArrayList<String> result;
    public static String image_url;

    public ProfilePhotosAdapter(Context context, ArrayList<String> items) {
        mContext = context;
        result = items;

    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return result.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            // inflate the GridView item layout
            LayoutInflater inflater = LayoutInflater.from(mContext.getApplicationContext());
            convertView = inflater.inflate(R.layout.gridview_item, parent,
                    false);


            // initialize the view holder
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView
                    .findViewById(R.id.image_category);


            ViewGroup.LayoutParams layoutParams =  viewHolder.imageView.getLayoutParams();
            layoutParams.height = Utils.getScreenWidthProfile(mContext);
            layoutParams.width = Utils.getScreenWidthProfile(mContext);
            viewHolder.imageView.setLayoutParams(layoutParams);


//			viewHolder.flRoot = (FrameLayout) convertView.findViewById(R.id.flRoot);
            // viewHolder.tvTitle = (TextView) convertView
            // .findViewById(R.id.tvTitle);
            convertView.setTag(viewHolder);
        } else {
            // recycle the already inflated view
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Glide.with(mContext)
                .load(result.get(position))
                .placeholder(R.drawable.empty_photo)     // optional
                .error(R.drawable.empty_photo)
                .override(Utils.getScreenWidthProfile(mContext), Utils.getScreenWidthProfile(mContext)).centerCrop()
                .into(viewHolder.imageView);


        return convertView;
    }

    private static class ViewHolder {
        ImageView imageView;
        CardView mExploreCard;
        //FrameLayout flRoot;

    }




}