package com.cs.ecoclean.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.cs.ecoclean.R;
import com.cs.ecoclean.model.ServiceProvider;

import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;

/**
 * Created by CS on 07-11-2016.
 */
public class ServiceProviderAdapter1 extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<ServiceProvider> spList = new ArrayList<>();
    int pos;
    public static String selectedSP;
    public static float grandtotal, total;
    String id;
    String language , selectedDate, selectedTime;
    private int selectedPosition = -1;
    public static ServiceProvider sp;
    //public ImageLoader imageLoader;

    public ServiceProviderAdapter1(Context context, ArrayList<ServiceProvider> spList, String language, String selectedDate , String selectedTime) {
        this.context = context;
        this.spList = spList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.selectedDate = selectedDate;
        this.selectedTime = selectedTime;
        selectedSP = null;
        sp = new ServiceProvider();
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return spList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView spRating, spPrice, spPrice1, spDistance, spName;
        CheckBox spCheckBox;
        RatingBar mRatingBar;
        ImageView likeIcon, busyNow;
        View strike;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.sp_list_item_new, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
//            }


            holder.spRating = (TextView) convertView.findViewById(R.id.sp_rating);
//            holder.storeName = (TextView) convertView
//                    .findViewById(R.id.store_name);
            holder.spPrice = (TextView) convertView.findViewById(R.id.sp_price);
            holder.spPrice1 = (TextView) convertView.findViewById(R.id.sp_price1);
            holder.strike = (View) convertView.findViewById(R.id.strike);
            holder.spDistance = (TextView) convertView.findViewById(R.id.sp_distance);
            holder.spName = (TextView) convertView.findViewById(R.id.sp_name);
            holder.spCheckBox = (CheckBox) convertView.findViewById(R.id.sp_checkbox);
            holder.mRatingBar = (RatingBar) convertView.findViewById(R.id.ratingBar1);
            holder.likeIcon = (ImageView) convertView.findViewById(R.id.like_icon);
            holder.busyNow = (ImageView) convertView.findViewById(R.id.busy_now);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if(language.equalsIgnoreCase("En")) {
            holder.spName.setText(WordUtils.capitalizeFully(spList.get(position).getSpName()));
            holder.spRating.setText(spList.get(position).getRating());
//            holder.spPrice.setText(spList.get(position).getPrice()+"SR");
            holder.spDistance.setText(spList.get(position).getDistance()+"KM");


        holder.mRatingBar.setRating(Float.parseFloat(spList.get(position).getRating()));

        if(Float.parseFloat(spList.get(position).getRating()) >= 2.5){
            holder.likeIcon.setImageResource(R.drawable.sp_like);
        }else {
            holder.likeIcon.setImageResource(R.drawable.sp_dislike);
        }

        if((spList.get(position).getOnJourney().equals("0"))){
            holder.spCheckBox.setVisibility(View.VISIBLE);
            holder.busyNow.setVisibility(View.INVISIBLE);
        }
        else {
            holder.spCheckBox.setVisibility(View.INVISIBLE);
            holder.busyNow.setVisibility(View.VISIBLE);
        }

//        holder.spName.setOnClickListener(onStateChangedListener(holder.spCheckBox, position));

        holder.spCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.spCheckBox.isChecked()){
                    holder.spCheckBox.setChecked(true);
                    selectedPosition = position;
                    selectedSP = spList.get(position).getSpID();
                    sp = spList.get(position);

                    float price = Float.parseFloat(spList.get(position).getPrice());
                    float rushCharge = 0;
                    if(spList.get(position).getDisStart()!=null) {
                        if (isDiscountHours(position)) {
                            float p = price;
                            float disc = Float.parseFloat(spList.get(position).getDisPercent());
                            price = p - p * (disc / 100);
                        }
                    }
                    if(spList.get(position).getRushStart()!=null){
                        if(isRushHours(position)){
                            rushCharge = Float.parseFloat(spList.get(position).getRushCharge());
                            rushCharge = price * rushCharge;
                        }
                        else{
                            rushCharge = price;
                        }
                    }
                    else {
                        rushCharge = price;
                    }
                    total = Math.round(price);
                    grandtotal = Math.round(rushCharge);
                }else {
                    holder.spCheckBox.setChecked(false);
                    selectedSP = null;
                    sp = null;
                    selectedPosition = -1;
                }
//                onStateChangedListener(holder.spCheckBox, pos);
                notifyDataSetChanged();
            }
        });

        holder.spName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.spCheckBox.isChecked()){
                    holder.spCheckBox.setChecked(false);
                    selectedSP = null;
                    sp = null;
                    selectedPosition = -1;
                }else {
                    holder.spCheckBox.setChecked(true);
                    selectedPosition = position;
                    selectedSP = spList.get(position).getSpID();
                    sp = spList.get(position);

                    float price = Float.parseFloat(spList.get(position).getPrice());
                    float rushCharge = 0;
                    if(spList.get(position).getDisStart()!=null) {
                        if (isDiscountHours(position)) {
                            float p = price;
                            float disc = Float.parseFloat(spList.get(position).getDisPercent());
                            price = p - p * (disc / 100);
                        }
                    }
                    if(spList.get(position).getRushStart()!=null){
                        if(isRushHours(position)){
                            rushCharge = Float.parseFloat(spList.get(position).getRushCharge());
                            rushCharge = price * rushCharge;
                        }
                        else{
                            rushCharge = price;
                        }
                    }
                    else {
                        rushCharge = price;
                    }
                    total = Math.round(price);
                    grandtotal = Math.round(rushCharge);
                }
//                onStateChangedListener(holder.spCheckBox, pos);
                notifyDataSetChanged();
            }
        });

        if (position == selectedPosition) {
            holder.spCheckBox.setChecked(true);
        } else holder.spCheckBox.setChecked(false);

        float price = Float.parseFloat(spList.get(position).getPrice());
        float rushCharge = 0;
        if(spList.get(position).getRushStart()!=null){
            if(isRushHours(position)){
                rushCharge = Float.parseFloat(spList.get(position).getRushCharge());
                price = price * rushCharge;
            }
        }

        if(spList.get(position).getDisStart()!=null) {
            if (isDiscountHours(position)) {
                holder.spPrice.setText(Math.round(price)+"SR");
                holder.spPrice1.setVisibility(View.VISIBLE);
                holder.strike.setVisibility(View.VISIBLE);
                float p = price;
                float disc = Float.parseFloat(spList.get(position).getDisPercent());
                float finalPrice = p - p * (disc / 100);
//                DecimalFormat decim = new DecimalFormat("0.00");
                holder.spPrice1.setText("" + Math.round(finalPrice)+"SR");
            } else {
                holder.spPrice.setText(Math.round(price)+"SR");
                holder.spPrice1.setVisibility(View.INVISIBLE);
                holder.strike.setVisibility(View.INVISIBLE);
                holder.spPrice1.setText("");
            }
        }
        else{
            holder.spPrice.setText(Math.round(price)+"SR");
            holder.spPrice1.setVisibility(View.INVISIBLE);
            holder.strike.setVisibility(View.INVISIBLE);
            holder.spPrice1.setText("");
        }

        return convertView;
    }

    private View.OnClickListener onStateChangedListener(final CheckBox checkBox, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    selectedPosition = position;
                    selectedSP = spList.get(position).getSpID();
                    sp = spList.get(position);
                } else {
                    selectedSP = null;
                    sp = null;
                    selectedPosition = -1;
                }
                notifyDataSetChanged();
            }
        };
    }

    public boolean isDiscountHours(int pos1) {
        String expDateString = selectedDate + " " + selectedTime;
        String rushStart = spList.get(pos1).getDisStart();
        String rushEnd = spList.get(pos1).getDisEnd();

//        rushStart = rushStart.replace("T"," ");
//        rushEnd = rushEnd.replace("T"," ");
        String[] str = expDateString.split(" ");
        String[] dNow = str[0].split("-");
        String[] tNow = str[1].split(":");
        String[] sDate = rushStart.split("T");
        String[] eDate = rushEnd.split("T");
        String[] sDate1 = sDate[0].split("-");
        String[] eDate1 = eDate[0].split("-");
        String[] sTime = sDate[1].split(":");
        String[] eTime = eDate[1].split(":");

        int nowYear = Integer.parseInt(dNow[2]);
        int startYear = Integer.parseInt(sDate1[0]);
        int endYear = Integer.parseInt(eDate1[0]);

        int nowMonth = Integer.parseInt(dNow[1]);
        int startMonth = Integer.parseInt(sDate1[1]);
        int endMonth = Integer.parseInt(eDate1[1]);

        int nowDate = Integer.parseInt(dNow[0]);
        int startDate = Integer.parseInt(sDate1[2]);
        int endDate = Integer.parseInt(eDate1[2]);

        int nowHour = Integer.parseInt(tNow[0]);
        int startHour = Integer.parseInt(sTime[0]);
        int endHour = Integer.parseInt(eTime[0]);

        int nowMinute = Integer.parseInt(tNow[1]);
        int startMinute = Integer.parseInt(sTime[1]);
        int endMinute = Integer.parseInt(eTime[1]);

        if(str[2].equalsIgnoreCase("PM")){
            nowHour = nowHour+12;
        }

        boolean b = false;
        if(nowYear >= startYear && nowYear<=endYear ){
            if(nowYear==startYear && nowYear == endYear) {
                if (nowMonth >= startMonth && nowMonth <= endMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                }
                else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth != startMonth && nowMonth != endMonth){
                        b = true;
                    }
                }
            }
            else if(nowYear == startYear){
                if (nowMonth >= startMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth != startMonth && nowMonth != endMonth){
                        b = true;
                    }
                }
            }
            else if(nowYear == endYear){
                if (nowMonth <= endMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                }
            }
            else if(nowYear!=startYear && nowYear != endYear){
                b=true;
            }
        }
        return b;
    }

    public boolean isRushHours(int pos1) {

        String expDateString = selectedDate + " " + selectedTime;
        String rushStart = spList.get(pos1).getRushStart();
        String rushEnd = spList.get(pos1).getRushEnd();

        String[] str = expDateString.split(" ");
        String[] dNow = str[0].split("-");
        String[] tNow = str[1].split(":");
        String[] sDate = rushStart.split("T");
        String[] eDate = rushEnd.split("T");
        String[] sDate1 = sDate[0].split("-");
        String[] eDate1 = eDate[0].split("-");
        String[] sTime = sDate[1].split(":");
        String[] eTime = eDate[1].split(":");

        int nowYear = Integer.parseInt(dNow[2]);
        int startYear = Integer.parseInt(sDate1[0]);
        int endYear = Integer.parseInt(eDate1[0]);

        int nowMonth = Integer.parseInt(dNow[1]);
        int startMonth = Integer.parseInt(sDate1[1]);
        int endMonth = Integer.parseInt(eDate1[1]);

        int nowDate = Integer.parseInt(dNow[0]);
        int startDate = Integer.parseInt(sDate1[2]);
        int endDate = Integer.parseInt(eDate1[2]);

        int nowHour = Integer.parseInt(tNow[0]);
        int startHour = Integer.parseInt(sTime[0]);
        int endHour = Integer.parseInt(eTime[0]);

        int nowMinute = Integer.parseInt(tNow[1]);
        int startMinute = Integer.parseInt(sTime[1]);
        int endMinute = Integer.parseInt(eTime[1]);

        if(str[2].equalsIgnoreCase("PM")){
            nowHour = nowHour+12;
        }

        boolean b = false;
        if(nowYear >= startYear && nowYear<=endYear ){
            if(nowYear==startYear && nowYear == endYear) {
                if (nowMonth >= startMonth && nowMonth <= endMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth != startMonth && nowMonth != endMonth){
                        b = true;
                    }
                }
            }
            else if(nowYear == startYear){
                if (nowMonth >= startMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth != startMonth && nowMonth != endMonth){
                        b = true;
                    }
                }
            }
            else if(nowYear == endYear){
                if (nowMonth <= endMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                }
            }
            else if(nowYear!=startYear && nowYear != endYear){
                b=true;
            }
        }
        return b;
    }
}

