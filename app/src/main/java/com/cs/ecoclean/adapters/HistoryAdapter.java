package com.cs.ecoclean.adapters;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.ecoclean.R;
import com.cs.ecoclean.model.CarDetails;
import com.cs.ecoclean.model.ToDo;

import org.apache.commons.lang3.text.WordUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 02-12-2016.
 */
public class HistoryAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<ToDo> todoList = new ArrayList<>();
    int pos;
    ProfilePhotosAdapter mAdapter;
    String id;
    AlertDialog alertDialog;
    //public ImageLoader imageLoader;
    private int selectedPosition = 0;


    public HistoryAdapter(Context context, ArrayList<ToDo> todoList) {
        this.context = context;
        this.todoList = todoList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return todoList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView address, time, price, spName, paymentMode, status, orderNumber;
        LinearLayout carsLayout;
        GridView imagesGrid;
//        RelativeLayout takePicture;
//        LinearLayout imagesLayout;
//        HorizontalScrollView hsv;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.history_listitem_new, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
//            }


            holder.address = (TextView) convertView
                    .findViewById(R.id.history_address);
            holder.time = (TextView) convertView.findViewById(R.id.history_time);

            holder.price = (TextView) convertView.findViewById(R.id.total_price);
            holder.carsLayout = (LinearLayout) convertView.findViewById(R.id.history_cars_layout);
            holder.spName = (TextView) convertView.findViewById(R.id.sp_name);
            holder.paymentMode = (TextView) convertView.findViewById(R.id.payment_mode);
            holder.status = (TextView) convertView.findViewById(R.id.order_status);
            holder.orderNumber = (TextView)convertView.findViewById(R.id.order_number);
            holder.imagesGrid = (GridView) convertView.findViewById(R.id.images_grid);
//            holder.takePicture = (RelativeLayout) convertView.findViewById(R.id.take_picture);
//            holder.imagesLayout = (LinearLayout) convertView.findViewById(R.id.image_layout);
//            holder.hsv = (HorizontalScrollView) convertView.findViewById(R.id.hs1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        mAdapter = new ProfilePhotosAdapter(context, todoList.get(position).getImagesList());
        holder.imagesGrid.setAdapter(mAdapter);

        holder.imagesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                int layout = R.layout.bigimage_popup;
//                if(language.equalsIgnoreCase("En")){
//                    layout = R.layout.bigimage_popup;
//                }else if(language.equalsIgnoreCase("Ar")){
//                    layout = R.layout.bigimage_popup;
//                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);

                ImageView img = (ImageView) dialogView.findViewById(R.id.big_image);
                ImageView cance = (ImageView) dialogView.findViewById(R.id.rating_cancel);
                cance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                Glide.with(context).load(todoList.get(position).getImagesList().get(i)).into(img);



                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = context.getResources().getDimensionPixelSize(R.dimen.popup_width);
                lp.height = context.getResources().getDimensionPixelSize(R.dimen.popup_height);
                window.setAttributes(lp);
            }
        });

        holder.address.setText(""+ todoList.get(position).getAddress());
        holder.spName.setText(""+ WordUtils.capitalizeFully(todoList.get(position).getSpName()));
        float total = 0;
        try {
            total = Float.parseFloat(todoList.get(position).getGrandTotal()) + Float.parseFloat(todoList.get(position).getOldPenalty());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        holder.paymentMode.setText(""+todoList.get(position).getPaymentMode());
        if(todoList.get(position).getRequestStatus().equalsIgnoreCase("StartJourney")){
            holder.status.setText("On the way");
            holder.price.setText(""+ Math.round(total));
        }
        else if(todoList.get(position).getRequestStatus().equalsIgnoreCase("StartService")){
            holder.status.setText("On Service");
            holder.price.setText(""+ Math.round(total));
        }else if(todoList.get(position).getRequestStatus().equalsIgnoreCase("Cancel")){
            holder.price.setText(""+ Math.round(Float.parseFloat(todoList.get(position).getOldPenalty())));
            holder.status.setText("Cancelled");
        }else if(todoList.get(position).getRequestStatus().equalsIgnoreCase("Temporary")){
            holder.status.setText("Accepted");
            holder.price.setText(""+ Math.round(total));
        }else {
            holder.status.setText("" + todoList.get(position).getRequestStatus());
            holder.price.setText(""+ Math.round(total));
        }

        String[] orderNo = todoList.get(position).getOrderNumber().split("-");
        holder.orderNumber.setText(""+orderNo[1]);


        holder.carsLayout.removeAllViews();

        for(int j = 0; j< todoList.get(position).getCarsList().size(); j++) {
            LinearLayout.LayoutParams paramsnew = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            paramsnew.setMargins(0,4,0,4);
            ArrayList<CarDetails> carDetailsArrayList = todoList.get(position).getCarsList().get(j).getCarDetailsList();
            View v = inflater.inflate(R.layout.history_item_cars, null);
            TextView carModel = (TextView) v.findViewById(R.id.car_model);
            TextView serviceType1 = (TextView) v.findViewById(R.id.service_type1);
            TextView serviceType2 = (TextView) v.findViewById(R.id.service_type2);
            TextView carCount = (TextView) v.findViewById(R.id.car_count);
            carCount.setText( String.format("%02d", (j+1)));
            for (int i = 0; i < carDetailsArrayList.size(); i++) {


                if (carDetailsArrayList.size() == 1) {
                    serviceType2.setVisibility(View.GONE);
                }
                if (i == 0) {
                    if (carDetailsArrayList.get(i).getBrandName().equals("null")) {
                        carModel.setText(carDetailsArrayList.get(i).getSize() + " Car");
                    } else {
                        carModel.setText(carDetailsArrayList.get(i).getBrandName() + " " + carDetailsArrayList.get(i).getModelName() + "(" + carDetailsArrayList.get(i).getSize() + ")");
                    }
                    serviceType1.setText(carDetailsArrayList.get(i).getsType() + " - " + carDetailsArrayList.get(i).getsSubType());
                }
                if (i == 1) {
                    serviceType2.setVisibility(View.VISIBLE);
//                holder.carName.setText(carDetailsArrayList.get(i).getBrandName() + " " + carDetailsArrayList.get(i).getModelName() + "(" + carDetailsArrayList.get(i).getSize() + ")");
                    serviceType2.setText(carDetailsArrayList.get(i).getsType() + " - " + carDetailsArrayList.get(i).getsSubType());
                }


            }
            v.setLayoutParams(paramsnew);
            holder.carsLayout.addView(v);
        }

//        holder.imagesLayout.removeAllViews();
//
//        if(todoList.get(position).getImagesList().size() == 0){
//            holder.hsv.setVisibility(View.GONE);
//        }else {
//            holder.hsv.setVisibility(View.VISIBLE);
//            for(int i = 0; i < todoList.get(position).getImagesList().size(); i++) {
//                LinearLayout.LayoutParams paramsnew = new LinearLayout.LayoutParams(holder.takePicture.getWidth(), LinearLayout.LayoutParams.MATCH_PARENT);
//                paramsnew.setMargins(2, 2, 2, 2);
//                ImageView imageView = new ImageView(context);
//                imageView.setId(i);
//                imageView.setPadding(2, 2, 2, 2);
//                imageView.setAdjustViewBounds(true);
//                imageView.setLayoutParams(paramsnew);
//                imageView.setScaleType(ImageView.ScaleType.MATRIX);
//                Glide.with(context).load(todoList.get(position).getImagesList().get(i)).placeholder(R.drawable.cam_small).into(imageView);
//                holder.imagesLayout.addView(imageView);
//            }
//
//        }

        String expDate = todoList.get(position).getExpDate();
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date expDateTime = null;
        String date = null;
        Date today = Calendar.getInstance().getTime();
        try {
            expDateTime = formatter.parse(expDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(expDateTime!= null) {
            expDate = timeFormat.format(expDateTime);
            date = dateFormat.format(expDateTime);
        }



        holder.time.setText(date+"\n"+expDate);

        return convertView;
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(context, perm));
    }

}

