package com.cs.ecoclean.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.ecoclean.R;
import com.cs.ecoclean.model.CarDetails;
import com.cs.ecoclean.model.HistoryCars;

import java.util.ArrayList;

/**
 * Created by CS on 28-11-2016.
 */
public class CarsAdapter extends BaseAdapter {


    public Context context;
    public LayoutInflater inflater;
    ArrayList<HistoryCars> carsList = new ArrayList<>();
    int pos;
    String id;
    String language;
    //public ImageLoader imageLoader;
    private int selectedPosition = 0;

    public CarsAdapter(Context context, ArrayList<HistoryCars> carsList, String language) {
        this.context = context;
        this.carsList = carsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return carsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView carName, serviceType1, serviceType2, carCount;
        ImageView deletecar;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.notif_caritem, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
//            }
            holder.carCount = (TextView) convertView.findViewById(R.id.car_count);
            holder.carName = (TextView) convertView
                    .findViewById(R.id.car_model);
            holder.serviceType1 = (TextView) convertView.findViewById(R.id.service_type1);
            holder.serviceType2 = (TextView) convertView.findViewById(R.id.service_type2);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.carCount.setText( String.format("%02d", (position+1)));
        ArrayList<CarDetails> carDetailsArrayList = carsList.get(position).getCarDetailsList();
        if(carDetailsArrayList.size()==1){
            holder.serviceType2.setVisibility(View.GONE);
        }
        for(int i=0; i< carDetailsArrayList.size();i++) {
            if(i==0) {
                if(carDetailsArrayList.get(i).getBrandName().equals("null")){
                    holder.carName.setText(carDetailsArrayList.get(i).getSize()+" Car");
                }else {
                    holder.carName.setText(carDetailsArrayList.get(i).getBrandName() + " " + carDetailsArrayList.get(i).getModelName() + "(" + carDetailsArrayList.get(i).getSize() + ")");
                }
                holder.serviceType1.setText(carDetailsArrayList.get(i).getsType()+" - "+carDetailsArrayList.get(i).getsSubType());
            }
            if(i == 1){
                holder.serviceType2.setVisibility(View.VISIBLE);
//                holder.carName.setText(carDetailsArrayList.get(i).getBrandName() + " " + carDetailsArrayList.get(i).getModelName() + "(" + carDetailsArrayList.get(i).getSize() + ")");
                holder.serviceType2.setText(carDetailsArrayList.get(i).getsType()+" - "+carDetailsArrayList.get(i).getsSubType());
            }
        }
//
//        if(carsList.get(position).getWashIN().equals("0") && carsList.get(position).getPolishIN().equals("0")){
//            holder.serviceType1.setVisibility(View.GONE);
//        }else if(carsList.get(position).getWashIN().equals("0") && !carsList.get(position).getPolishIN().equals("0")){
//            holder.serviceType1.setVisibility(View.VISIBLE);
//            holder.serviceType1.setText("Polish - Interoir");
//        }else if(!carsList.get(position).getWashIN().equals("0") && carsList.get(position).getPolishIN().equals("0")){
//            holder.serviceType1.setVisibility(View.VISIBLE);
//            holder.serviceType1.setText("Wash - Interior");
//        }
//
//
//
//        if(carsList.get(position).getWashEX().equals("0") && carsList.get(position).getPolishEX().equals("0")){
//            holder.serviceType2.setVisibility(View.GONE);
//        }else if(carsList.get(position).getWashEX().equals("0") && !carsList.get(position).getPolishEX().equals("0")){
//            holder.serviceType2.setVisibility(View.VISIBLE);
//            holder.serviceType2.setText("Polish - Exterior");
//        }else if(!carsList.get(position).getWashEX().equals("0") && carsList.get(position).getPolishEX().equals("0")){
//            holder.serviceType2.setVisibility(View.VISIBLE);
//            holder.serviceType2.setText("Wash - Exterior");
//        }


        return convertView;
    }


}

