package com.cs.ecoclean.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class Rating implements Serializable {

    String spId,reqId,userId, givenrating;
    ArrayList<RatingDetails> ratingDetails;

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGivenrating() {
        return givenrating;
    }

    public void setGivenrating(String givenrating) {
        this.givenrating = givenrating;
    }

    public ArrayList<RatingDetails> getRatingDetails() {
        return ratingDetails;
    }

    public void setRatingDetails(ArrayList<RatingDetails> ratingDetails) {
        this.ratingDetails = ratingDetails;
    }
}
