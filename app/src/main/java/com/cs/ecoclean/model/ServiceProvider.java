package com.cs.ecoclean.model;

import org.json.JSONArray;

import java.util.Comparator;

/**
 * Created by CS on 07-11-2016.
 */
public class ServiceProvider {

    String spID, spName, spDesc, spAddress, storeLat, storeLong, spLat, spLong, distance, rating, phone, price, startTime, endTime, isShift, StartTime2, EndTime2,isClose, OnJourney;
    JSONArray spPrices;
    String rushId, rushCityId, rushCharge, rushType, rushStart, rushEnd, rushChargeValue, grandTotal, oldPenalty, disId, disStart, disEnd, disPercent, disPrice;

    public String getSpID() {
        return spID;
    }

    public void setSpID(String spID) {
        this.spID = spID;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public String getSpDesc() {
        return spDesc;
    }

    public void setSpDesc(String spDesc) {
        this.spDesc = spDesc;
    }

    public String getSpAddress() {
        return spAddress;
    }

    public void setSpAddress(String spAddress) {
        this.spAddress = spAddress;
    }

    public String getStoreLat() {
        return storeLat;
    }

    public void setStoreLat(String storeLat) {
        this.storeLat = storeLat;
    }

    public String getStoreLong() {
        return storeLong;
    }

    public void setStoreLong(String storeLong) {
        this.storeLong = storeLong;
    }

    public String getSpLat() {
        return spLat;
    }

    public String getDisId() {
        return disId;
    }

    public void setDisId(String disId) {
        this.disId = disId;
    }

    public String getDisStart() {
        return disStart;
    }

    public void setDisStart(String disStart) {
        this.disStart = disStart;
    }

    public String getDisEnd() {
        return disEnd;
    }

    public void setDisEnd(String disEnd) {
        this.disEnd = disEnd;
    }

    public String getDisPrice() {
        return disPrice;
    }

    public void setDisPrice(String disPrice) {
        this.disPrice = disPrice;
    }

    public String getDisPercent() {
        return disPercent;
    }

    public void setDisPercent(String disPercent) {
        this.disPercent = disPercent;
    }

    public void setSpLat(String spLat) {
        this.spLat = spLat;
    }

    public String getSpLong() {
        return spLong;
    }

    public void setSpLong(String spLong) {
        this.spLong = spLong;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public JSONArray getSpPrices() {
        return spPrices;
    }

    public void setSpPrices(JSONArray spPrices) {
        this.spPrices = spPrices;
    }

    public String getIsShift() {
        return isShift;
    }

    public void setIsShift(String isShift) {
        this.isShift = isShift;
    }

    public String getStartTime2() {
        return StartTime2;
    }

    public void setStartTime2(String startTime2) {
        StartTime2 = startTime2;
    }

    public String getEndTime2() {
        return EndTime2;
    }

    public void setEndTime2(String endTime2) {
        EndTime2 = endTime2;
    }

    public String getIsClose() {
        return isClose;
    }

    public void setIsClose(String isClose) {
        this.isClose = isClose;
    }

    public String getOnJourney() {
        return OnJourney;
    }

    public void setOnJourney(String onJourney) {
        OnJourney = onJourney;
    }

    public String getRushId() {
        return rushId;
    }

    public void setRushId(String rushId) {
        this.rushId = rushId;
    }

    public String getRushCityId() {
        return rushCityId;
    }

    public void setRushCityId(String rushCityId) {
        this.rushCityId = rushCityId;
    }

    public String getRushCharge() {
        return rushCharge;
    }

    public void setRushCharge(String rushCharge) {
        this.rushCharge = rushCharge;
    }

    public String getRushType() {
        return rushType;
    }

    public void setRushType(String rushType) {
        this.rushType = rushType;
    }

    public String getRushStart() {
        return rushStart;
    }

    public void setRushStart(String rushStart) {
        this.rushStart = rushStart;
    }

    public String getRushEnd() {
        return rushEnd;
    }

    public void setRushEnd(String rushEnd) {
        this.rushEnd = rushEnd;
    }

    public String getRushChargeValue() {
        return rushChargeValue;
    }

    public void setRushChargeValue(String rushChargeValue) {
        this.rushChargeValue = rushChargeValue;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getOldPenalty() {
        return oldPenalty;
    }

    public void setOldPenalty(String oldPenalty) {
        this.oldPenalty = oldPenalty;
    }

    /*Comparator for sorting the list by roll no*/
    public static Comparator<ServiceProvider> distanceSort = new Comparator<ServiceProvider>() {

        public int compare(ServiceProvider s1, ServiceProvider s2) {

            float rollno1 = Float.parseFloat(s1.getDistance());
            float rollno2 = Float.parseFloat(s2.getDistance());

	   /*For ascending order*/
            return Float.compare(rollno1,rollno2);

	   /*For descending order*/
            //rollno2-rollno1;
        }};

    /*Comparator for sorting the list by roll no*/
    public static Comparator<ServiceProvider> ratingSort = new Comparator<ServiceProvider>() {

        public int compare(ServiceProvider s1, ServiceProvider s2) {

            float rollno1 = Float.parseFloat(s1.getRating());
            float rollno2 = Float.parseFloat(s2.getRating());

	   /*For ascending order*/
            return Float.compare(rollno2,rollno1);

	   /*For descending order*/
            //rollno2-rollno1;
        }};


    /*Comparator for sorting the list by roll no*/
    public static Comparator<ServiceProvider> priceSort = new Comparator<ServiceProvider>() {

        public int compare(ServiceProvider s1, ServiceProvider s2) {

            float rollno1 = Float.parseFloat(s1.getPrice());
            float rollno2 = Float.parseFloat(s2.getPrice());

	   /*For ascending order*/
            return Float.compare(rollno1,rollno2);

	   /*For descending order*/
            //rollno2-rollno1;
        }};
}
