package com.cs.ecoclean.model;

import java.util.ArrayList;

/**
 * Created by CS on 02-12-2016.
 */
public class HistoryCars {
    ArrayList<CarDetails> carDetailsList;

    public ArrayList<CarDetails> getCarDetailsList() {
        return carDetailsList;
    }

    public void setCarDetailsList(ArrayList<CarDetails> carDetailsList) {
        this.carDetailsList = carDetailsList;
    }
}
