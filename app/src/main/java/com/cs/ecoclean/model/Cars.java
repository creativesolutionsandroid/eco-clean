package com.cs.ecoclean.model;

import java.util.ArrayList;

/**
 * Created by CS on 09-11-2016.
 */
public class Cars {
    String brandName, brandId,filtered;
    ArrayList<CarModel> modelList;


    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public ArrayList<CarModel> getModelList() {
        return modelList;
    }

    public void setModelList(ArrayList<CarModel> modelList) {
        this.modelList = modelList;
    }

    public String toString()
    {
        return brandName;
    }

    public String getFiltered() {
        return filtered;
    }

    public void setFiltered(String filtered) {
        this.filtered = filtered;
    }
}
