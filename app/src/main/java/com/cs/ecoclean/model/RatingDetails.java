package com.cs.ecoclean.model;

import java.io.Serializable;

/**
 * Created by CS on 28-02-2017.
 */

public class RatingDetails implements Serializable {

    String commentId,remarks, Rating;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }
}
