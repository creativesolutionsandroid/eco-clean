package com.cs.ecoclean.model;

import java.util.ArrayList;

/**
 * Created by CS on 24-11-2016.
 */
public class ToDo {

    String reqId, address, expDate, agreementPrice, requestType, fullName, mobile, latitude, longitude, totalPrice, paymentMode, requestStatus, spName, spNumber, orderNumber, RushChargeValue, GrandTotal, OldPenalty;

    ArrayList<HistoryCars> carsList;

    ArrayList<String> imagesList;

    public ArrayList<HistoryCars> getCarsList() {
        return carsList;
    }

    public void setCarsList(ArrayList<HistoryCars> carsList) {
        this.carsList = carsList;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getAgreementPrice() {
        return agreementPrice;
    }

    public void setAgreementPrice(String agreementPrice) {
        this.agreementPrice = agreementPrice;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public String getSpNumber() {
        return spNumber;
    }

    public void setSpNumber(String spNumber) {
        this.spNumber = spNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public ArrayList<String> getImagesList() {
        return imagesList;
    }

    public void setImagesList(ArrayList<String> imagesList) {
        this.imagesList = imagesList;
    }

    public String getRushChargeValue() {
        return RushChargeValue;
    }

    public void setRushChargeValue(String rushChargeValue) {
        RushChargeValue = rushChargeValue;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getOldPenalty() {
        return OldPenalty;
    }

    public void setOldPenalty(String oldPenalty) {
        OldPenalty = oldPenalty;
    }
}
