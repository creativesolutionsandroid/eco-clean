package com.cs.ecoclean.model;

/**
 * Created by CS on 18-11-2016.
 */
public class SelectedCars {

    String washIN, washEX, polishIN, polishEX, size, carName, carId;

    public String getWashIN() {
        return washIN;
    }

    public void setWashIN(String washIN) {
        this.washIN = washIN;
    }

    public String getWashEX() {
        return washEX;
    }

    public void setWashEX(String washEX) {
        this.washEX = washEX;
    }

    public String getPolishIN() {
        return polishIN;
    }

    public void setPolishIN(String polishIN) {
        this.polishIN = polishIN;
    }

    public String getPolishEX() {
        return polishEX;
    }

    public void setPolishEX(String polishEX) {
        this.polishEX = polishEX;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }
}
