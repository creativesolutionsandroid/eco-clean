package com.cs.ecoclean.model;

/**
 * Created by CS on 28-11-2016.
 */
public class CarDetails {
    String size, sType, sSubType, brandName, modelName, price;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getsType() {
        return sType;
    }

    public void setsType(String sType) {
        this.sType = sType;
    }

    public String getsSubType() {
        return sSubType;
    }

    public void setsSubType(String sSubType) {
        this.sSubType = sSubType;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
