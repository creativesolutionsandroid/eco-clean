package com.cs.ecoclean;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.ecoclean.Activities.EditProfileActivity;
import com.cs.ecoclean.Activities.MapsActivity;
import com.cs.ecoclean.Activities.OrderDetails;
import com.cs.ecoclean.Activities.OrderHistory;
import com.cs.ecoclean.Activities.RatingActivity;
import com.cs.ecoclean.Activities.SplashActivity;
import com.cs.ecoclean.adapters.AddedCarsAdapter;
import com.cs.ecoclean.adapters.CarProfileAdapter;
import com.cs.ecoclean.adapters.DrawerListAdapter;
import com.cs.ecoclean.adapters.SelectedCarsAdapter;
import com.cs.ecoclean.adapters.ServiceProviderAdapter;
import com.cs.ecoclean.adapters.SpinnerBrandAdapter;
import com.cs.ecoclean.adapters.SpinnerModelAdapter;
import com.cs.ecoclean.adapters.UserCarsAdapter;
import com.cs.ecoclean.model.CarModel;
import com.cs.ecoclean.model.Cars;
import com.cs.ecoclean.model.Rating;
import com.cs.ecoclean.model.RatingDetails;
import com.cs.ecoclean.model.SPMarkers;
import com.cs.ecoclean.model.SelectedCars;
import com.cs.ecoclean.model.ServiceProvider;
import com.cs.ecoclean.model.UserCars;
import com.cs.ecoclean.widgets.NetworkUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.cs.ecoclean.adapters.ServiceProviderAdapter.sp;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener {
    private static final int PLACE_PICKER_REQUEST = 1;
    LatLngBounds BOUNDS_MOUNTAIN_VIEW;
    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";

    private Button button;
    private int hour;
    private int minute;

    private int mYear, mMonth, mDay, mHour, mMinute;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    String selectedDate, selectedTime, time1;
    String format;
    boolean isToday;

    String address = "";
    Double lat, longi;
    GoogleMap mMap;
    String userId;

    private DrawerLayout mDrawerLayout;
    DrawerListAdapter adapter;
    private String[] mSidemenuTitles;
    private int[] mSidemenuIcons;
    private ListView mDrawerList;
    //    TextView langAr, langEng;
    private LinearLayout mDrawerLinear;
    boolean doubleBackToExitPressedOnce = false;
    boolean ordered = false;
    String secs;
    private CountDownTimer countDownTimer;

    ImageView menuBar;
    CheckBox smallCarSelector, bigCarSelector;
    LinearLayout smallCarLayout, bigCarLayout;
    LinearLayout datePicker, timePicker;
    LinearLayout fromProfile, bySize, sizeInnerLayout;
    RelativeLayout profileInnerLayout;
    LinearLayout chooseLocation;
    ListView profileListview, addedCarsListview;
    TextView profileBtn, sizeBtn, editProfile;
    TextView doneBtn, addCarBtn, dateField, timeField;
    TextView selAddr1, selAddr2;
    CheckBox washInterior, washExterior, polishInterior, polishExterior;
    String wIN, wEX, pIN, pEX, size = "0";
    String SHARED_PREFS_FILE = "selectedcars";
    String TASKS = "cars";
    Gson gson = new Gson();


    JSONArray SRDetailsArray = new JSONArray();
    String paymentMode= "3";
    String sServiceType1, sServiceType2;
    boolean isFromProfile, isBySize = true, isServiceSelected;
    ArrayList<ServiceProvider> spList = new ArrayList<>();
    ArrayList<Cars> carsList = new ArrayList<>();
    ArrayList<UserCars> userCarsList = new ArrayList<>();
    private ArrayList<SelectedCars> selectedCars = new ArrayList<>();
    private ArrayList<SelectedCars> selectedCars1 = new ArrayList<>();
    ArrayList<SPMarkers> markersList = new ArrayList<>();
    ServiceProviderAdapter mAdapter;
    UserCarsAdapter userCarsAdapter;
    AddedCarsAdapter addedCarsAdapter;
    CarProfileAdapter carProfileAdapter;
    float totalPriceFloat;
    ArrayList<Rating> ratingsArray = new ArrayList<>();

    AlertDialog alertDialog, alertDialog1, alertDialog2, carDialog, carProfileDialog, customDialog;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefEditor;

    SharedPreferences gcmPrefs;
    SharedPreferences.Editor gcmPrefEditor;

    Set<SelectedCars> set_cars = new HashSet<SelectedCars>();
    ArrayList<SelectedCars> array_cars = new ArrayList<>();

    Toolbar toolbar;
    MarkerOptions markerOptions;
    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    String reqId, order_status, regId;
    int orderCnt;

    SpinnerBrandAdapter spinnerBrandAdapter;
    SpinnerModelAdapter spinnerModelAdapter;
    ArrayList<CarModel> cmList = new ArrayList<>();
    int brandPos = 0, modelPos = 0;
    int brandSpinnerCount = 0, modelSpinnerCount = 0;
    ArrayList<Rating> ratingsArrayList = new ArrayList<>();
    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel,rateTitle;
    boolean rushHours = false;
    boolean discountNow = false;
    LinearLayout schedule_layout;
    ImageView schedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_location_layout);
//        setCurrentTimeOnView();

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        gcmPrefs = getSharedPreferences("GCM_PREFS", Context.MODE_PRIVATE);
        gcmPrefEditor  = gcmPrefs.edit();

        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefEditor  = orderPrefs.edit();
        reqId = orderPrefs.getString("req_id", "");
        orderCnt = orderPrefs.getInt("order_cnt", 1);
        order_status = orderPrefs.getString("order_status", "new");

        userId = userPrefs.getString("userId", null);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        mDrawerList = (ListView) findViewById(R.id.drawer_list);
            mSidemenuTitles = new String[] { "Profile", "Add Car", "Order History", "Track Service",
                    "Share Our App","Rate Our App" };
        mSidemenuIcons = new int[5];
//        mTitle = mSidemenuTitles1[0];
//        mTitle = "Oregano Pi";
        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                mSidemenuTitles, mSidemenuIcons, "En");
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLinear = (LinearLayout) findViewById(R.id.left_drawer);

        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        washInterior = (CheckBox) findViewById(R.id.wash_interior);
        washInterior.setChecked(false);
        washExterior = (CheckBox) findViewById(R.id.wash_exterior);
        washExterior.setChecked(false);
        polishInterior = (CheckBox) findViewById(R.id.polish_interior);
        polishInterior.setChecked(false);
        polishExterior = (CheckBox) findViewById(R.id.polish_exterior);
        polishExterior.setChecked(false);
//        carDoneBtn = (TextView) findViewById(R.id.car_done_btn);
        fromProfile = (LinearLayout) findViewById(R.id.from_profile_layout);
        bySize = (LinearLayout) findViewById(R.id.bysize_layout);
        profileBtn = (TextView) findViewById(R.id.profile_radio_btn);
        sizeBtn = (TextView) findViewById(R.id.size_radio_btn);
        smallCarSelector = (CheckBox) findViewById(R.id.small_cars_selector);
        bigCarSelector = (CheckBox) findViewById(R.id.big_cars_selector);
        smallCarLayout = (LinearLayout) findViewById(R.id.small_car_layout);
        bigCarLayout = (LinearLayout) findViewById(R.id.big_car_layout);
        addCarBtn = (TextView) findViewById(R.id.add_car_btn);
        editProfile = (TextView) findViewById(R.id.edit_profile_btn);
        doneBtn = (TextView) findViewById(R.id.done_btn);
        datePicker = (LinearLayout) findViewById(R.id.date_picker);
        timePicker = (LinearLayout) findViewById(R.id.time_picker);
        dateField = (TextView) findViewById(R.id.date_field);
        timeField = (TextView) findViewById(R.id.time_field);
        selAddr1 = (TextView) findViewById(R.id.sel_address);
        selAddr2 = (TextView) findViewById(R.id.sel_address1);
        sizeInnerLayout = (LinearLayout) findViewById(R.id.size_inner_layout);
        profileInnerLayout = (RelativeLayout) findViewById(R.id.profile_inner_layout);
        profileListview = (ListView) findViewById(R.id.profile_listview);
        addedCarsListview = (ListView) findViewById(R.id.added_cars_listview);
        chooseLocation = (LinearLayout) findViewById(R.id.choose_location_layout);
        menuBar = (ImageView) findViewById(R.id.side_menubar);
        ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBar1);
        cancel = (TextView) findViewById(R.id.skip_rating);
        rateTitle = (TextView) findViewById(R.id.dialog_title);
        rate_layout = (RelativeLayout) findViewById(R.id.rate_layout);
        schedule = (ImageView) findViewById(R.id.schedule);
        schedule_layout = (LinearLayout) findViewById(R.id.schedule_layout);
        rate_layout.setVisibility(View.GONE);

        userCarsAdapter = new UserCarsAdapter(MainActivity.this, userCarsList, "En");
        profileListview.setAdapter(userCarsAdapter);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(MainActivity.this);

//        Gson gson = new Gson();
        try {
            String jsonCars = userPrefs.getString("cars","");
            Type type = new TypeToken<ArrayList<SelectedCars>>(){}.getType();
            Log.i("TAG","try");
            selectedCars = gson.fromJson(jsonCars, type);
            if(selectedCars == null){
                selectedCars = new ArrayList<>();
            }
        } catch (JsonSyntaxException e) {
            Log.i("TAG","catch");
            selectedCars =null;
            e.printStackTrace();
        }
        if(selectedCars!=null && selectedCars.size()>0){
        addedCarsAdapter = new AddedCarsAdapter(MainActivity.this, selectedCars, "En");
        addedCarsListview.setAdapter(addedCarsAdapter);
            addedCarsAdapter.notifyDataSetChanged();
        }
//

//        Log.i("TAG","cars size "+selectedCars.size());
//        if(selectedCars1.size()>0){
//            selectedCars = selectedCars1;
//        }

        fromProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromProfile = true;
                isBySize = false;
                fromProfile.setBackgroundResource(R.drawable.oval_shape);
                sizeInnerLayout.setVisibility(View.GONE);
                profileInnerLayout.setVisibility(View.VISIBLE);
                if(userCarsList.size()==0){
                    new GetUserCars().execute(Constants.GET_USERCARS_URL+userId);
                }
                bySize.setBackgroundResource(R.drawable.shape3);
                profileBtn.setTextColor(Color.parseColor("#FFFFFF"));
                sizeBtn.setTextColor(Color.parseColor("#000000"));
            }
        });

        menuBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);

                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

        bySize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromProfile = false;
                isBySize = true;
                sizeInnerLayout.setVisibility(View.VISIBLE);
                profileInnerLayout.setVisibility(View.GONE);
                fromProfile.setBackgroundResource(R.drawable.shape3);
                bySize.setBackgroundResource(R.drawable.oval_shape);
                profileBtn.setTextColor(Color.parseColor("#000000"));
                sizeBtn.setTextColor(Color.parseColor("#FFFFFF"));
            }
        });

        smallCarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!smallCarSelector.isChecked()){
                    smallCarSelector.setChecked(true);
                    bigCarSelector.setChecked(false);
                    size = "1";
                }else{
                    size = "1";
                }
            }
        });

        bigCarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!bigCarSelector.isChecked()){
                    smallCarSelector.setChecked(false);
                    bigCarSelector.setChecked(true);
                    size = "2";
                }else{
                    size = "2";
                }
            }
        });


        schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                schedule_layout.setVisibility(View.VISIBLE);
                showDatePicker();
            }
        });
//        smallCarSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    if(bigCarSelector.isChecked()){
//                        bigCarSelector.setChecked(false);
//                    }
//                    size = "1";
//                }
//            }
//        });
//
//        bigCarSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    if(smallCarSelector.isChecked()){
//                        smallCarSelector.setChecked(false);
//                    }
//                    size = "2";
//                }
//            }
//        });


//        washInterior.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isFromProfile){
//                    if(UserCarsAdapter.userCars == null){
//                        Toast.makeText(MainActivity.this, "Please select a car", Toast.LENGTH_SHORT).show();
//                    }else{
//                        if(washInterior.isChecked()){
//                            washInterior.setChecked(false);
//                            sServiceType1 = null;
//                        }else {
//                            washInterior.setChecked(true);
//                            if (polishInterior.isChecked()) {
//                                polishInterior.setChecked(false);
//                            }
//                            sServiceType1 = "Car Wash - Interior";
//                        }
//                    }
//                }else if(isBySize){
//                    if(size.equals("0")){
//                        Toast.makeText(MainActivity.this, "Please select car size", Toast.LENGTH_SHORT).show();
//                    }else{
//                        if(washInterior.isChecked()){
//                            washInterior.setChecked(false);
//                            sServiceType1 = null;
//                        }else {
//                            washInterior.setChecked(true);
//                            if (polishInterior.isChecked()) {
//                                polishInterior.setChecked(false);
//                            }
//                            sServiceType1 = "Car Wash - Interior";
//                        }
//                    }
//                }
//            }
//        });
//
//        washExterior.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isFromProfile){
//                    if(UserCarsAdapter.userCars == null){
//                        Toast.makeText(MainActivity.this, "Please select a car", Toast.LENGTH_SHORT).show();
//                    }else{
//                        if(washExterior.isChecked()){
//                            washExterior.setChecked(false);
//                            sServiceType2 = null;
//                        }else {
//                            washExterior.setChecked(true);
//                            if (polishExterior.isChecked()) {
//                                polishExterior.setChecked(false);
//                            }
//                            sServiceType2 = "Car Wash - Exterior";
//                        }
//                    }
//                }else if(isBySize){
//                    if(size.equals("0")){
//                        Toast.makeText(MainActivity.this, "Please select car size", Toast.LENGTH_SHORT).show();
//                    }else{
//                        if(washExterior.isChecked()){
//                            washExterior.setChecked(false);
//                            sServiceType2 = null;
//                        }else {
//                            washExterior.setChecked(true);
//                            if (polishExterior.isChecked()) {
//                                polishExterior.setChecked(false);
//                            }
//                            sServiceType2 = "Car Wash - Exterior";
//                        }
//                    }
//                }
//            }
//        });


        washInterior.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {

                    if (polishInterior.isChecked()) {
                        polishInterior.setChecked(false);
                    }
                    sServiceType1 = "Car Wash - Interior";
                }else {
                    sServiceType1 = null;
                }
            }
        });

        washExterior.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {

                    if (polishExterior.isChecked()) {
                        polishExterior.setChecked(false);
                    }
                    sServiceType2 = "Car Wash - Exterior";
                }else {
                    sServiceType2 = null;
                }
            }
        });

        polishInterior.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    if (washInterior.isChecked()) {
                        washInterior.setChecked(false);
                    }
                    sServiceType1 = "Polishing - Interior";
                }else {
                    sServiceType1 = null;
                }
            }
        });

        polishExterior.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    if (washExterior.isChecked()) {
                        washExterior.setChecked(false);
                    }
                    sServiceType2 = "Polishing - Exterior";
                }else {
                    sServiceType2 = null;
                }
            }
        });

        addCarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isServiceSelected = false;
                if(washInterior.isChecked()){
                    isServiceSelected = true;
                    wIN = "1";
                }else {
                    wIN = "0";
                }

                if(washExterior.isChecked()){
                    isServiceSelected = true;
                    wEX = "2";
                }else {
                    wEX = "0";
                }

                if(polishInterior.isChecked()){
                    isServiceSelected = true;
                    pIN = "1";
                }else {
                    pIN = "0";
                }

                if(polishExterior.isChecked()){
                    isServiceSelected = true;
                    pEX = "2";
                }else {
                    pEX = "0";
                }

                if(isFromProfile){
                    if(UserCarsAdapter.selectedPosition == -1){
                        Toast.makeText(MainActivity.this, "Please select a car", Toast.LENGTH_SHORT).show();
                    }else if(!isServiceSelected){
                        Toast.makeText(MainActivity.this, "Please select service type", Toast.LENGTH_SHORT).show();
                    }else{
                        SelectedCars sc = new SelectedCars();
                        sc.setCarName(UserCarsAdapter.userCars.getBrandName()+" "+UserCarsAdapter.userCars.getModelName());
                        sc.setCarId(UserCarsAdapter.userCars.getUcID());
                        sc.setSize(UserCarsAdapter.userCars.getSize());
                        sc.setWashIN(wIN);
                        sc.setWashEX(wEX);
                        sc.setPolishIN(pIN);
                        sc.setPolishEX(pEX);
                        selectedCars.add(sc);

//                        TinyDB tinydb = new TinyDB(getApplicationContext());
//                        tinydb.putListObject("cars", selectedCars);
                        addedCarsAdapter = new AddedCarsAdapter(MainActivity.this, selectedCars, "En");
                        addedCarsListview.setAdapter(addedCarsAdapter);
                        Gson gson = new Gson();
                        String jsonCars = gson.toJson(selectedCars);
                        userPrefEditor.putString("cars",jsonCars);
                        userPrefEditor.commit();

                        washInterior.setChecked(false);
                        washExterior.setChecked(false);
                        polishInterior.setChecked(false);
                        polishExterior.setChecked(false);
                        smallCarSelector.setChecked(false);
                        bigCarSelector.setChecked(false);
                        isServiceSelected = false;
                        UserCarsAdapter.userCars = null;
                        UserCarsAdapter.selectedPosition = -1;
                        userCarsAdapter.notifyDataSetChanged();
                    }
                }else {
                    if (size.equals("0")) {
                        Toast.makeText(MainActivity.this, "Please select car size", Toast.LENGTH_SHORT).show();
                    }else if(!isServiceSelected){
                        Toast.makeText(MainActivity.this, "Please select service type", Toast.LENGTH_SHORT).show();
                    } else {
                        SelectedCars sc = new SelectedCars();
                        if(size.equals("1")){
                            sc.setCarName("Small Car");
                        }else if(size.equals("2")){
                            sc.setCarName("Big Car");
                        }
                        sc.setCarId("");
                        sc.setSize(size);
                        sc.setWashIN(wIN);
                        sc.setWashEX(wEX);
                        sc.setPolishIN(pIN);
                        sc.setPolishEX(pEX);
                        selectedCars.add(sc);

//                        TinyDB tinydb = new TinyDB(getApplicationContext());
//                        tinydb.putListObject("cars", selectedCars);
                        addedCarsAdapter = new AddedCarsAdapter(MainActivity.this, selectedCars, "En");
                        addedCarsListview.setAdapter(addedCarsAdapter);
                        Gson gson = new Gson();
                        String jsonCars = gson.toJson(selectedCars);
                        userPrefEditor.putString("cars",jsonCars);
                        userPrefEditor.commit();

                        washInterior.setChecked(false);
                        washExterior.setChecked(false);
                        polishInterior.setChecked(false);
                        polishExterior.setChecked(false);
                        size = "0";
                        isServiceSelected = false;
                        smallCarSelector.setChecked(false);
                        bigCarSelector.setChecked(false);
                        UserCarsAdapter.userCars = null;
                        UserCarsAdapter.selectedPosition = -1;
                        userCarsAdapter.notifyDataSetChanged();
                    }
                }

                addedCarsAdapter.notifyDataSetChanged();
                System.out.println(""+selectedCars.toString());
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carProfileDialog();
            }
        });

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        dateField.setText(mDay + "," + MONTHS[mMonth] + "," + mYear);
        if(mMonth<9) {
            selectedDate = mDay + "-0" + (mMonth + 1) + "-" + mYear;
        }
        else{
            selectedDate = mDay + "-" + (mMonth + 1) + "-" + mYear;
        }
        final int hour = c.get(Calendar.HOUR);
        final int minute = c.get(Calendar.MINUTE);
        final int seconds = c.get(Calendar.SECOND);
        int am_pm = c.get(Calendar.AM_PM);
        String am_pm_string = "";
        if(am_pm == 0){
            am_pm_string = "AM";
        }else if(am_pm == 1){
            am_pm_string = "PM";
        }
        selectedTime = String.format("%02d:%02d", hour, minute) + " " + am_pm_string;
        time1 = String.format("%02d:%02d:%02d", hour, minute, seconds) + " " + am_pm_string;
        timeField.setText(selectedTime);
        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(selectedCars.size()>0) {
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    if(year == mYear && monthOfYear == mMonth && dayOfMonth == mDay){
                                        isToday = true;
                                    }else {
                                        isToday = false;
                                    }
                                    mYear = year;
                                    mDay = dayOfMonth;
                                    mMonth = monthOfYear;

                                    final int hour = c.get(Calendar.HOUR_OF_DAY);
                                    final int minute = c.get(Calendar.MINUTE);

                                    com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                                            MainActivity.this,
                                            hour,
                                            minute,
                                            false
                                    );
                                    tpd.setThemeDark(true);
                                    tpd.vibrate(false);
                                    tpd.setAccentColor(Color.parseColor("#76C8FC"));
                                    tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogInterface dialogInterface) {
                                            Log.d("TimePicker", "Dialog was cancelled");
                                        }
                                    });
                                    if (isToday) {
                                        tpd.setMinTime(hour + 1, minute, 00);
                                    }else {
                                        tpd.setMinTime(0,0,0);
                                    }
                                    tpd.show(getFragmentManager(), "Timepickerdialog");



                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                    long max = TimeUnit.DAYS.toMillis(90);
                    datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
                    datePickerDialog.setTitle("Select Date");
                    datePickerDialog.show();
            }
        });

        timePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(selectedCars.size()>0) {
                    final Calendar c = Calendar.getInstance();
//                mYear = c.get(Calendar.YEAR);
//                mMonth = c.get(Calendar.MONTH);
//                mDay = c.get(Calendar.DAY_OF_MONTH);
                    final int hour = c.get(Calendar.HOUR_OF_DAY);
                    final int minute = c.get(Calendar.MINUTE);

                    com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                            MainActivity.this,
                            hour,
                            minute,
                            false
                    );
                    tpd.setThemeDark(true);
                    tpd.vibrate(false);
//                    tpd.dismissOnPause(dismissTime.isChecked());
//                    tpd.enableSeconds(enableSeconds.isChecked());
//                    if (modeCustomAccentTime.isChecked()) {
                        tpd.setAccentColor(Color.parseColor("#76C8FC"));
//                    }
//                    if (titleTime.isChecked()) {
//                        tpd.setTitle("TimePicker Title");
//                    }
//                    if (limitTimes.isChecked()) {
//                        tpd.setTimeInterval(2, 5, 10);
//                    }
                    tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            Log.d("TimePicker", "Dialog was cancelled");
                        }
                    });
                    if (isToday) {
                        tpd.setMinTime(hour + 1, minute, 00);
                    }else {
                        tpd.setMinTime(0,0,0);
                    }
                    tpd.show(getFragmentManager(), "Timepickerdialog");

//                }else {
//                    Toast.makeText(MainActivity.this, "Please add a car before selecting time", Toast.LENGTH_SHORT).show();
//                }
            }
        });


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parent = new JSONObject();
                isServiceSelected = false;

                if(washInterior.isChecked()){
                    isServiceSelected = true;
                    wIN = "1";
                }else {
                    wIN = "0";
                }

                if(washExterior.isChecked()){
                    isServiceSelected = true;
                    wEX = "2";
                }else {
                    wEX = "0";
                }

                if(polishInterior.isChecked()){
                    isServiceSelected = true;
                    pIN = "1";
                }else {
                    pIN = "0";
                    pIN = "0";
                }

                if(polishExterior.isChecked()){
                    isServiceSelected = true;
                    pEX = "2";
                }else {
                    pEX = "0";
                }

//                if(isFromProfile){
//                    if(UserCarsAdapter.userCars == null){
//                        Toast.makeText(MainActivity.this, "Please select a car", Toast.LENGTH_SHORT).show();
//                    }else if(!isServiceSelected){
//                        Toast.makeText(MainActivity.this, "Please select service type", Toast.LENGTH_SHORT).show();
//                    }else if(selectedDate == null){
//                        Toast.makeText(MainActivity.this, "Please select date", Toast.LENGTH_SHORT).show();
//                    }else if(selectedTime == null){
//                        Toast.makeText(MainActivity.this, "Please select time", Toast.LENGTH_SHORT).show();
//                    }else{
//                        try {
//                            JSONArray mainItem = new JSONArray();
//
//
//                            JSONObject mainObj = new JSONObject();
//                            mainObj.put("Latitude", lat);
//                            mainObj.put("Longitude", longi);
//                            mainObj.put("Win", wIN);
//                            mainObj.put("Wex", wEX);
//                            mainObj.put("Pin", pIN);
//                            mainObj.put("Pex", pEX);
//                            mainObj.put("sz", UserCarsAdapter.userCars.getSize());
//                            mainItem.put(mainObj);
//
//
//                            parent.put("Request", mainItem);
//                            Log.i("TAG", parent.toString());
//                        } catch (JSONException je) {
//
//                        }
//                        new GetServiceProviders().execute(parent.toString());
//                    }
//                }else {
                    if(selectedCars.size()==0){
//                        Toast.makeText(MainActivity.this, "Please add car ", Toast.LENGTH_SHORT).show();
                        isServiceSelected = false;

                        if(washInterior.isChecked()){
                            isServiceSelected = true;
                            wIN = "1";
                        }else {
                            wIN = "0";
                        }

                        if(washExterior.isChecked()){
                            isServiceSelected = true;
                            wEX = "2";
                        }else {
                            wEX = "0";
                        }

                        if(polishInterior.isChecked()){
                            isServiceSelected = true;
                            pIN = "1";
                        }else {
                            pIN = "0";
                        }

                        if(polishExterior.isChecked()){
                            isServiceSelected = true;
                            pEX = "2";
                        }else {
                            pEX = "0";
                        }

                        if(isFromProfile){
                            if(UserCarsAdapter.selectedPosition == -1){
                                Toast.makeText(MainActivity.this, "Please select a car", Toast.LENGTH_SHORT).show();
                            }else if(!isServiceSelected){
                                Toast.makeText(MainActivity.this, "Please select service type", Toast.LENGTH_SHORT).show();
                            }else{
                                SelectedCars sc = new SelectedCars();
                                sc.setCarName(UserCarsAdapter.userCars.getBrandName()+" "+UserCarsAdapter.userCars.getModelName());
                                sc.setCarId(UserCarsAdapter.userCars.getUcID());
                                sc.setSize(UserCarsAdapter.userCars.getSize());
                                sc.setWashIN(wIN);
                                sc.setWashEX(wEX);
                                sc.setPolishIN(pIN);
                                sc.setPolishEX(pEX);
                                selectedCars.add(sc);

//                                TinyDB tinydb = new TinyDB(getApplicationContext());
//                                tinydb.putListObject("cars", selectedCars);
                                addedCarsAdapter = new AddedCarsAdapter(MainActivity.this, selectedCars, "En");
                                addedCarsListview.setAdapter(addedCarsAdapter);
                                Gson gson = new Gson();
                                String jsonCars = gson.toJson(selectedCars);
                                userPrefEditor.putString("cars",jsonCars);
                                userPrefEditor.commit();

                                washInterior.setChecked(false);
                                washExterior.setChecked(false);
                                polishInterior.setChecked(false);
                                polishExterior.setChecked(false);
                                smallCarSelector.setChecked(false);
                                bigCarSelector.setChecked(false);
                                isServiceSelected = false;
                                UserCarsAdapter.userCars = null;
                                UserCarsAdapter.selectedPosition = -1;
                                userCarsAdapter.notifyDataSetChanged();

                                Date today = Calendar.getInstance().getTime();
                                // (2) create a date "formatter" (the date format we want)
                                SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa", Locale.US);
                                SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.US);

                                String expDateString = selectedDate+" "+time1;
                                Date newDate = null;
                                try {
                                    newDate = timeFormat.parse(expDateString);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    FirebaseCrash.report(e);
                                }
//
                                if(newDate!= null) {
                                    expDateString = formatter.format(newDate);
                                }

                                JSONArray mainItem = new JSONArray();
                                for(int i = 0; i< selectedCars.size(); i++) {
                                    try {

                                        JSONObject mainObj = new JSONObject();
                                        mainObj.put("Latitude", lat);
                                        mainObj.put("Longitude", longi);
                                        mainObj.put("Win", selectedCars.get(i).getWashIN());
                                        mainObj.put("Wex", selectedCars.get(i).getWashEX());
                                        mainObj.put("Pin", selectedCars.get(i).getPolishIN());
                                        mainObj.put("Pex", selectedCars.get(i).getPolishEX());
                                        mainObj.put("sz", selectedCars.get(i).getSize());
                                        mainObj.put("ExpectedDate", expDateString);
                                        mainItem.put(mainObj);

                                    } catch (JSONException je) {
                                        je.printStackTrace();
                                        FirebaseCrash.report(je);
                                    }
                                }
                                try {
                                    parent.put("Request", mainItem);
                                }catch (JSONException je){
                                    je.printStackTrace();
                                    FirebaseCrash.report(je);
                                }
                                Log.i("TAG", parent.toString());
                                orderPrefEditor.putString("sel_date", selectedDate);
                                orderPrefEditor.putString("sel_time", selectedTime);
                                orderPrefEditor.commit();
                                new GetServiceProviders().execute(parent.toString());
                            }
                        }else {
                            if (size.equals("0")) {
                                Toast.makeText(MainActivity.this, "Please select car size", Toast.LENGTH_SHORT).show();
                            }else if(!isServiceSelected){
                                Toast.makeText(MainActivity.this, "Please select service type", Toast.LENGTH_SHORT).show();
                            } else {
                                SelectedCars sc = new SelectedCars();
                                if(size.equals("1")){
                                    sc.setCarName("Small Car");
                                }else if(size.equals("2")){
                                    sc.setCarName("Big Car");
                                }
                                sc.setCarId("");
                                sc.setSize(size);
                                sc.setWashIN(wIN);
                                sc.setWashEX(wEX);
                                sc.setPolishIN(pIN);
                                sc.setPolishEX(pEX);
                                selectedCars.add(sc);

//                                TinyDB tinydb = new TinyDB(getApplicationContext());
//                                tinydb.putListObject("cars", selectedCars);
                                addedCarsAdapter = new AddedCarsAdapter(MainActivity.this, selectedCars, "En");
                                addedCarsListview.setAdapter(addedCarsAdapter);
                                Gson gson = new Gson();
                                String jsonCars = gson.toJson(selectedCars);
                                userPrefEditor.putString("cars",jsonCars);
                                userPrefEditor.commit();

                                washInterior.setChecked(false);
                                washExterior.setChecked(false);
                                polishInterior.setChecked(false);
                                polishExterior.setChecked(false);
                                size = "0";
                                isServiceSelected = false;
                                smallCarSelector.setChecked(false);
                                bigCarSelector.setChecked(false);
                                UserCarsAdapter.userCars = null;
                                UserCarsAdapter.selectedPosition = -1;
                                userCarsAdapter.notifyDataSetChanged();

                                Date today = Calendar.getInstance().getTime();

                                // (2) create a date "formatter" (the date format we want)
                                SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa", Locale.US);
                                SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.US);

                                String expDateString = selectedDate+" "+time1;
                                Date newDate = null;
                                try {
                                    newDate = timeFormat.parse(expDateString);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    FirebaseCrash.report(e);
                                }
//
                                if(newDate!= null) {
                                    expDateString = formatter.format(newDate);
                                }

                                JSONArray mainItem = new JSONArray();
                                for(int i = 0; i< selectedCars.size(); i++) {
                                    try {

                                        JSONObject mainObj = new JSONObject();
                                        mainObj.put("Latitude", lat);
                                        mainObj.put("Longitude", longi);
                                        mainObj.put("Win", selectedCars.get(i).getWashIN());
                                        mainObj.put("Wex", selectedCars.get(i).getWashEX());
                                        mainObj.put("Pin", selectedCars.get(i).getPolishIN());
                                        mainObj.put("Pex", selectedCars.get(i).getPolishEX());
                                        mainObj.put("sz", selectedCars.get(i).getSize());
                                        mainObj.put("ExpectedDate", expDateString);
                                        mainItem.put(mainObj);

                                    } catch (JSONException je) {
                                        je.printStackTrace();
                                    }
                                }
                                try {
                                    parent.put("Request", mainItem);
                                }catch (JSONException je){
                                    je.printStackTrace();
                                    FirebaseCrash.report(je);
                                }
                                Log.i("TAG", parent.toString());
                                orderPrefEditor.putString("sel_date", selectedDate);
                                orderPrefEditor.putString("sel_time", selectedTime);
                                orderPrefEditor.commit();
                                new GetServiceProviders().execute(parent.toString());
                            }
                        }
                        addedCarsAdapter.notifyDataSetChanged();

                    }
//                    else if(address.equalsIgnoreCase("")){
//                        Toast.makeText(MainActivity.this, "Please select your address", Toast.LENGTH_SHORT).show();
//                    }
//                    else if(selectedDate == null){
//                        Toast.makeText(MainActivity.this, "Please select date", Toast.LENGTH_SHORT).show();
//                    }else if(selectedTime == null){
//                        Toast.makeText(MainActivity.this, "Please select time", Toast.LENGTH_SHORT).show();
//                    }
                    else {

                        isServiceSelected = false;
                        if(washInterior.isChecked()){
                            isServiceSelected = true;
                            wIN = "1";
                        }else {
                            wIN = "0";
                        }

                        if(washExterior.isChecked()){
                            isServiceSelected = true;
                            wEX = "2";
                        }else {
                            wEX = "0";
                        }

                        if(polishInterior.isChecked()){
                            isServiceSelected = true;
                            pIN = "1";
                        }else {
                            pIN = "0";
                        }

                        if(polishExterior.isChecked()){
                            isServiceSelected = true;
                            pEX = "2";
                        }else {
                            pEX = "0";
                        }

                        if(isFromProfile){
                            if(UserCarsAdapter.selectedPosition == -1){
//                                Toast.makeText(MainActivity.this, "Please select a car", Toast.LENGTH_SHORT).show();
                            }else if(!isServiceSelected){
//                                Toast.makeText(MainActivity.this, "Please select service type", Toast.LENGTH_SHORT).show();
                            }else{
                                SelectedCars sc = new SelectedCars();
                                sc.setCarName(UserCarsAdapter.userCars.getBrandName()+" "+UserCarsAdapter.userCars.getModelName());
                                sc.setCarId(UserCarsAdapter.userCars.getUcID());
                                sc.setSize(UserCarsAdapter.userCars.getSize());
                                sc.setWashIN(wIN);
                                sc.setWashEX(wEX);
                                sc.setPolishIN(pIN);
                                sc.setPolishEX(pEX);
                                selectedCars.add(sc);

//                                TinyDB tinydb = new TinyDB(getApplicationContext());
//                                tinydb.putListObject("cars", selectedCars);
                                addedCarsAdapter = new AddedCarsAdapter(MainActivity.this, selectedCars, "En");
                                addedCarsListview.setAdapter(addedCarsAdapter);
                                Gson gson = new Gson();
                                String jsonCars = gson.toJson(selectedCars);
                                userPrefEditor.putString("cars",jsonCars);
                                userPrefEditor.commit();

                                washInterior.setChecked(false);
                                washExterior.setChecked(false);
                                polishInterior.setChecked(false);
                                polishExterior.setChecked(false);
                                smallCarSelector.setChecked(false);
                                bigCarSelector.setChecked(false);
                                isServiceSelected = false;
                                UserCarsAdapter.userCars = null;
                                UserCarsAdapter.selectedPosition = -1;
                                userCarsAdapter.notifyDataSetChanged();
                            }
                        }else {
                            if (size.equals("0")) {
//                                Toast.makeText(MainActivity.this, "Please select car size", Toast.LENGTH_SHORT).show();
                            }else if(!isServiceSelected){
//                                Toast.makeText(MainActivity.this, "Please select service type", Toast.LENGTH_SHORT).show();
                            } else {
                                SelectedCars sc = new SelectedCars();
                                if(size.equals("1")){
                                    sc.setCarName("Small Car");
                                }else if(size.equals("2")){
                                    sc.setCarName("Big Car");
                                }
                                sc.setCarId("");
                                sc.setSize(size);
                                sc.setWashIN(wIN);
                                sc.setWashEX(wEX);
                                sc.setPolishIN(pIN);
                                sc.setPolishEX(pEX);
                                selectedCars.add(sc);

//                                TinyDB tinydb = new TinyDB(getApplicationContext());
//                                tinydb.putListObject("cars", selectedCars);
                                addedCarsAdapter = new AddedCarsAdapter(MainActivity.this, selectedCars, "En");
                                addedCarsListview.setAdapter(addedCarsAdapter);
                                Gson gson = new Gson();
                                String jsonCars = gson.toJson(selectedCars);
                                userPrefEditor.putString("cars",jsonCars);
                                userPrefEditor.commit();

                                washInterior.setChecked(false);
                                washExterior.setChecked(false);
                                polishInterior.setChecked(false);
                                polishExterior.setChecked(false);
                                size = "0";
                                isServiceSelected = false;
                                smallCarSelector.setChecked(false);
                                bigCarSelector.setChecked(false);
                                UserCarsAdapter.userCars = null;
                                UserCarsAdapter.selectedPosition = -1;
                                userCarsAdapter.notifyDataSetChanged();
                            }
                        }

                        addedCarsAdapter.notifyDataSetChanged();

                        Date today = Calendar.getInstance().getTime();

                        // (2) create a date "formatter" (the date format we want)
                        SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa", Locale.US);
                        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss", Locale.US);

                        Log.i("TAG","selectedDate "+selectedDate);
                        String expDateString = selectedDate+" "+time1;
                        Date newDate = null;
                        try {
                            newDate = timeFormat.parse(expDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            FirebaseCrash.report(e);
                        }
//
                        if(newDate!= null) {
                            expDateString = formatter.format(newDate);
                        }

                        JSONArray mainItem = new JSONArray();
                        for(int i = 0; i< selectedCars.size(); i++) {
                            try {

                                JSONObject mainObj = new JSONObject();
                                mainObj.put("Latitude", lat);
                                mainObj.put("Longitude", longi);
                                mainObj.put("Win", selectedCars.get(i).getWashIN());
                                mainObj.put("Wex", selectedCars.get(i).getWashEX());
                                mainObj.put("Pin", selectedCars.get(i).getPolishIN());
                                mainObj.put("Pex", selectedCars.get(i).getPolishEX());
                                mainObj.put("sz", selectedCars.get(i).getSize());
                                mainObj.put("ExpectedDate", expDateString);
                                mainItem.put(mainObj);

                            } catch (JSONException je) {
                                je.printStackTrace();
                                FirebaseCrash.report(je);
                            }
                        }
                        try {
                            parent.put("Request", mainItem);
                        }catch (JSONException je){
                            je.printStackTrace();
                            FirebaseCrash.report(je);
                        }
                        Log.i("TAG", parent.toString());
                        orderPrefEditor.putString("sel_date", selectedDate);
                        orderPrefEditor.putString("sel_time", selectedTime);
                        orderPrefEditor.commit();
                        new GetServiceProviders().execute(parent.toString());
                    }
//                }


            }
        });
    }

//    @Override
//    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
//        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
//        String minuteString = minute < 10 ? "0"+minute : ""+minute;
//        String secondString = second < 10 ? "0"+second : ""+second;
//        String time = "You picked the following time: "+hourString+"h"+minuteString+"m"+secondString+"s";
//        timeTextView.setText(time);
//    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int selectedHour, int selectedMinute, int second) {
        if (selectedHour == 0) {
//
            selectedHour += 12;

            format = "AM";
        } else if (selectedHour == 12) {

            format = "PM";

        } else if (selectedHour > 12) {

            selectedHour -= 12;

            format = "PM";

        } else {

            format = "AM";
        }
        if(mMonth<9) {
            selectedDate = mDay + "-0" + (mMonth + 1) + "-" + mYear;
        }
        else{
            selectedDate = mDay + "-" + (mMonth + 1) + "-" + mYear;
        }
        selectedTime = String.format("%02d:%02d", selectedHour, selectedMinute) + " " + format;
        time1 = String.format("%02d:%02d:%02d", selectedHour, selectedMinute, second) + " " + format;
        timeField.setText(selectedTime);
        dateField.setText(selectedDate);

        if(alertDialog1 != null){
            alertDialog1.dismiss();
//                        showDialog1(secs, false, "");
        }

        if(alertDialog != null){
            alertDialog.dismiss();
//                        showDialog1(secs, false, "");
        }
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            view.setSelected(true);
            selectItem(position);
        }
    }


    public void selectItem(int position) {

        // update the main content by replacing fragments
        switch (position) {
            case 0:
                mDrawerLayout.closeDrawer(mDrawerLinear);
                Intent i = new Intent(MainActivity.this, EditProfileActivity.class);
                startActivity(i);
                break;
            case 1:
                mDrawerLayout.closeDrawer(mDrawerLinear);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new GetCars().execute(Constants.GET_CARS_URL);
                    }
                }, 150);
                break;
            case 2:
                mDrawerLayout.closeDrawer(mDrawerLinear);
                Intent intent = new Intent(MainActivity.this, OrderHistory.class);
                startActivity(intent);
                break;

            case 3:
                String reqId = orderPrefs.getString("order_id", "-1");
                if(reqId.equals("-1")){
                    Toast.makeText(MainActivity.this, "No requests found", Toast.LENGTH_SHORT).show();
                }else {
                    Intent trackIntent = new Intent(MainActivity.this, OrderDetails.class);
                    trackIntent.putExtra("req_id", reqId);
                    trackIntent.putExtra("from_history", false);
                    trackIntent.putExtra("screen", "home");
                    startActivity(trackIntent);
                }
                break;

            case 4:
                mDrawerLayout.closeDrawer(mDrawerLinear);


                new Handler().postDelayed(new Runnable() {

                    // Using handler with postDelayed called runnable run method

                    @Override
                    public void run() {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT,
                                "https://play.google.com/store/apps/details?id=com.cs.ecoclean&hl=en");
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                                "Check out ECO Clean App for your smartphone.");
                        startActivity(Intent.createChooser(intent, "Share"));
                    }
                }, 200);
                break;

            case 5:
                mDrawerLayout.closeDrawer(mDrawerLinear);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.ecoclean")));

                new Handler().postDelayed(new Runnable() {

                    // Using handler with postDelayed called runnable run method

                    @Override
                    public void run() {
//                        AppRater.app_launched(MainActivity.this);
                    }
                }, 200);
                break;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
//        Log.i("TAG","map ready");
//        Toast.makeText(getApplicationContext(),"Map ready",Toast.LENGTH_SHORT).show();
        mMap = googleMap;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }

        new GetSPMarkers().execute(Constants.GET_ALL_SPLIST);
        markerOptions = new MarkerOptions();



//        LatLng driver = new LatLng(driverLat, driverLong);
//        LatLng store = new LatLng(storeLat, storeLong);
//        LatLng user = new LatLng(userLat, userLong);
//
//        mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));
//        mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.store_marker)));
//        mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
    }


    public void getGPSCoordinates(){
        gps = new GPSTracker(MainActivity.this);
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
        try {
            mMap.setMyLocationEnabled(true);
        }catch (Exception npe){
            npe.printStackTrace();
            FirebaseCrash.report(npe);
        }
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
//                lat = 24.805712;
//                longi = 46.693132;
                getAddress(lat,longi);
                BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
                        new LatLng(lat, longi), new LatLng(lat, longi));
                chooseLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        if(selectedCars.size() > 0) {
                            new GetSPMarkers().execute(Constants.GET_ALL_SPLIST);
                            Intent i = new Intent(MainActivity.this, MapsActivity.class);
                            i.putExtra("markers_list", markersList);
                            startActivityForResult(i, PLACE_PICKER_REQUEST);

//                        }
//                        else{
//                            Toast.makeText(MainActivity.this, "Please add a car before selecting location", Toast.LENGTH_SHORT).show();
//                        }
                    }
                });

                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
//                        if (selectedCars.size() > 0) {
                            new GetSPMarkers().execute(Constants.GET_ALL_SPLIST);
                            Intent i = new Intent(MainActivity.this, MapsActivity.class);
                            i.putExtra("markers_list", markersList);
                            startActivityForResult(i, PLACE_PICKER_REQUEST);
//                        } else {
//                            Toast.makeText(MainActivity.this, "Please add a car before selecting location", Toast.LENGTH_SHORT).show();
//                        }
                    }
                });
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
                // Show the current location in Google Map
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                // Zoom in the Google Map
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(MainActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(MainActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    getGPSCoordinates();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

//            final Place place = PlacePicker.getPlace(this, data);
//            final CharSequence name = place.getName();
//            final CharSequence address = place.getAddress();
//            String attributions = (String) place.getAttributions();
//            if (attributions == null) {
//                attributions = "";
//            }

            if(data == null){

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                vert.setVisibility(View.GONE);
                no.setVisibility(View.GONE);
                yes.setText("Ok");
                desc.setText("Sorry! we couldn't detect your location. Please place the pin on your exact location.");

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
//
////                    if(language.equalsIgnoreCase("En")) {
//                // set title
//                alertDialogBuilder.setTitle("ECO Clean");
//
//                // set dialog message
//                alertDialogBuilder
//                        .setMessage("Sorry! we couldn't detect your location. Please place the pin on your exact location.")
//                        .setCancelable(false)
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
////                    }else if(language.equalsIgnoreCase("Ar")){
////                        // set title
////                        alertDialogBuilder.setTitle("د. كيف");
////
////                        // set dialog message
////                        alertDialogBuilder
////                                .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
////                                .setCancelable(false)
////                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                    public void onClick(DialogInterface dialog, int id) {
////                                        dialog.dismiss();
////                                    }
////                                });
////                    }
//
//                // create alert dialog
//                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                // show it
//                alertDialog.show();
//                Toast.makeText(AddressActivity.this, "Please select a address", Toast.LENGTH_SHORT).show();
            }else{
                address =  data.getStringExtra("address");
                lat =  data.getExtras().getDouble("lat");
                longi = data.getExtras().getDouble("longi");
                selAddr1.setVisibility(View.GONE);
                selAddr2.setText(address);

                Log.i("Location TAG", "outside" + lat + " " + longi);

                LatLng latLng = new LatLng(lat, longi);
                mMap.clear();

                for(int i = 0; i<markersList.size(); i++){
                    if(lat != null) {
                        markerOptions.position(new LatLng(markersList.get(i).getLat(), markersList.get(i).getLongi()))
                                .title(markersList.get(i).getSpName())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.sp_marker));
                        mMap.addMarker(markerOptions);
                    }
                }
                if(mMap != null) {
                    // Show the current location in Google Map
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, longi))
                            .title(address));
                    // Zoom in the Google Map
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));

                    if(alertDialog1 != null){
                        alertDialog1.dismiss();
//                        showDialog1(secs, false, "");
                    }

                    if(alertDialog != null){
                        alertDialog.dismiss();
//                        showDialog1(secs, false, "");
                    }
                }
            }

//            addressTxt.setText(name+", "+ address);
//            mAddress.setText(address);
//            mAttributions.setText(Html.fromHtml(attributions));

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
//        String reqId = orderPrefs.getString("order_id", "-1");
//        if(reqId.equals("-1") || order_status.equalsIgnoreCase("cancel") || order_status.equalsIgnoreCase("close")){
////            menu.getItem(0).setVisible(false);
//        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.alert_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
            View vert = (View) dialogView.findViewById(R.id.vert_line);

            no.setText("No");
            yes.setText("Yes");
            desc.setText("Do you want to Logout?");

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customDialog.dismiss();
                    userPrefEditor.clear();
                    userPrefEditor.commit();
                    Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(loginIntent);
                    finish();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customDialog.dismiss();
                }
            });

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth*0.85;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

            return true;
        }

//        if (id == R.id.action_track) {
//
//            String reqId = orderPrefs.getString("order_id", "-1");
//            if(reqId.equals("-1")){
//                Toast.makeText(MainActivity.this, "No requests found", Toast.LENGTH_SHORT).show();
//            }else {
////                if(!order_status.equalsIgnoreCase("cancel"))
//                Intent trackIntent = new Intent(MainActivity.this, OrderDetails.class);
//                trackIntent.putExtra("req_id", reqId);
//                trackIntent.putExtra("from_history", false);
//                trackIntent.putExtra("screen", "home");
//                startActivity(trackIntent);
//            }
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }



    public class GetServiceProviders extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            dialog = new MaterialDialog.Builder(MainActivity.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.SERVICE_PROVIDER_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                        FirebaseCrash.report(e);
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                    FirebaseCrash.report(e);
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            spList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONArray priceArray = new JSONArray();
                                for(int i = 0; i< ja.length(); i++){
                                    ServiceProvider sp = new ServiceProvider();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    sp.setSpID(jo1.getString("sp_SPID"));
                                    sp.setSpName(jo1.getString("SpName"));
                                    sp.setSpDesc(jo1.getString("SpDesc"));
                                    sp.setSpAddress(jo1.getString("SpAddress"));
                                    sp.setStoreLat(jo1.getString("StoreLatitude"));
                                    sp.setStoreLong(jo1.getString("StoreLongitude"));
                                    sp.setSpLat(jo1.getString("SpLatitude"));
                                    sp.setSpLong(jo1.getString("SpLongitude"));
                                    sp.setDistance(jo1.getString("Distance"));
                                    sp.setRating(jo1.getString("Points"));
                                    sp.setPhone(jo1.getString("Phone"));
                                    sp.setPrice(jo1.getString("TPrice"));
                                    sp.setSpPrices(jo1.getJSONArray("spPrice"));
                                    sp.setOnJourney(jo1.getString("OnJourney"));
                                    JSONArray ja1 = jo1.getJSONArray("SPWorkingDet");
                                    JSONObject jo2 = ja1.getJSONObject(0);
                                    sp.setStartTime(jo2.getString("StartTime"));
                                    sp.setEndTime(jo2.getString("EndTime"));
                                    sp.setIsShift(jo2.getString("IsInShifts"));
                                    sp.setStartTime2(jo2.getString("StartTime2"));
                                    sp.setEndTime2(jo2.getString("EndTime2"));
                                    sp.setIsClose(jo2.getString("IsClose"));

                                    int fCharge = 0;
                                    int pos = 0;
                                    try {
                                        JSONArray rushArray = jo1.getJSONArray("RushHoursDetails");
                                        for(int k =0; k<rushArray.length(); k++){
                                            JSONObject rushObj1 = rushArray.getJSONObject(k);
                                            int charge1 = rushObj1.getInt("RushCharge");
                                            if(charge1>fCharge){
                                                pos=k;
                                                fCharge = charge1;
                                            }
                                        }
                                        JSONObject rushObj = rushArray.getJSONObject(pos);
                                        sp.setRushId(rushObj.getString("ID"));
                                        sp.setRushCityId(rushObj.getString("CityId"));
                                        sp.setRushStart(rushObj.getString("StartFrom"));
                                        sp.setRushEnd(rushObj.getString("EndAt"));
                                        sp.setRushCharge(rushObj.getString("RushCharge"));
                                        sp.setRushType(rushObj.getString("ServiceType"));
                                        rushHours = true;
                                    } catch (JSONException e) {
                                        rushHours = false;
                                        e.printStackTrace();
                                    }

                                    int dcount = 0;
                                    int pos1 = 0;
                                    try {
                                        JSONArray rushArray = jo1.getJSONArray("Discounts");
                                        for(int k =0; k<rushArray.length(); k++){
                                            JSONObject rushObj1 = rushArray.getJSONObject(k);
                                            int charge1 = rushObj1.getInt("DiscountPercentage");
                                            if(charge1>dcount){
                                                pos1=k;
                                                dcount = charge1;
                                            }
                                        }
                                        JSONObject rushObj = rushArray.getJSONObject(pos1);
                                        sp.setDisId(rushObj.getString("id"));
                                        sp.setDisStart(rushObj.getString("StartDate"));
                                        sp.setDisEnd(rushObj.getString("EndDate"));
                                        sp.setDisPercent(rushObj.getString("DiscountPercentage"));
                                        discountNow = true;
                                    } catch (JSONException e) {
                                        discountNow = false;
                                        e.printStackTrace();
                                    }


                                    if(jo2.getString("IsClose").equals("false")) {
                                        if ((jo2.getString("IsInShifts").equals("true"))) {
                                            if (isWorkingTime(jo2.getString("StartTime"), jo2.getString("EndTime"))) {
                                                spList.add(sp);
                                            } else {
                                                if (isWorkingTime(jo2.getString("StartTime2"), jo2.getString("EndTime2"))) {
                                                    spList.add(sp);
                                                }
                                            }
                                        } else if ((jo2.getString("IsInShifts").equals("false"))) {
                                            if (isWorkingTime(jo2.getString("StartTime"), jo2.getString("EndTime"))) {
                                                spList.add(sp);
                                            }
                                        }
                                    }
                                }
                                orderPrefEditor.putString("sp_response", result);
                                orderPrefEditor.commit();
                                if(spList.size()>0) {
                                    Collections.sort(spList, ServiceProvider.distanceSort);
                                    showDialog();
                                }else{

                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                                    vert.setVisibility(View.GONE);
                                    no.setVisibility(View.GONE);
                                    yes.setText("Ok");
                                    desc.setText("No service providers in your area");

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth*0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
//                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                                    // set title
//                                    alertDialogBuilder.setTitle("ECO Clean");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage("No service providers in your area")
//                                            .setCancelable(false)
//                                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//
//
//                                    // create alert dialog
//                                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                    // show it
//                                    alertDialog.show();
                                }

                            }catch (JSONException je){
                                je.printStackTrace();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                vert.setVisibility(View.GONE);
                                no.setVisibility(View.GONE);
                                yes.setText("Ok");
                                desc.setText("No service providers in your area");

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);

//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("ECO Clean");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No service providers in your area")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//                                }else if(language.equalsIgnoreCase("Ar")){
//                                    // set title
//                                    alertDialogBuilder.setTitle("اوريجانو");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(result)
//                                            .setCancelable(false)
//                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }


                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }


            super.onPostExecute(result);

        }

    }




    public class GetCars extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            dialog = new MaterialDialog.Builder(MainActivity.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            carsList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for(int i = 0; i< ja.length(); i++){
                                    Cars cars = new Cars();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    ArrayList<CarModel> modelList = new ArrayList<>();
                                    cars.setBrandName(jo1.getString("BrandName"));
                                    cars.setBrandId(jo1.getString("BrandId"));
                                    JSONArray ja1 = jo1.getJSONArray("Model");
                                    for(int j = 0; j< ja1.length(); j++){
                                        CarModel cm = new CarModel();
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        cm.setModelName(jo2.getString("ModelName"));
                                        cm.setModelId(jo2.getString("ModelId"));
                                        cm.setSize(jo2.getString("Size"));
                                        modelList.add(cm);
                                    }

                                    cars.setModelList(modelList);
                                    carsList.add(cars);
                                }



                            }catch (JSONException je){
                                je.printStackTrace();
                                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            addCarDialog();
            super.onPostExecute(result);

        }

    }


    public class SaveCarToProfile extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            dialog = new MaterialDialog.Builder(MainActivity.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.SAVE_CAR_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
                        new GetUserCars().execute(Constants.GET_USERCARS_URL+userId);
                       Toast.makeText(MainActivity.this, "Car added to profile", Toast.LENGTH_SHORT).show();
                    }
                }

            }else {
                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    public class GetUserCars extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            dialog = new MaterialDialog.Builder(MainActivity.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            userCarsList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for(int i = 0; i< ja.length(); i++){
                                    UserCars userCars = new UserCars();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    userCars.setUcID(jo1.getString("UcId"));
                                    userCars.setvType(jo1.getString("VType"));
                                    userCars.setSize(jo1.getString("size"));
                                    userCars.setBrandName(jo1.getString("BName"));
                                    userCars.setBrandId(jo1.getString("BId"));
                                    userCars.setModelId(jo1.getString("MId"));
                                    userCars.setModelName(jo1.getString("MName"));

                                    userCarsList.add(userCars);
                                }



                            }catch (JSONException je){
                                je.printStackTrace();
                                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            userCarsAdapter.notifyDataSetChanged();
            if(carProfileAdapter != null){
                carProfileAdapter.notifyDataSetChanged();
            }
            super.onPostExecute(result);

        }

    }



    public class InsertOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            dialog = new MaterialDialog.Builder(MainActivity.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        FirebaseCrash.report(e);
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    FirebaseCrash.report(e);
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            ordered = false;
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }
                    else if(result.contains("Error") || result.contains("error")){
                        try {
                            JSONObject jo= new JSONObject(result);
                            result = jo.getString("Error");
                        } catch (JSONException e) {
                            FirebaseCrash.report(e);
                            e.printStackTrace();
                        }

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        vert.setVisibility(View.GONE);
                        no.setVisibility(View.GONE);
                        yes.setText("Ok");
                        desc.setText(""+result);

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                        // set title
//                        alertDialogBuilder.setTitle("ECO Clean");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage(""+result)
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//
//
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
                    }
                        else {
                        String order_number = "";
                        try{
                            JSONObject jo= new JSONObject(result);
                            order_number = jo.getString("Success");

                            String[] orderNo = order_number.split("-");
                            String[] parts = order_number.split(",");
                            final String orderId = parts[0];
                            selectedCars.clear();
                            addedCarsAdapter.notifyDataSetChanged();
//                            timeField.setText("choose time");
//                            dateField.setText("choose date");
                            schedule_layout.setVisibility(View.GONE);
//                            selectedTime = "";
//                            selectedDate = "";

                            Gson gson = new Gson();
                            String jsonCars = gson.toJson(selectedCars);
                            userPrefEditor.putString("cars","");
                            userPrefEditor.commit();


                            Intent i = new Intent(MainActivity.this, OrderDetails.class);
                            i.putExtra("req_id", orderId);
                            i.putExtra("screen", "confirm");
                            startActivity(i);

                            orderPrefEditor.putString("order_id", orderId);
                            orderPrefEditor.putInt("order_cnt", 1);
                            orderPrefEditor.putString("req_id", orderId);
                            orderPrefEditor.putString("order_status", "new");
                            orderPrefEditor.commit();
                            Toast.makeText(MainActivity.this, "Request sent to service provider successfully", Toast.LENGTH_SHORT).show();
//                            recreate();
                            if(alertDialog1 != null){
                                alertDialog1.cancel();
                            }
//                            showDialog1(secs, true, orderId);
                        }catch (JSONException je){
                            FirebaseCrash.report(je);
                            je.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
//            if(alertDialog1 != null){
//                alertDialog1.dismiss();
//            }

            super.onPostExecute(result);

        }

    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        String distanceResponse;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            pDialog = ProgressDialog.show(MainActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + lat +","+ longi +"&destinations="+ sp.getSpLat() +","+ sp.getSpLong()+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDbUS1BGaNS_-UlStOypbm_FhnCPKFHK7Q");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        secs = jo3.getString("text");
                        String value = jo3.getString("value");
//                        if(language.equalsIgnoreCase("En")) {

                        showDialog1(secs, false, "");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            if(pDialog != null) {
                pDialog.dismiss();
            }
            super.onPostExecute(result);

        }

    }


    public class CancelOrder extends AsyncTask<String, Integer, String> {
        MaterialDialog pDialog;
        String  networkStatus;
        String distanceResponse;
        InputStream inputStream = null;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            pDialog = new MaterialDialog.Builder(MainActivity.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(params[0]);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
//                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", ""+response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    try {
                        JSONObject jo= new JSONObject(result);
                        String sResult = jo.getString("Success");
                        Toast.makeText(MainActivity.this, "Request cancelled successfully.", Toast.LENGTH_SHORT).show();
                        recreate();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            if(pDialog != null) {
                pDialog.dismiss();
            }
            super.onPostExecute(result);

        }

    }


    public class GetSPMarkers extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
//        MaterialDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
//            dialog = new MaterialDialog.Builder(MainActivity.this)
//                    .title("ECO Clean")
//                    .content("Please wait")
//                    .progress(true, 0)
//                    .progressIndeterminateStyle(true)
//                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
//            mMap.clear();
            markersList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for(int i = 0; i< ja.length(); i++){
                                    SPMarkers spm = new SPMarkers();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    String name = jo1.getString("SPNAME");
                                    String address = jo1.getString("SPADDRESS");
                                    Double lat = jo1.getDouble("LATITUDE");
                                    Double longi = jo1.getDouble("LONGITUDE");
                                    spm.setSpName(name);
                                    spm.setSpAddress(address);
                                    spm.setLat(lat);
                                    spm.setLongi(longi);
                                    markersList.add(spm);

                                }



                            }catch (JSONException je){
                                je.printStackTrace();
//                                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }
            for(int i = 0; i<markersList.size(); i++){
                if(lat != null) {
                    markerOptions.position(new LatLng(markersList.get(i).getLat(), markersList.get(i).getLongi()))
                            .title(markersList.get(i).getSpName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.sp_marker));
                    mMap.addMarker(markerOptions);
                }
            }
            super.onPostExecute(result);

        }

    }


    public void showDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.service_provider;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);

//        final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
        ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.sp_cancel);
        ListView spListView = (ListView) dialogView.findViewById(R.id.sp_listview);
        TextView doneBtn = (TextView) dialogView.findViewById(R.id.done_btn);
        final TextView filterDistance = (TextView) dialogView.findViewById(R.id.filter_distance);
        final TextView filterRate = (TextView) dialogView.findViewById(R.id.filter_rating);
        final TextView filterPrice = (TextView) dialogView.findViewById(R.id.filter_price);


        Collections.sort(spList, ServiceProvider.distanceSort);
        mAdapter = new ServiceProviderAdapter(MainActivity.this, spList, "En", selectedDate, selectedTime);
        spListView.setAdapter(mAdapter);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ServiceProviderAdapter.selectedSP == null){
                    Toast.makeText(MainActivity.this, "Please select a service provider", Toast.LENGTH_SHORT).show();
                }else {
//                    alertDialog.dismiss();
                    try {
                        JSONArray ja1 = sp.getSpPrices();
//                        for(int j = 0; j< ja1.length(); j++){
//                            JSONArray ja2 = ja1.getJSONArray(j);
//                            for(int k = 0; k< ja2.length(); k++){
//                                SRDetailsArray.put(ja2.getJSONObject(k));
//                            }
//                        }

                        boolean available = false;
                        if(sp.getIsClose().equals("false")) {
                            if (sp.getIsShift().equals("true")) {
                                if (isWorkingTime(sp.getStartTime().replace(" ", ""), sp.getEndTime().replace(" ", ""))) {
                                    available = true;
                                } else {
                                    if (isWorkingTime(sp.getStartTime2().replace(" ", ""), sp.getEndTime2().replace(" ", ""))) {
                                        available = true;
                                    }
                                }
                            } else if ((sp.getIsShift().equals("false"))) {
                                if (isWorkingTime(sp.getStartTime().replace(" ", ""), sp.getEndTime().replace(" ", ""))) {
                                    available = true;
                                }
                            }
                        }

                        if(available){
                            SRDetailsArray = new JSONArray();
                            for (int j = 0; j < ja1.length(); j++){

                                JSONArray priceSubArray = new JSONArray();
                                JSONArray ja2 = ja1.getJSONArray(j);
                                for(int k = 0; k< ja2.length(); k++){
                                    JSONObject priceObj = new JSONObject();
                                    JSONObject jo2 = ja2.getJSONObject(k);
                                    priceObj.put("ServiceType", jo2.getString("ServiceType"));
                                    priceObj.put("ServiceSubType", jo2.getString("ServiceSubType"));
                                    priceObj.put("Price", jo2.getString("Price"));
                                    priceObj.put("Size", jo2.getString("Size"));
                                    priceObj.put("CarId", j+1);
                                    try {
                                        priceObj.put("PCid", selectedCars.get(j).getCarId());
                                    }catch (Exception e){

                                    }
                                    SRDetailsArray.put(priceObj);
                                }

                            }
                            try {
                                if(alertDialog!=null) {
                                    alertDialog.dismiss();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(sp.getRushStart()!=null) {
                                if (isRushHours()) {
                                    showDialog2();
                                }
                                else {
                                    showDialog1("0", false, "");
                                }
                            }
                            else {
                            showDialog1("0", false, "");
                            }
//                            new getTrafficTime().execute();
                        }else{
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout = R.layout.alert_dialog;
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                            vert.setVisibility(View.GONE);
                            no.setVisibility(View.GONE);
                            yes.setText("Ok");
                            desc.setText("Selected service provider works between "+ sp.getStartTime() + " & "+ sp.getEndTime()+ " only.\nPlease choose another service provider.");

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                }
                            });

                            customDialog = dialogBuilder.create();
                            customDialog.show();
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x; // int screenWidth = display.getWidth(); on API < 13
                            int screenHeight = size.y;

                            double d = screenWidth*0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);

//                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                            // set title
//                            alertDialogBuilder.setTitle("ECO Clean");
//
//                            // set dialog message
//                            alertDialogBuilder
//                                    .setMessage("Selected service provider works between "+ sp.getStartTime() + " & "+ sp.getEndTime()+ " only.\nPlease choose another service provider.")
//                                    .setCancelable(false)
//                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.dismiss();
//                                        }
//                                    });
//
//
//                            // create alert dialog
//                            AlertDialog alertDialog = alertDialogBuilder.create();
//
//                            // show it
//                            alertDialog.show();
                        }
                    }catch (JSONException je){
                        je.printStackTrace();
                    }
//                    showDialog1();
                }
            }
        });

        filterDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDistance.setBackgroundResource(R.drawable.oval_shape);
                filterDistance.setTextColor(Color.parseColor("#FFFFFF"));
                filterRate.setBackgroundResource(R.drawable.round_rect);
                filterRate.setTextColor(Color.parseColor("#000000"));
                filterPrice.setBackgroundResource(R.drawable.round_rect);
                filterPrice.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.distanceSort);
                mAdapter.notifyDataSetChanged();
            }
        });

        filterRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterRate.setBackgroundResource(R.drawable.oval_shape);
                filterRate.setTextColor(Color.parseColor("#FFFFFF"));
                filterDistance.setBackgroundResource(R.drawable.round_rect);
                filterDistance.setTextColor(Color.parseColor("#000000"));
                filterPrice.setBackgroundResource(R.drawable.round_rect);
                filterPrice.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.ratingSort);
                mAdapter.notifyDataSetChanged();
            }
        });

        filterPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterPrice.setBackgroundResource(R.drawable.oval_shape);
                filterPrice.setTextColor(Color.parseColor("#FFFFFF"));
                filterRate.setBackgroundResource(R.drawable.round_rect);
                filterRate.setTextColor(Color.parseColor("#000000"));
                filterDistance.setBackgroundResource(R.drawable.round_rect);
                filterDistance.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.priceSort);
                mAdapter.notifyDataSetChanged();
            }
        });


        alertDialog = dialogBuilder.create();
        alertDialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showDialog2(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.rush_hours_dialog;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView charge = (TextView) dialogView.findViewById(R.id.rush_charge);
        TextView total = (TextView) dialogView.findViewById(R.id.price_equation);
        ImageView cancel = (ImageView) dialogView.findViewById(R.id.sp_cancel);
        TextView confirm = (TextView) dialogView.findViewById(R.id.confirm);

        float disc = 0;
        if(sp.getDisStart()!=null){
            if(isDiscountHours()){
                disc  = Float.parseFloat(sp.getDisPercent());
            }
        }
        charge.setText(sp.getRushCharge());
        float price = Float.parseFloat(sp.getPrice());
        price = price - price*(disc/100);
        totalPriceFloat = Float.parseFloat(sp.getRushCharge())*price;
        total.setText(""+Math.round(price)+" * "+sp.getRushCharge()+" = "+Math.round(totalPriceFloat)+" SR");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog2.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog2.dismiss();
                showDialog1("0",false,"");
            }
        });
        alertDialog2 = dialogBuilder.create();
//        alertDialog2.getWindow().setGravity(Gravity.BOTTOM);
//        alertDialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        alertDialog2.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog2.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showDialog1(String secs, boolean isCompleted, final String reqId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.payment_screen;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);

//        final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
        ImageView tickBtn = (ImageView) dialogView.findViewById(R.id.tick);
        final TextView date = (TextView) dialogView.findViewById(R.id.confirm_date);
        final TextView time = (TextView) dialogView.findViewById(R.id.confirm_time);
        ListView selectedCarsListview = (ListView) dialogView.findViewById(R.id.selectedcar_listView);
//        TextView carName = (TextView) dialogView.findViewById(R.id.confirm_car_name);
        TextView totalPrice = (TextView) dialogView.findViewById(R.id.total_price);
        TextView confirmAddress = (TextView) dialogView.findViewById(R.id.confirm_address);
        TextView confirmSP = (TextView) dialogView.findViewById(R.id.confirm_service_provider);
        TextView cancelTxt = (TextView) dialogView.findViewById(R.id.cancel_txt);
        ImageView confirmOrder = (ImageView) dialogView.findViewById(R.id.confirm_order_btn);
        RelativeLayout cancelOrder = (RelativeLayout) dialogView.findViewById(R.id.cancel_order);
        RelativeLayout dateTimeLayout = (RelativeLayout) dialogView.findViewById(R.id.payment_datetime_layout);
//        TextView serviceType1 = (TextView) dialogView.findViewById(R.id.confirm_service_type);
//        TextView serviceType2 = (TextView) dialogView.findViewById(R.id.confirm_service_type1);
        LinearLayout cardPayment = (LinearLayout) dialogView.findViewById(R.id.card_payment);
        LinearLayout cashPayment = (LinearLayout) dialogView.findViewById(R.id.cash_payment);
        final ImageView cashBtn = (ImageView) dialogView.findViewById(R.id.cash_btn);
//        final ImageView cardBtn = (ImageView) dialogView.findViewById(R.id.card_btn);

//        if(sServiceType1 == null && sServiceType2 != null){
//            serviceType2.setVisibility(View.GONE);
//            serviceType1.setText(sServiceType2);
//        }else if(sServiceType1 != null && sServiceType2 == null){
//            serviceType2.setVisibility(View.GONE);
//            serviceType1.setText(sServiceType1);
//        }else{
//            serviceType1.setText(sServiceType1);
//            serviceType2.setText(sServiceType2);
//        }
//
//        if(isFromProfile){
//            carName.setText(UserCarsAdapter.userCars.getBrandName()+" "+UserCarsAdapter.userCars.getModelName());
//        }else{
//            if(size.equals("1")){
//                carName.setText("Small Car");
//            }else carName.setText("Big Car");
//        }

        paymentMode = "2";

        if (isCompleted) {
            dateTimeLayout.setClickable(false);
            dateTimeLayout.setEnabled(false);
            confirmAddress.setClickable(false);
            confirmAddress.setEnabled(false);
            cashPayment.setClickable(false);
            cashPayment.setEnabled(false);
            cardPayment.setClickable(false);
            cardPayment.setEnabled(false);
            confirmOrder.setClickable(false);
            confirmOrder.setEnabled(false);
            startTimer((10 * 60 * 1000), cancelTxt, cancelOrder, reqId);
        } else {
        }

        SelectedCarsAdapter scAdapter = new SelectedCarsAdapter(MainActivity.this, selectedCars, "En");

        selectedCarsListview.setAdapter(scAdapter);

        date.setText(selectedDate);
        time.setText(selectedTime);
        float disc = 0;
        float normalPrice = Float.parseFloat(sp.getPrice());
        if (sp.getDisStart() != null) {
            if (isDiscountHours()) {
                disc = Float.parseFloat(sp.getDisPercent());
            }
        }
        if (sp.getRushStart() != null) {
            if (isRushHours()) {
                normalPrice = normalPrice - normalPrice * (disc / 100);
                totalPriceFloat = Float.parseFloat(sp.getRushCharge()) * normalPrice;
//                totalPriceFloat = totalPriceFloat - totalPriceFloat*(disc/100);
                totalPrice.setText("" + Math.round(totalPriceFloat));
            } else {
                normalPrice = normalPrice - normalPrice * (disc / 100);
                totalPrice.setText("" + Math.round(normalPrice));
            }
        } else {
            normalPrice = normalPrice - normalPrice * (disc / 100);
            totalPrice.setText("" + Math.round(normalPrice));
        }
        confirmSP.setText(WordUtils.capitalizeFully(sp.getSpName()));
        if (address != null) {
            confirmAddress.setText(address);
        } else {
            confirmAddress.setText("Address not detected");
        }

        dateTimeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;
                                } else {
                                    isToday = false;
                                }
//                                    dateField.setText(dayOfMonth + "," + MONTHS[monthOfYear] + "," + year);

                                mYear = year;
                                mDay = dayOfMonth;
                                mMonth = monthOfYear;

                                final int hour = c.get(Calendar.HOUR_OF_DAY);
                                final int minute = c.get(Calendar.MINUTE);


//                                    TimePickerDialog mTimePicker;
//                                    mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                                        @Override
//                                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                                            if (selectedHour == 0) {
//
//                                                selectedHour += 12;
//
//                                                format = "AM";
//                                            } else if (selectedHour == 12) {
//
//                                                format = "PM";
//
//                                            } else if (selectedHour > 12) {
//
//                                                selectedHour -= 12;
//
//                                                format = "PM";
//
//                                            } else {
//
//                                                format = "AM";
//                                            }
//                                            selectedTime = String.format("%02d:%02d", selectedHour, selectedMinute) + " " + format;
//                                            timeField.setText(selectedTime);
//                                        }
//                                    }, hour, minute, false);//true 24 hour time
//                                    mTimePicker.setTitle("Choose Time");
//                                    mTimePicker.show();


                                com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                                        MainActivity.this,
                                        hour,
                                        minute,
                                        false
                                );
                                tpd.setThemeDark(true);
                                tpd.vibrate(false);
//                    tpd.dismissOnPause(dismissTime.isChecked());
//                    tpd.enableSeconds(enableSeconds.isChecked());
//                    if (modeCustomAccentTime.isChecked()) {
                                tpd.setAccentColor(Color.parseColor("#76C8FC"));
//                    }
//                    if (titleTime.isChecked()) {
//                        tpd.setTitle("TimePicker Title");
//                    }
//                    if (limitTimes.isChecked()) {
//                        tpd.setTimeInterval(2, 5, 10);
//                    }
                                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialogInterface) {
                                        Log.d("TimePicker", "Dialog was cancelled");
                                    }
                                });
                                if (isToday) {
                                    tpd.setMinTime(hour + 1, minute, 00);
                                } else {
                                    tpd.setMinTime(0, 0, 0);
                                }
                                tpd.show(getFragmentManager(), "Timepickerdialog");


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                long max = TimeUnit.DAYS.toMillis(90);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();


//                final Calendar c = Calendar.getInstance();
////                mYear = c.get(Calendar.YEAR);
////                mMonth = c.get(Calendar.MONTH);
////                mDay = c.get(Calendar.DAY_OF_MONTH);
//                final int hour = c.get(Calendar.HOUR_OF_DAY);
//                final int minute = c.get(Calendar.MINUTE);
//
//                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
//                        new DatePickerDialog.OnDateSetListener() {
//
//                            @Override
//                            public void onDateSet(DatePicker view, int year,
//                                                  int monthOfYear, int dayOfMonth) {
//
//                                dateField.setText(dayOfMonth + "," + MONTHS[monthOfYear] + "," + year);
//                                selectedDate = dayOfMonth+"-"+ (monthOfYear + 1) +"-"+year;
//                                mYear = year;
//                                mDay = dayOfMonth;
//                                mMonth = monthOfYear;
//
//
//                                TimePickerDialog mTimePicker;
//                                mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                                    @Override
//                                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                                        if (selectedHour == 0) {
//
//                                            selectedHour += 12;
//
//                                            format = "AM";
//                                        }
//                                        else if (selectedHour == 12) {
//
//                                            format = "PM";
//
//                                        }
//                                        else if (selectedHour > 12) {
//
//                                            selectedHour -= 12;
//
//                                            format = "PM";
//
//                                        }
//                                        else {
//
//                                            format = "AM";
//                                        }
//                                        selectedTime = String.format("%02d:%02d", selectedHour, selectedMinute) +" "+ format;
//                                        date.setText(selectedDate);
//                                        time.setText(selectedTime);
//                                    }
//                                }, hour, minute, false);//true 24 hour time
//                                mTimePicker.setTitle("Select Time");
//                                mTimePicker.show();
//
//                            }
//                        }, mYear, mMonth, mDay);
//                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//                datePickerDialog.show();
            }
        });


        confirmAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MapsActivity.class);
                i.putExtra("markers_list", markersList);
                startActivityForResult(i, PLACE_PICKER_REQUEST);
//                try {
//                    PlacePicker.IntentBuilder intentBuilder =
//                            new PlacePicker.IntentBuilder();
//                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
//                    Intent intent = intentBuilder.build(MainActivity.this);
//                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
//
//                } catch (GooglePlayServicesRepairableException
//                        | GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }
            }
        });

        tickBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog1.dismiss();
            }
        });

//        cancelOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog1.dismiss();
//            }
//        });

//        cardPayment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                paymentMode = "3";
//                cardBtn.setImageResource(R.drawable.payment_selected);
//                cashBtn.setImageResource(R.drawable.payment_unselected);
//            }
//        });
//
//        cashPayment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                paymentMode = "2";
//                cardBtn.setImageResource(R.drawable.payment_unselected);
//                cashBtn.setImageResource(R.drawable.payment_selected);
//            }
//        });

        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ordered) {
                    ordered = true;
                    if (alertDialog != null) {
                        alertDialog.dismiss();
                    }

                    boolean available = false;
                    if (sp.getIsClose().equals("false")) {
                        if (sp.getIsShift().equals("true")) {
                            if (isWorkingTime(sp.getStartTime().replace(" ", ""), sp.getEndTime().replace(" ", ""))) {
                                available = true;
                            } else {
                                if (isWorkingTime(sp.getStartTime2().replace(" ", ""), sp.getEndTime2().replace(" ", ""))) {
                                    available = true;
                                }
                            }
                        } else if ((sp.getIsShift().equals("false"))) {
                            if (isWorkingTime(sp.getStartTime().replace(" ", ""), sp.getEndTime().replace(" ", ""))) {
                                available = true;
                            }
                        }
                    }

                    if (available) {
                        Log.i("TAG Visible", "true");


                        Date today = Calendar.getInstance().getTime();
                        Date expDateTime = null;

                        // (2) create a date "formatter" (the date format we want)
                        SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                        String expDateString = selectedDate + " " + selectedTime;

                        try {
                            expDateTime = timeFormat.parse(expDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (expDateTime != null) {
                            expDateString = formatter.format(expDateTime);


                            // (3) create a new String using the date format we want
                            String nowDate = formatter.format(today);
                            JSONObject parent = new JSONObject();
                            try {
                                JSONArray mainItem = new JSONArray();
                                JSONArray addressItem = new JSONArray();

                                Log.i("TAG","splash id "+SplashActivity.regid);
                                Log.i("TAG","gcmPrefs "+gcmPrefs.getString("gcm",""));
                                regId = SplashActivity.regid;
                                if(regId == null){
                                    regId = gcmPrefs.getString("gcm","");
                                }

                                JSONObject mainObj = new JSONObject();
                                mainObj.put("UserId", userId);
                                mainObj.put("Sp_Id", ServiceProviderAdapter.selectedSP);
                                mainObj.put("RequestType", "NEW");
//                    mainObj.put("ExpectedDT", selectedDate+" "+selectedTime);
                                mainObj.put("ExpectedDT", expDateString);
                                mainObj.put("RequestedDate", nowDate);
                                mainObj.put("DeviceToken", regId);
                                mainObj.put("Comments", "");
                                mainObj.put("Comments", "Android v1.0.0");
                                mainObj.put("PaymentMode", "2");
                                float disc = 0;
                                float normalPrice = Float.parseFloat(sp.getPrice());
                                if (sp.getDisStart() != null) {
                                    if (isDiscountHours()) {
                                        disc = Float.parseFloat(sp.getDisPercent());
                                    }
                                }
                                if (sp.getRushStart() != null) {
                                    if (isRushHours()) {
                                        normalPrice = normalPrice - normalPrice * (disc / 100);
                                        totalPriceFloat = Float.parseFloat(sp.getRushCharge()) * normalPrice;
//                                totalPriceFloat = totalPriceFloat - totalPriceFloat*(disc/100);
                                        mainObj.put("TotalPrice", Math.round(normalPrice));
                                        mainObj.put("RushHourValue", sp.getRushCharge());
                                        mainObj.put("GrandTotal", Math.round(totalPriceFloat));
                                    } else {
                                        normalPrice = normalPrice - normalPrice * (disc / 100);
                                        mainObj.put("TotalPrice", Math.round(normalPrice));
                                        mainObj.put("RushHourValue", "1.0");
                                        mainObj.put("GrandTotal", Math.round(normalPrice));
                                    }
                                } else {
                                    normalPrice = normalPrice - normalPrice * (disc / 100);
                                    mainObj.put("TotalPrice", Math.round(normalPrice));
                                    mainObj.put("RushHourValue", "1.0");
                                    mainObj.put("GrandTotal", Math.round(normalPrice));
                                }
                                mainObj.put("RequestStatus", "New");
//                        if (isFromProfile) {
//                            mainObj.put("UsrCarDetails", UserCarsAdapter.userCars.getUcID());
//                        } else {
                                mainObj.put("UsrCarDetails", "0");
//                        }

                                mainItem.put(mainObj);

                                JSONObject addressObj = new JSONObject();
                                addressObj.put("UserId", userId);
                                addressObj.put("Address", address);
                                addressObj.put("Latitude", lat);
                                addressObj.put("Longitude", longi);
                                addressObj.put("LandMark", "");
                                addressItem.put(addressObj);


                                parent.put("ServiceRequest", mainItem);
                                parent.put("Location", addressItem);
                                parent.put("SRDetails", SRDetailsArray);
                                Log.i("TAG", parent.toString());
                            } catch (JSONException je) {
                                je.printStackTrace();
                            }
                            new InsertOrder().execute(parent.toString());
                        } else {

                        }
                    } else {

                        ordered = false;
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        vert.setVisibility(View.GONE);
                        no.setVisibility(View.GONE);
                        yes.setText("Ok");
                        desc.setText("Selected service provider works between " + sp.getStartTime() + " & " + sp.getEndTime() + " only.\nPlease choose time in this range.");

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x; // int screenWidth = display.getWidth(); on API < 13
                        int screenHeight = size.y;

                        double d = screenWidth * 0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    // set title
//                    alertDialogBuilder.setTitle("ECO Clean");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("Selected service provider works between "+ sp.getStartTime() + " & "+ sp.getEndTime()+ " only.\nPlease choose time in this range.")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
                    }
                }
            }
        });


        alertDialog1 = dialogBuilder.create();
        alertDialog1.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog1.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }



    public void addCarDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.addcar_popup;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);

        ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cancel_btn);
        TextView doneBtn = (TextView) dialogView.findViewById(R.id.done_btn);
        final EditText brandSpinner = (EditText) dialogView.findViewById(R.id.brand_spinner);
        final EditText carModelSpinner = (EditText) dialogView.findViewById(R.id.carmodel_spinner);
        final ListView brandList = (ListView) dialogView.findViewById(R.id.brand_list);
        final ListView modelsList = (ListView) dialogView.findViewById(R.id.model_list);


//        brandList.setVisibility(View.GONE);
        modelsList.setVisibility(View.GONE);

        spinnerBrandAdapter = new SpinnerBrandAdapter(MainActivity.this,carsList);
        brandList.setAdapter(spinnerBrandAdapter);

        brandSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                    brandSpinner.setFocusable(true);
//                    brandSpinner.setFocusableInTouchMode(true);
                    brandList.setVisibility(View.VISIBLE);

//                    carModelSpinner.setFocusable(false);
//                    carModelSpinner.setFocusableInTouchMode(false);
                    modelsList.setVisibility(View.GONE);

                    spinnerBrandAdapter = new SpinnerBrandAdapter(MainActivity.this, carsList);
                    brandList.setAdapter(spinnerBrandAdapter);
                    spinnerBrandAdapter.notifyDataSetChanged();
            }
        });
        brandList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//                brandList.setVisibility(View.GONE);
                brandSpinner.setText(SpinnerBrandAdapter.filteredData.get(i).getBrandName());
                brandPos = i;
//                brandSpinner.setFocusable(false);
//                brandSpinner.setFocusableInTouchMode(false);
                brandList.setVisibility(View.GONE);
                brandSpinnerCount=0;

                for (int j=0; j<carsList.size(); j++){
                    if(brandSpinner.getText().toString().equals(carsList.get(j).getBrandName()))
                        cmList = carsList.get(j).getModelList();
                    carModelSpinner.setText("");
                }


                spinnerModelAdapter = new SpinnerModelAdapter(MainActivity.this,cmList);
                modelsList.setAdapter(spinnerModelAdapter);
                spinnerModelAdapter.notifyDataSetChanged();
            }
        });

        carModelSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                    brandSpinner.setFocusable(false);
//                    brandSpinner.setFocusableInTouchMode(false);
                    brandList.setVisibility(View.GONE);

//                    carModelSpinner.setFocusable(true);
//                    carModelSpinner.setFocusableInTouchMode(true);
                    modelsList.setVisibility(View.VISIBLE);

                    spinnerModelAdapter = new SpinnerModelAdapter(MainActivity.this, cmList);
                    modelsList.setAdapter(spinnerModelAdapter);
                    spinnerModelAdapter.notifyDataSetChanged();
            }
        });
        modelsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//                brandList.setVisibility(View.GONE);
                carModelSpinner.setText(SpinnerModelAdapter.filteredData.get(i).getModelName());
                modelPos = i;
//                carModelSpinner.setFocusable(false);
//                carModelSpinner.setFocusableInTouchMode(false);
                modelsList.setVisibility(View.GONE);
                modelSpinnerCount=0;
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carDialog.dismiss();
            }
        });

        brandSpinner.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                MainActivity.this.spinnerBrandAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        carModelSpinner.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                try {
                    MainActivity.this.spinnerModelAdapter.getFilter().filter(cs);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


//        brandSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Cars cars;
//                ArrayList<CarModel> cmList = new ArrayList<>();
//                if(!(brandSpinner.getSelectedItem() == null)) {
//                    cars = (Cars) brandSpinner.getSelectedItem();
//                    cmList = cars.getModelList();
//
//                    SpinnerModelAdapter spinnerModelAdapter = new SpinnerModelAdapter(MainActivity.this,cmList);
////                    ArrayAdapter<CarModel> adapter =
////                            new ArrayAdapter<>(MainActivity.this, R.layout.spinner_item, cmList);
////                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//                    carModelSpinner.setAdapter(spinnerModelAdapter);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carDialog.dismiss();

                for (int j=0; j<carsList.size(); j++){
                    if(brandSpinner.getText().toString().equals(carsList.get(j).getBrandName()))
                        brandPos = j;
                }

                for (int j=0; j<cmList.size(); j++){
                    if(carModelSpinner.getText().toString().equals(cmList.get(j).getModelName()))
                        modelPos = j;
                }

                JSONObject parent = new JSONObject();
//                Cars cars = new Cars();
//                CarModel cm = new CarModel();
                if(brandPos == -1) {
                }else if(modelPos == -1){
                }else{
//                    cars = (Cars) brandPos;
//                    cm = (CarModel) carModelSpinner.getSelectedItem();
                    try {
                        JSONArray mainItem = new JSONArray();


                        JSONObject mainObj = new JSONObject();
                        mainObj.put("Model", cmList.get(modelPos).getModelId());
                        mainObj.put("Brand", carsList.get(brandPos).getBrandId());
                        mainObj.put("Size", cmList.get(modelPos).getSize());
                        mainObj.put("UserId", userId);
                        mainObj.put("VehicalType", "Personal");
                        mainObj.put("IsActive", true);
                        mainItem.put(mainObj);


                        parent.put("UsrCar", mainItem);
                        Log.i("TAG", parent.toString());
                    } catch (JSONException je) {

                    }
                    new SaveCarToProfile().execute(parent.toString());
                }
            }
        });

        carDialog = dialogBuilder.create();
        carDialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = carDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }



    public void carProfileDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.car_profile_popup;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);

        ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.cp_cancel);
        TextView doneBtn = (TextView) dialogView.findViewById(R.id.done_btn);
        TextView addCarBtn = (TextView) dialogView.findViewById(R.id.add_car_btn);
        ListView cpListView = (ListView) dialogView.findViewById(R.id.cp_listview);

        carProfileAdapter = new CarProfileAdapter(MainActivity.this, userCarsList, "En");
        cpListView.setAdapter(carProfileAdapter);

        new GetUserCars().execute(Constants.GET_USERCARS_URL+userId);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carProfileDialog.dismiss();
                userCarsAdapter.notifyDataSetChanged();
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carProfileDialog.dismiss();
                userCarsAdapter.notifyDataSetChanged();
            }
        });

        addCarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetCars().execute(Constants.GET_CARS_URL);
            }
        });


        carProfileDialog = dialogBuilder.create();
        carProfileDialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = carProfileDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    //Start Countodwn method
    private void startTimer(int noOfMinutes, final TextView v, final RelativeLayout v1, final String reqId) {
        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                v.setText("Cancel Order\n"+hms);
                v1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new CancelOrder().execute(Constants.CANCEL_ORDER_URL+reqId+"&comment=");
                        new CancelOrder().execute(Constants.CANCEL_ORDER_URL+reqId+"&comment=");
                        countDownTimer.cancel();
                    }
                });
            }

            public void onFinish() {
                recreate();
            }
        }.start();

    }

    public boolean isDiscountHours() {

        String expDateString = selectedDate + " " + selectedTime;
        String rushStart = sp.getDisStart();
        String rushEnd = sp.getDisEnd();

        String[] str = expDateString.split(" ");
        String[] dNow = str[0].split("-");
        String[] tNow = str[1].split(":");
        String[] sDate = rushStart.split("T");
        String[] eDate = rushEnd.split("T");
        String[] sDate1 = sDate[0].split("-");
        String[] eDate1 = eDate[0].split("-");
        String[] sTime = sDate[1].split(":");
        String[] eTime = eDate[1].split(":");

        int nowYear = Integer.parseInt(dNow[2]);
        int startYear = Integer.parseInt(sDate1[0]);
        int endYear = Integer.parseInt(eDate1[0]);

        int nowMonth = Integer.parseInt(dNow[1]);
        int startMonth = Integer.parseInt(sDate1[1]);
        int endMonth = Integer.parseInt(eDate1[1]);

        int nowDate = Integer.parseInt(dNow[0]);
        int startDate = Integer.parseInt(sDate1[2]);
        int endDate = Integer.parseInt(eDate1[2]);

        int nowHour = Integer.parseInt(tNow[0]);
        int startHour = Integer.parseInt(sTime[0]);
        int endHour = Integer.parseInt(eTime[0]);

        int nowMinute = Integer.parseInt(tNow[1]);
        int startMinute = Integer.parseInt(sTime[1]);
        int endMinute = Integer.parseInt(eTime[1]);

        if(str[2].equalsIgnoreCase("PM")){
            nowHour = nowHour+12;
        }

        boolean b = false;
        if(nowYear >= startYear && nowYear<=endYear ){
            if(nowYear==startYear && nowYear == endYear) {
                if (nowMonth >= startMonth && nowMonth <= endMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth != startMonth && nowMonth != endMonth){
                        b = true;
                    }
                }
            }
            else if(nowYear == startYear){
                if (nowMonth >= startMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth != startMonth && nowMonth != endMonth){
                        b = true;
                    }
                }
            }
            else if(nowYear == endYear){
                if (nowMonth <= endMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                }
            }
            else if(nowYear!=startYear && nowYear != endYear){
                b=true;
            }
        }
        return b;
    }

    public boolean isRushHours() {

        String expDateString = selectedDate + " " + selectedTime;
        String rushStart = sp.getRushStart();
        String rushEnd = sp.getRushEnd();

        String[] str = expDateString.split(" ");
        String[] dNow = str[0].split("-");
        String[] tNow = str[1].split(":");
        String[] sDate = rushStart.split("T");
        String[] eDate = rushEnd.split("T");
        String[] sDate1 = sDate[0].split("-");
        String[] eDate1 = eDate[0].split("-");
        String[] sTime = sDate[1].split(":");
        String[] eTime = eDate[1].split(":");

        int nowYear = Integer.parseInt(dNow[2]);
        int startYear = Integer.parseInt(sDate1[0]);
        int endYear = Integer.parseInt(eDate1[0]);

        int nowMonth = Integer.parseInt(dNow[1]);
        int startMonth = Integer.parseInt(sDate1[1]);
        int endMonth = Integer.parseInt(eDate1[1]);

        int nowDate = Integer.parseInt(dNow[0]);
        int startDate = Integer.parseInt(sDate1[2]);
        int endDate = Integer.parseInt(eDate1[2]);

        int nowHour = Integer.parseInt(tNow[0]);
        int startHour = Integer.parseInt(sTime[0]);
        int endHour = Integer.parseInt(eTime[0]);

        int nowMinute = Integer.parseInt(tNow[1]);
        int startMinute = Integer.parseInt(sTime[1]);
        int endMinute = Integer.parseInt(eTime[1]);

        if(str[2].equalsIgnoreCase("PM")){
            nowHour = nowHour+12;
        }

        boolean b = false;
        if(nowYear >= startYear && nowYear<=endYear ){
            if(nowYear==startYear && nowYear == endYear) {
                if (nowMonth >= startMonth && nowMonth <= endMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth != startMonth && nowMonth != endMonth){
                        b = true;
                    }
                }
            }
            else if(nowYear == startYear){
                if (nowMonth >= startMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth != startMonth && nowMonth != endMonth){
                        b = true;
                    }
                }
            }
            else if(nowYear == endYear){
                if (nowMonth <= endMonth) {
                    if (nowMonth == startMonth && nowMonth == endMonth){
                        if (nowDate >= startDate && nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == startMonth){
                        if (nowDate >= startDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                    else if(nowMonth == endMonth){
                        if (nowDate <= endDate) {
                            if (nowDate == endDate) {
                                if (nowHour <= endHour) {
                                    if (nowHour == endHour) {
                                        if (nowMinute <= endMinute) {
                                            b = true;
                                        }
                                    } else {
                                        b = true;
                                    }
                                }
                            } else {
                                b = true;
                            }
                        }
                    }
                }
            }
            else if(nowYear!=startYear && nowYear != endYear){
                b=true;
            }
        }
        return b;
    }

    public boolean isWorkingTime(String stTime, String etTime){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat ExptimeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        String expDateString = selectedDate+" "+selectedTime;
        expDateString = expDateString.replace("-", "/");

        String startTime = stTime.replace(" ","");
        String endTime = etTime.replace(" ","");

        if (endTime.equals("12:00AM")) {
            endTime = "11:59PM";
        }

        Date serverDate = null;
        Date end24Date = null;
        Date start24Date = null;
        Date current24Date = null;
        Date dateToday = null;
        Calendar dateStoreClose = Calendar.getInstance();
        try {
            end24Date = timeFormat.parse(endTime);
            start24Date = timeFormat.parse(startTime);
            serverDate = dateFormat.parse(expDateString);
            dateToday = dateFormat1.parse(expDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateStoreClose.setTime(dateToday);
        dateStoreClose.add(Calendar.DATE, 1);
        String current24 = timeFormat1.format(serverDate);
        String end24 =timeFormat1.format(end24Date);
        String start24 = timeFormat1.format(start24Date);
        String startDateString = dateFormat1.format(dateToday);
        String endDateString = dateFormat1.format(dateToday);
        String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
        dateStoreClose.add(Calendar.DATE, -2);
        String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

        Date startDate = null;
        Date endDate = null;

        try {
            end24Date = timeFormat1.parse(end24);
            start24Date = timeFormat1.parse(start24);
            current24Date = timeFormat1.parse(current24);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] parts2 = start24.split(":");
        int startHour = Integer.parseInt(parts2[0]);
        int startMinute = Integer.parseInt(parts2[1]);

        String[] parts = end24.split(":");
        int endHour = Integer.parseInt(parts[0]);
        int endMinute = Integer.parseInt(parts[1]);

        String[] parts1 = current24.split(":");
        int currentHour = Integer.parseInt(parts1[0]);
        int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


        if(startTime.contains("AM") && endTime.contains("AM")){
            if(startHour < endHour){
                startDateString = startDateString+ " "+ startTime;
                endDateString = endDateString+"  " + endTime;
                try {
                    startDate = dateFormat2.parse(startDateString);
                    endDate = dateFormat2.parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else if(startHour > endHour){
                if(expDateString.contains("AM")){
                    if(currentHour > endHour){
                        startDateString = startDateString + " " + startTime;
                        endDateString = endDateTomorrow + "  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        startDateString = endDateYesterday + " " + startTime;
                        endDateString = endDateString + "  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    startDateString = startDateString + " " + startTime;
                    endDateString = endDateTomorrow + "  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else if(startTime.contains("AM") && endTime.contains("PM")){
            startDateString = startDateString+ " "+ startTime;
            endDateString = endDateString+"  " + endTime;
            try {
                startDate = dateFormat2.parse(startDateString);
                endDate = dateFormat2.parse(endDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else if(startTime.contains("PM") && endTime.contains("AM")){
            if(expDateString.contains("AM")){
                if(currentHour <= endHour){
                    startDateString = endDateYesterday+ " "+ startTime;
                    endDateString = endDateString+"  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }else{
                    startDateString = startDateString+ " "+ startTime;
                    endDateString = endDateTomorrow+"  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }else{

                startDateString = startDateString+ " "+ startTime;
                endDateString = endDateTomorrow+"  " + endTime;
                try {
                    startDate = dateFormat2.parse(startDateString);
                    endDate = dateFormat2.parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }else if(startTime.contains("PM") && endTime.contains("PM")){
            startDateString = startDateString+ " "+ startTime;
            endDateString = endDateString+"  " + endTime;
            try {
                startDate = dateFormat2.parse(startDateString);
                endDate = dateFormat2.parse(endDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        String serverDateString = dateFormat2.format(serverDate);

        try {
            serverDate = dateFormat2.parse(serverDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.i("TAG DATE" , ""+ startDate);
        Log.i("TAG DATE1" , ""+ endDate);
        Log.i("TAG DATE2" , ""+ serverDate);
        Log.i("TAG DATE2" , ""+ startTime);
        Log.i("TAG DATE2" , ""+ endTime);

        if(serverDate.after(startDate) && serverDate.before(endDate)) {
            Log.i("TAG Visible", "true");
            return true;
        }else {
            return false;
        }
    }



    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,"Press BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
        new GetOrderDetails().execute(Constants.TRACK_ORDER_URL + "-1&userId="+userId);

    }

    public void getAddress(double lat, double lng){
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        // Address found using the Geocoder.
        List<Address> addresses = null;

        try {
            // Using getFromLocation() returns an array of Addresses for the area immediately
            // surrounding the given latitude and longitude. The results are a best guess and are
            // not guaranteed to be accurate.
            addresses = geocoder.getFromLocation(
                    lat, lng,
                    // In this sample, we get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
//            errorMessage = getString(R.string.service_not_available);
//            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
//            errorMessage = getString(R.string.invalid_lat_long_used);
//            Log.e(TAG, errorMessage + ". " +
//                    "Latitude = " + location.getLatitude() +
//                    ", Longitude = " + location.getLongitude(), illegalArgumentException);
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
//            if (errorMessage.isEmpty()) {
//                errorMessage = getString(R.string.no_address_found);
//                Log.e(TAG, errorMessage);
//            }
//            deliverResultToReceiver(AppUtils.LocationConstants.FAILURE_RESULT, errorMessage, null);
        } else {
            Address address1 = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();

            for (int i = 0; i < address1.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address1.getAddressLine(i));

            }
            selAddr1.setVisibility(View.GONE);
            selAddr2.setText(TextUtils.join(System.getProperty("line.separator"), addressFragments));
            address = selAddr2.getText().toString();

            LatLng latLng = new LatLng(lat, longi);
            mMap.clear();

            for(int i = 0; i<markersList.size(); i++){
                if(lat != 0) {
                    markerOptions.position(new LatLng(markersList.get(i).getLat(), markersList.get(i).getLongi()))
                            .title(markersList.get(i).getSpName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.sp_marker));
                    mMap.addMarker(markerOptions);
                }
            }
            if(mMap != null) {
                // Show the current location in Google Map
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, longi))
                        .title(TextUtils.join(System.getProperty("line.separator"), addressFragments)));
                // Zoom in the Google Map
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));

                if(alertDialog1 != null){
                    alertDialog1.dismiss();
//                        showDialog1(secs, false, "");
                }

                if(alertDialog != null){
                    alertDialog.dismiss();
//                        showDialog1(secs, false, "");
                }
            }
        }
    }
    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
//        ProgressDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
//            dialog = ProgressDialog.show(OrderDetails.this, "",
//                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
//                        Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject success = jo.getJSONObject("Success");

                                JSONArray ja = success.getJSONArray("Rdetails");
                                for(int i = 0; i< ja.length(); i++){
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    String status = jo1.getString("RequestStatus");
                                    String mSpName = jo1.getString("spname");

                                    if(status.equalsIgnoreCase("close")){
                                        if(jo1.getInt("Ratings") == 0){

//                                            JSONArray RatingArray = jo1.getJSONArray("RatingDetails");
//                                            for(int j = 0; j < carsArray.length(); j++){
                                            Rating rating = new Rating();
                                            JSONObject obj = jo1.getJSONObject("RatingDetails");
                                            rating.setSpId(obj.getString("sp_id"));
                                            rating.setReqId(obj.getString("req_id"));
                                            rating.setUserId(obj.getString("user_id"));

//                                            RatingDetails rd = new RatingDetails();
                                            JSONObject jA = obj.getJSONObject("sampleComments");
//                                            JSONObject obj = jA.getJSONObject(0);
                                            rating.setGivenrating("1");
                                            JSONArray ratingArray = jA.getJSONArray("1");
                                            ArrayList<RatingDetails> ratingsArrayList = new ArrayList<>();
                                            for (int j = 0; j < ratingArray.length(); j++) {
                                                JSONObject object1 = ratingArray.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);

                                            rating.setGivenrating("2");
                                            JSONArray ratingArray1 = jA.getJSONArray("2");
                                            for (int j = 0; j < ratingArray1.length(); j++) {
                                                JSONObject object1 = ratingArray1.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);

                                            rating.setGivenrating("3");
                                            JSONArray ratingArray2 = jA.getJSONArray("3");
                                            for (int j = 0; j < ratingArray2.length(); j++) {
                                                JSONObject object1 = ratingArray2.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);

                                            rating.setGivenrating("4");
                                            JSONArray ratingArray3 = jA.getJSONArray("4");
                                            for (int j = 0; j < ratingArray3.length(); j++) {
                                                JSONObject object1 = ratingArray3.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);

                                            rating.setGivenrating("5");
                                            JSONArray ratingArray4 = jA.getJSONArray("5");
                                            for (int j = 0; j < ratingArray4.length(); j++) {
                                                JSONObject object1 = ratingArray4.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);
                                            ratingsArray.add(rating);

                                            rate_layout.setVisibility(View.VISIBLE);
                                            cancel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    rate_layout.setVisibility(View.GONE);
                                                }
                                            });

                                            ratingBar.setRating(0);
                                            rateTitle.setText(WordUtils.capitalizeFully("Rate "+mSpName));
                                            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                @Override
                                                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//                                                    showDialog2(rating);
                                                    if(fromUser) {
                                                        Intent intent = new Intent(MainActivity.this, RatingActivity.class);
                                                        intent.putExtra("rating", rating);
                                                        intent.putExtra("array", ratingsArray);
                                                        intent.putExtra("userid", userId);
                                                        intent.putExtra("spid", ratingsArray.get(0).getSpId());
                                                        intent.putExtra("reqid", ratingsArray.get(0).getReqId());
                                                        startActivity(intent);
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                }
                                            });
//                                            slideUpDown(rateLayout);
                                        }
                                    }
                                }
                            }catch (JSONException je){
                                je.printStackTrace();
//                                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }


            super.onPostExecute(result);
        }
    }
    public void showDatePicker(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if(year == mYear && monthOfYear == mMonth && dayOfMonth == mDay){
                            isToday = true;
                        }else {
                            isToday = false;
                        }
                        mYear = year;
                        mDay = dayOfMonth;
                        mMonth = monthOfYear;

                        final int hour = c.get(Calendar.HOUR_OF_DAY);
                        final int minute = c.get(Calendar.MINUTE);

                        com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                                MainActivity.this,
                                hour,
                                minute,
                                false
                        );
                        tpd.setThemeDark(true);
                        tpd.vibrate(false);
                        tpd.setAccentColor(Color.parseColor("#76C8FC"));
                        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                Log.d("TimePicker", "Dialog was cancelled");
                            }
                        });
                        if (isToday) {
                            tpd.setMinTime(hour + 1, minute, 00);
                        }else {
                            tpd.setMinTime(0,0,0);
                        }
                        tpd.show(getFragmentManager(), "Timepickerdialog");

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        long max = TimeUnit.DAYS.toMillis(90);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis()+max);
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gps.stopUsingGPS();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            Bundle b = intent.getBundleExtra("Location");
            Location location = (Location) b.getParcelable("Location");
            if (location != null) {
                mMap.clear();

                lat = location.getLatitude();
                longi = location.getLongitude();

                getAddress(lat,longi);
                BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
                        new LatLng(lat, longi), new LatLng(lat, longi));

                LatLng latLng = new LatLng(lat, longi);
                // Show the current location in Google Map
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                // Zoom in the Google Map
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
            }
        }
    };
}
