package com.cs.ecoclean;

/**
 * Created by CS on 01-11-2016.
 */
public class Constants {
//    static String MAIN_URL = "http://85.194.94.241/ECOClean/";
//    static String MAIN_URL = "http://217.145.255.75/ECOClean/";
    static String MAIN_URL = "http://csadms.com/EcoCleanServices/";
    public static String LOGIN_URL = MAIN_URL + "api/Registration/Signin/";
    public static String REGISTRATION_URL = MAIN_URL + "api/Registration/RegisterUser";
    public static String MOBILE_VERIFY_URL = MAIN_URL + "api/Registration/CheckMobileNo?MobileNo=";
    public static String SERVICE_PROVIDER_URL = MAIN_URL + "api/ServiceProviderDetails/GetSPDetails";
    public static String GET_CARS_URL = MAIN_URL + "api/Vehical/GetVehicalDetails";
    public static String GET_USERCARS_URL = MAIN_URL + "api/Vehical/GetUserCars?Userid=";
    public static String SAVE_CAR_URL = MAIN_URL + "api/Vehical/AddUserCar";
    public static String INSERT_ORDER_URL = MAIN_URL + "api/InsertRequest/InsertRequest";
    public static String DELETE_CAR_URL = MAIN_URL + "api/Vehical/DeleteUserCar?UserId=";
    public static String UPDATE_PROFILE_URL = MAIN_URL + "api/Registration/UpdateUserProfile";
    public static String CHANGE_PASSWORD_URL = MAIN_URL + "api/Registration/ChangePassword/";
    public static String GET_ALL_SPLIST = MAIN_URL + "api/ServiceProviderDetails/GetAllSp";
    public static String GET_ORDER_HISTORY = MAIN_URL + "api/InsertRequest/GetOrderHistory?usrId=";
    public static String CANCEL_ORDER_URL = MAIN_URL + "api/ServiceProviderDetails/CancelServiceRequest?Reqid=";
    public static String FORGOT_PASSWORD_URL = MAIN_URL+"/api/Registration/SendOTP?userid=";
    public static String RESET_PASSWORD_URL = MAIN_URL + "/api/Registration/ForgetPassword?Username=";
    public static String TRACK_ORDER_URL = MAIN_URL + "/api/InsertRequest/GetTrackDetails?ReqId=";
    public static String UPDATE_ORDER_URL = MAIN_URL + "/api/InsertRequest/UpdateRequestDetails";
    public static String RATING_URL = MAIN_URL + "api/ServiceProviderDetails/RateSp";
    public static String IMAGE_URL = MAIN_URL + "UploadedImages/";
    public static String UPDATE_LOG = MAIN_URL + "api/insertRequest/updateRequestlog?reqId=";
}
