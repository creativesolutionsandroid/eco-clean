package com.cs.ecoclean;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.ecoclean.Activities.ForgotPassword;
import com.cs.ecoclean.Activities.SplashActivity;
import com.cs.ecoclean.adapters.CountriesAdapter;
import com.cs.ecoclean.model.Countries;
import com.cs.ecoclean.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CS on 01-11-2016.
 */
public class LoginActivity extends Activity {

    EditText mobileNumber, password;
    RelativeLayout loginBtn;
    TextView forgotPwd;
    LinearLayout signUpBtn;

    String response, regId;
    SharedPreferences userPrefs;
    AlertDialog customDialog;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences gcmPrefs;
    SharedPreferences.Editor gcmPrefEditor;

    private ImageView mCountryFlag;
    private ArrayList<Countries> countriesList = new ArrayList<>();
    private Countries countries;
    private CountriesAdapter mCountriesAdapter;
    private String countryCode = "+966";
    private int countryFlag;
    private int phoneNumberLength;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        gcmPrefs = getSharedPreferences("GCM_PREFS", Context.MODE_PRIVATE);
        gcmPrefEditor  = gcmPrefs.edit();

        mobileNumber = (EditText) findViewById(R.id.mobile_number);
        password = (EditText) findViewById(R.id.password);
        loginBtn = (RelativeLayout) findViewById(R.id.login_btn);
        forgotPwd = (TextView) findViewById(R.id.forgot_password);
        signUpBtn = (LinearLayout) findViewById(R.id.signup_btn);
        mCountryFlag = (ImageView) findViewById(R.id.country_flag);


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phNo = mobileNumber.getText().toString();
                String pwd = password.getText().toString();
                if(phNo.length() == 0) {
                    mobileNumber.setError("Please enter mobile number");
                    mobileNumber.requestFocus();
                } else if (pwd.length() == 0) {
                    password.setError("Please enter Password");
                    password.requestFocus();
                }else {
                    regId = SplashActivity.regid;
                    if(regId == null){
                        regId = gcmPrefs.getString("gcm","");
                    }
                    new CheckLoginDetails().execute(Constants.LOGIN_URL + countryCode.replace("+","") + phNo + "?psw=" + pwd + "&dtoken="+ regId + "&lan=En");
                }
            }
        });


        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(i);
            }
        });

        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(i);
            }
        });

        mCountryFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog();
            }
        });

        prepareArrayLits();
        mCountriesAdapter = new CountriesAdapter(this,countriesList);
    }



    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            dialog = new MaterialDialog.Builder(LoginActivity.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoginActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try {
                                JSONObject jo1 = jo.getJSONObject("Success");
//                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
//                                String gender = jo1.getString("Gender");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
//                                    jsonObject.put("family_name", familyName);
//                                    jsonObject.put("nick_name", nickName);
//                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    userPrefEditor.putString("login_status", "loggedin");
                                    userPrefEditor.commit();
                                    Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(loginIntent);
//                                        setResult(RESULT_OK);
                                        finish();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                                    // set title
//                                    alertDialogBuilder.setTitle("ECO Clean");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage("Invalid Mobile / Password")
//                                            .setCancelable(false)
//                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                vert.setVisibility(View.GONE);
                                no.setVisibility(View.GONE);
                                yes.setText("Ok");
                                desc.setText("Invalid Mobile / Password");

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    /* Method used to prepare the ArrayList,
    * Same way, you can also do looping and adding object into the ArrayList.
    */
    public void prepareArrayLits()
    {

        AddObjectToList(R.drawable.bh, "Bahrain", "+973", "18");
        AddObjectToList(R.drawable.bd, "Bangladesh", "+880", "19");
        AddObjectToList(R.drawable.cn, "China", "+86", "45");
        AddObjectToList(R.drawable.cy, "Cyprus", "+537" ,"57");
        AddObjectToList(R.drawable.eg, "Egypt", "+20", "64");
        AddObjectToList(R.drawable.fr, "France", "+33", "74");
        AddObjectToList(R.drawable.de, "Germany", "+49", "81");
        AddObjectToList(R.drawable.in, "India", "+91", "101");
        AddObjectToList(R.drawable.id, "Indonesia", "+62", "102");

        AddObjectToList(R.drawable.ir, "Iran", "+98", "103");
        AddObjectToList(R.drawable.iq, "Iraq", "+964", "104");
        AddObjectToList(R.drawable.jo, "Jordan", "+962", "112");
        AddObjectToList(R.drawable.kw, "Kuwait", "+965", "118");
        AddObjectToList(R.drawable.lb, "Lebanon", "+961", "122");
        AddObjectToList(R.drawable.my, "Malaysia", "+60", "133");
        AddObjectToList(R.drawable.ma, "Morocco", "+212", "147");
        AddObjectToList(R.drawable.np, "Nepal", "+977", "152");
        AddObjectToList(R.drawable.om, "Oman", "+968", "164");

        AddObjectToList(R.drawable.pk, "Pakistan", "+92", "165");
        AddObjectToList(R.drawable.ps, "Palestinian Territories", "", "167");
        AddObjectToList(R.drawable.ph, "Philippines", "+63", "172");
        AddObjectToList(R.drawable.qa, "Qatar", "+974", "177");
        AddObjectToList(R.drawable.sa, "Saudi Arabia", "+966", "190");
        AddObjectToList(R.drawable.sg, "Singapore", "+65", "195");
        AddObjectToList(R.drawable.es, "Spain", "+34", "202");
        AddObjectToList(R.drawable.lk, "Sri Lanka", "+94", "203");
        AddObjectToList(R.drawable.sd, "Sudan", "+249", "204");

        AddObjectToList(R.drawable.tr, "Syria", "+963", "210");
        AddObjectToList(R.drawable.tr, "Turkey", "+90", "221");
        AddObjectToList(R.drawable.ae, "United Arab Emirates", "+971", "227");
        AddObjectToList(R.drawable.gb, "United Kingdom", "+44", "228");
        AddObjectToList(R.drawable.us, "United States", "+1", "229");
        AddObjectToList(R.drawable.ye, "Yemen", "+967", "240");
    }

    // Add one item into the Array List
    public void AddObjectToList(int image, String countryName, String countryCode, String countrrId)
    {
        countries = new Countries();
        countries.setCountryName(countryName);
        countries.setCountryFlag(image);
        countries.setCountryCode(countryCode);
        countries.setCountryID(countrrId);
        countriesList.add(countries);
    }

    public void dialog() {

        final Dialog dialog2 = new Dialog(this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.setContentView(R.layout.countries_list);
        dialog2.setCanceledOnTouchOutside(true);
        ListView lv = (ListView) dialog2.findViewById(R.id.countries_list);
        lv.setAdapter(mCountriesAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                countryCode = countriesList.get(arg2).getCountryCode();
                countryFlag = countriesList.get(arg2).getCountryFlag();

                if(countryCode.equalsIgnoreCase("+966")){
                    int maxLength = 9;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mobileNumber.setFilters(fArray);
                    phoneNumberLength = 9;
                }else{
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mobileNumber.setFilters(fArray);
                    phoneNumberLength = 10;
                }

                mCountryFlag.setImageResource(countriesList.get(arg2).getCountryFlag());
//                mCountryCode.setText("" + countriesList.get(arg2).getCountryCode());

                dialog2.dismiss();

            }
        });


        dialog2.show();

    }
}
