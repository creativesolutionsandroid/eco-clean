package com.cs.ecoclean.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.ecoclean.Constants;
import com.cs.ecoclean.JSONParser;
import com.cs.ecoclean.MainActivity;
import com.cs.ecoclean.R;
import com.cs.ecoclean.adapters.ServiceProviderAdapter;
import com.cs.ecoclean.model.CarDetails;
import com.cs.ecoclean.model.HistoryCars;
import com.cs.ecoclean.model.ServiceProvider;
import com.cs.ecoclean.model.ToDo;
import com.cs.ecoclean.widgets.NetworkUtil;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by CS on 03-01-2017.
 */
public class TrackOrder extends AppCompatActivity {
    private DilatingDotsProgressBar mDilatingDotsProgressBar;
    private CountDownTimer countDownTimer;
    TextView timerText, spAction;
    Button cancelButton, yesBtn;
    LinearLayout requestNewOrder, liveTrackBtn, newOrderButton;
    ImageView backBtn;
    String reqId, reqStatus, spName, spNumber;
    private String mSpResponse;
    boolean fromHistory, isClosed = false, isNewOrder = false;
    ArrayList<ServiceProvider> spList = new ArrayList<>();
    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefEditor;
    int orderCnt = 1;

    ServiceProviderAdapter mAdapter;
    AlertDialog alertDialog, customDialog;

    String selectedDate, selectedTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_order);

        reqId = getIntent().getExtras().getString("req_id");
        reqStatus = getIntent().getStringExtra("order_status");
        fromHistory = getIntent().getBooleanExtra("from_history", false);
        spName = getIntent().getStringExtra("sp_name");
        spNumber = getIntent().getStringExtra("sp_number");

        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefEditor = orderPrefs.edit();
        mSpResponse = orderPrefs.getString("sp_response", null);
        selectedDate = orderPrefs.getString("sel_date", null);
        selectedTime = orderPrefs.getString("sel_time", null);
        orderCnt = orderPrefs.getInt("order_cnt", 1);
        Log.i("TAG", ""+orderCnt);

        mDilatingDotsProgressBar = (DilatingDotsProgressBar) findViewById(R.id.progress);

        timerText = (TextView) findViewById(R.id.timer_text);
        cancelButton = (Button) findViewById(R.id.cancel_button);
        requestNewOrder = (LinearLayout) findViewById(R.id.request_new_order);
        spAction = (TextView) findViewById(R.id.sp_action);
        yesBtn = (Button) findViewById(R.id.yes_btn);
        liveTrackBtn = (LinearLayout) findViewById(R.id.live_track_btn);
        newOrderButton = (LinearLayout) findViewById(R.id.new_order_btn);
        backBtn = (ImageView) findViewById(R.id.back_btn1);
        backBtn.setVisibility(View.GONE);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if(orderCnt < 4){

        }else {
            timerText.setText("Request Accepted");
            spAction.setText("Request accepted by Service provider`");
            newOrderButton.setVisibility(View.VISIBLE);
        }


        new GetOrderHistory().execute(Constants.TRACK_ORDER_URL + reqId);
        if(fromHistory) {
            if(reqStatus.equalsIgnoreCase("new")) {
                requestNewOrder.setVisibility(View.GONE);
                liveTrackBtn.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                timerText.setText("Your request not accepted");
            }
        }else {
            mDilatingDotsProgressBar.show(500);
            requestNewOrder.setVisibility(View.GONE);
//            timerText.setText("Service Requested");
//            startTimer((10*60*1000), reqId);
            if(orderCnt < 4) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        if (!isClosed) {
                            isNewOrder = true;
                            new GetOrderHistory().execute(Constants.TRACK_ORDER_URL + reqId);
                        }

                    }
                }, 25000);
            }
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                JSONObject parent = new JSONObject();
                if (!isClosed && orderCnt<4) {
                    JSONArray mainItem = new JSONArray();
                    try {

                        orderCnt = 3;
                        JSONObject mainObj = new JSONObject();
                        mainObj.put("ReqId", reqId);
                        mainObj.put("Comment", "re-order");
                        Log.i("TAG Service", "" + orderCnt);
                        mainObj.put("IsGlobal", true);
                        mainObj.put("Spid", ServiceProviderAdapter.selectedSP);
                        mainItem.put(mainObj);


                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    try {
                        parent.put("UpdRequest", mainItem);
                    }catch (JSONException je){
                        je.printStackTrace();
                    }
                    Log.i("TAG", parent.toString());
                    requestNewOrder.setVisibility(View.GONE);
                    new UpdateOrder().execute(parent.toString());
                }

            }
        }, 120000);


//        if(reqStatus.equalsIgnoreCase("new")){
//            if(isNewOrder) {
//                timerText.setText("Service Requested");
//                requestNewOrder.setVisibility(View.VISIBLE);
//            }
//        }else if(reqStatus.equalsIgnoreCase("cancel")){
//            requestNewOrder.setVisibility(View.GONE);
//            liveTrackBtn.setVisibility(View.GONE);
//            cancelButton.setVisibility(View.GONE);
//            timerText.setText("Request Cancelled");
//            spAction.setText("Request has been cancelled`");
//        }else if(reqStatus.equalsIgnoreCase("accepted")){
//            requestNewOrder.setVisibility(View.GONE);
//            liveTrackBtn.setVisibility(View.VISIBLE);
//            timerText.setText("Request Accepted");
//            spAction.setText("Request accepted by Service provider`");
//        }else if(reqStatus.equalsIgnoreCase("close")){
//            requestNewOrder.setVisibility(View.GONE);
//            liveTrackBtn.setVisibility(View.GONE);
//            cancelButton.setVisibility(View.GONE);
//            timerText.setText("Request Completed");
//            spAction.setText("Your Request had completed.");
//        }


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CancelOrder().execute(Constants.CANCEL_ORDER_URL + reqId + "&comment=");
//                countDownTimer.cancel();
            }
        });

        newOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TrackOrder.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        liveTrackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TrackOrder.this, LiveTrackingActivity.class);
                i.putExtra("req_id", reqId);
                i.putExtra("sp_name", spName);
                i.putExtra("sp_number", spNumber);
                startActivity(i);
            }
        });


        if (mSpResponse != null) {
            try {
                JSONObject jo = new JSONObject(mSpResponse);

                try {
                    JSONArray ja = jo.getJSONArray("Success");
                    JSONArray priceArray = new JSONArray();
                    for (int i = 0; i < ja.length(); i++) {
                        ServiceProvider sp = new ServiceProvider();
                        JSONObject jo1 = ja.getJSONObject(i);

                        sp.setSpID(jo1.getString("sp_SPID"));
                        sp.setSpName(jo1.getString("SpName"));
                        sp.setSpDesc(jo1.getString("SpDesc"));
                        sp.setSpAddress(jo1.getString("SpAddress"));
                        sp.setStoreLat(jo1.getString("StoreLatitude"));
                        sp.setStoreLong(jo1.getString("StoreLongitude"));
                        sp.setSpLat(jo1.getString("SpLatitude"));
                        sp.setSpLong(jo1.getString("SpLongitude"));
                        sp.setDistance(jo1.getString("Distance"));
                        sp.setRating(jo1.getString("Points"));
                        sp.setPhone(jo1.getString("Phone"));
                        sp.setPrice(jo1.getString("TPrice"));
                        sp.setSpPrices(jo1.getJSONArray("spPrice"));
                        sp.setOnJourney(jo1.getString("OnJourney"));
                        JSONArray ja1 = jo1.getJSONArray("SPWorkingDet");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        sp.setStartTime(jo2.getString("StartTime"));
                        sp.setEndTime(jo2.getString("EndTime"));
                        sp.setIsShift(jo2.getString("IsInShifts"));
                        sp.setStartTime2(jo2.getString("StartTime2"));
                        sp.setEndTime2(jo2.getString("EndTime2"));
                        sp.setIsClose(jo2.getString("IsClose"));

                        if(jo2.getString("IsClose").equals("false")) {
                            if ((jo2.getString("IsInShifts").equals("true"))) {
                                if (isWorkingTime(jo2.getString("StartTime"), jo2.getString("EndTime"))) {
                                    spList.add(sp);
                                } else {
                                    if (isWorkingTime(jo2.getString("StartTime2"), jo2.getString("EndTime2"))) {
                                        spList.add(sp);
                                    }
                                }
                            } else if ((jo2.getString("IsInShifts").equals("false"))) {
                                if (isWorkingTime(jo2.getString("StartTime"), jo2.getString("EndTime"))) {
                                    spList.add(sp);
                                }
                            }
                        }
                    }

                    if (spList.size() > 0) {
                        Collections.sort(spList, ServiceProvider.distanceSort);

                    } else {
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrder.this, android.R.style.Theme_Material_Light_Dialog));
//
//                        // set title
//                        alertDialogBuilder.setTitle("ECO Clean");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("No service providers in your area")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//
//
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrder.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        vert.setVisibility(View.GONE);
                        no.setVisibility(View.GONE);
                        yes.setText("Ok");
                        desc.setText("No service providers in your area");

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }

                } catch (JSONException je) {
                    je.printStackTrace();
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrder.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                    // set title
//                    alertDialogBuilder.setTitle("ECO Clean");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("No service providers in your area")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("اوريجانو");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage(result)
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrder.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    yes.setText("Ok");
                    desc.setText("No service providers in your area");

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //Start Countodwn method
    private void startTimer(int noOfMinutes, final String reqId) {
        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                timerText.setText(""+hms);

            }

            public void onFinish() {
//                recreate();
                timerText.setText("Please wait...");
            }
        }.start();

    }
    public boolean isWorkingTime(String stTime, String etTime){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat ExptimeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        String expDateString = selectedDate+" "+selectedTime;
        expDateString = expDateString.replace("-", "/");

        String startTime = stTime.replace(" ","");
        String endTime = etTime.replace(" ","");

        if (endTime.equals("12:00AM")) {
            endTime = "11:59PM";
        }

        Date serverDate = null;
        Date end24Date = null;
        Date start24Date = null;
        Date current24Date = null;
        Date dateToday = null;
        Calendar dateStoreClose = Calendar.getInstance();
        try {
            end24Date = timeFormat.parse(endTime);
            start24Date = timeFormat.parse(startTime);
            serverDate = dateFormat.parse(expDateString);
            dateToday = dateFormat1.parse(expDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dateStoreClose.setTime(dateToday);
        dateStoreClose.add(Calendar.DATE, 1);
        String current24 = timeFormat1.format(serverDate);
        String end24 =timeFormat1.format(end24Date);
        String start24 = timeFormat1.format(start24Date);
        String startDateString = dateFormat1.format(dateToday);
        String endDateString = dateFormat1.format(dateToday);
        String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
        dateStoreClose.add(Calendar.DATE, -2);
        String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

        Date startDate = null;
        Date endDate = null;

        try {
            end24Date = timeFormat1.parse(end24);
            start24Date = timeFormat1.parse(start24);
            current24Date = timeFormat1.parse(current24);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] parts2 = start24.split(":");
        int startHour = Integer.parseInt(parts2[0]);
        int startMinute = Integer.parseInt(parts2[1]);

        String[] parts = end24.split(":");
        int endHour = Integer.parseInt(parts[0]);
        int endMinute = Integer.parseInt(parts[1]);

        String[] parts1 = current24.split(":");
        int currentHour = Integer.parseInt(parts1[0]);
        int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


        if(startTime.contains("AM") && endTime.contains("AM")){
            if(startHour < endHour){
                startDateString = startDateString+ " "+ startTime;
                endDateString = endDateString+"  " + endTime;
                try {
                    startDate = dateFormat2.parse(startDateString);
                    endDate = dateFormat2.parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else if(startHour > endHour){
                if(expDateString.contains("AM")){
                    if(currentHour > endHour){
                        startDateString = startDateString + " " + startTime;
                        endDateString = endDateTomorrow + "  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        startDateString = endDateYesterday + " " + startTime;
                        endDateString = endDateString + "  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    startDateString = startDateString + " " + startTime;
                    endDateString = endDateTomorrow + "  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else if(startTime.contains("AM") && endTime.contains("PM")){
            startDateString = startDateString+ " "+ startTime;
            endDateString = endDateString+"  " + endTime;
            try {
                startDate = dateFormat2.parse(startDateString);
                endDate = dateFormat2.parse(endDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else if(startTime.contains("PM") && endTime.contains("AM")){
            if(expDateString.contains("AM")){
                if(currentHour <= endHour){
                    startDateString = endDateYesterday+ " "+ startTime;
                    endDateString = endDateString+"  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }else{
                    startDateString = startDateString+ " "+ startTime;
                    endDateString = endDateTomorrow+"  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }else{

                startDateString = startDateString+ " "+ startTime;
                endDateString = endDateTomorrow+"  " + endTime;
                try {
                    startDate = dateFormat2.parse(startDateString);
                    endDate = dateFormat2.parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }else if(startTime.contains("PM") && endTime.contains("PM")){
            startDateString = startDateString+ " "+ startTime;
            endDateString = endDateString+"  " + endTime;
            try {
                startDate = dateFormat2.parse(startDateString);
                endDate = dateFormat2.parse(endDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        String serverDateString = dateFormat2.format(serverDate);

        try {
            serverDate = dateFormat2.parse(serverDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.i("TAG DATE" , ""+ startDate);
        Log.i("TAG DATE1" , ""+ endDate);
        Log.i("TAG DATE2" , ""+ serverDate);
        Log.i("TAG DATE2" , ""+ startTime);
        Log.i("TAG DATE2" , ""+ endTime);

        if(serverDate.after(startDate) && serverDate.before(endDate)) {
            Log.i("TAG Visible", "true");
            return true;
        }else {
            return false;
        }
    }

    public class CancelOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        String distanceResponse;
        InputStream inputStream = null;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrder.this);
            pDialog = ProgressDialog.show(TrackOrder.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(params[0]);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
//                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", ""+response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(TrackOrder.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    try {
                        JSONObject jo= new JSONObject(result);
                        String sResult = jo.getString("Success");
                        Toast.makeText(TrackOrder.this, "Request cancelled successfully.", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(TrackOrder.this, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            if(pDialog != null) {
                pDialog.dismiss();
            }
            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }


    public class GetOrderHistory extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrder.this);
            dialog = ProgressDialog.show(TrackOrder.this, "",
                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(TrackOrder.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject success = jo.getJSONObject("Success");
//                                JSONObject spDetails = success.getJSONObject("spDetails");


                                JSONArray ja = success.getJSONArray("Rdetails");
                                for(int i = 0; i< ja.length(); i++){
                                    ToDo todo = new ToDo();
                                    ArrayList<HistoryCars> carList = new ArrayList<>();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    todo.setAddress(jo1.getString("address"));
                                    todo.setExpDate(jo1.getString("ExpectedDT"));
                                    todo.setFullName(jo1.getString("fullname"));
                                    todo.setMobile(jo1.getString("mobile"));
                                    todo.setSpName(jo1.getString("spname"));
                                    spName = jo1.getString("spname");
                                    spNumber = jo1.getString("spContact");
                                    todo.setTotalPrice(jo1.getString("TotalPrice"));
                                    todo.setLatitude(jo1.getString("latitude"));
                                    todo.setLongitude(jo1.getString("longitude"));
                                    todo.setPaymentMode(jo1.getString("pMode"));
                                    todo.setRequestStatus(jo1.getString("RequestStatus"));
                                    reqStatus = jo1.getString("RequestStatus");
                                    JSONArray carsArray = jo1.getJSONArray("CarDetails");
                                    for(int j = 0; j < carsArray.length(); j++){
                                        HistoryCars c = new HistoryCars();
                                        ArrayList<CarDetails> carDetailsList = new ArrayList<>();
                                        JSONArray ja1 = carsArray.getJSONArray(j);
                                        for(int k = 0; k < ja1.length(); k++){
                                            JSONObject jo2 = ja1.getJSONObject(k);
                                            CarDetails cd = new CarDetails();
                                            cd.setSize(jo2.getString("size"));
                                            cd.setsType(jo2.getString("sType"));
                                            cd.setsSubType(jo2.getString("sSubtype"));
                                            cd.setBrandName(jo2.getString("brandname"));
                                            cd.setModelName(jo2.getString("modelname"));
                                            cd.setPrice(jo2.getString("price"));
                                            carDetailsList.add(cd);
                                        }
                                        c.setCarDetailsList(carDetailsList);
                                        carList.add(c);
                                    }
                                    todo.setCarsList(carList);
//                                    historyList.add(todo);





                                    String status = jo1.getString("RequestStatus");
                                    if(status.equalsIgnoreCase("new")){
                                        if(isNewOrder) {
                                            if(orderCnt >= 4){
                                                timerText.setText("Request Accepted");
                                                spAction.setText("Request accepted by Service provider`");

                                                newOrderButton.setVisibility(View.VISIBLE);
                                            }else {
                                                timerText.setText("Service Requested");
                                                requestNewOrder.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }else if(status.equalsIgnoreCase("cancel")){
                                        requestNewOrder.setVisibility(View.GONE);
                                        liveTrackBtn.setVisibility(View.GONE);
                                        cancelButton.setVisibility(View.GONE);
                                        timerText.setText("Request Cancelled");
                                        spAction.setText("Request has been cancelled`");

                                        newOrderButton.setVisibility(View.VISIBLE);
                                    }else if(status.equalsIgnoreCase("accepted")){
                                        requestNewOrder.setVisibility(View.GONE);
                                        liveTrackBtn.setVisibility(View.VISIBLE);
                                        timerText.setText("Request Accepted");
                                        spAction.setText("Request accepted by Service provider");

                                        newOrderButton.setVisibility(View.VISIBLE);
                                    }else if(status.equalsIgnoreCase("ontheway")){
                                        requestNewOrder.setVisibility(View.GONE);
                                        liveTrackBtn.setVisibility(View.VISIBLE);
                                        timerText.setText("Request On the way");
                                        spAction.setText("Service provider is on the way to your address");

                                        newOrderButton.setVisibility(View.VISIBLE);
                                    }else if(status.equalsIgnoreCase("close")){
                                        requestNewOrder.setVisibility(View.GONE);
                                        liveTrackBtn.setVisibility(View.GONE);
                                        cancelButton.setVisibility(View.GONE);
                                        timerText.setText("Request Completed");
                                        spAction.setText("Your Request had completed.");

                                        newOrderButton.setVisibility(View.VISIBLE);
                                    }
                                }



                            }catch (JSONException je){
                                je.printStackTrace();
                                Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

//            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }



    public class UpdateOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrder.this);
            dialog = ProgressDialog.show(TrackOrder.this, "",
                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.UPDATE_ORDER_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(TrackOrder.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
                        String order_number = "";
                        try{
                            JSONObject jo= new JSONObject(result);
                            order_number = jo.getString("Success");

                            orderCnt = orderCnt+1;
                            orderPrefEditor.putInt("order_cnt", orderCnt);
                            orderPrefEditor.commit();
                            isNewOrder = false;
                            Log.i("TAG Service run", ""+orderCnt);
                            if(orderCnt >= 4){
                                timerText.setText("Request Accepted");
                                spAction.setText("Request accepted by Service provider`");
                                Toast.makeText(TrackOrder.this, "Request Accepted", Toast.LENGTH_SHORT).show();
                            }else {
                                new GetOrderHistory().execute(Constants.TRACK_ORDER_URL + reqId);
                                Toast.makeText(TrackOrder.this, "Request sent successfully", Toast.LENGTH_SHORT).show();
                                if(fromHistory) {
                                    if(reqStatus.equalsIgnoreCase("new")) {
                                        requestNewOrder.setVisibility(View.GONE);
                                        liveTrackBtn.setVisibility(View.GONE);
                                        cancelButton.setVisibility(View.GONE);
                                        timerText.setText("Your request not accepted");
                                    }
                                }else {
                                    mDilatingDotsProgressBar.show(500);
                                    requestNewOrder.setVisibility(View.GONE);
                                    timerText.setText("Service Requested");
//            startTimer((10*60*1000), reqId);
                                    if(orderCnt < 4) {
                                        new Handler().postDelayed(new Runnable() {

                                            @Override
                                            public void run() {
                                                // This method will be executed once the timer is over
                                                if (!isClosed) {
                                                    isNewOrder = true;
                                                    new GetOrderHistory().execute(Constants.TRACK_ORDER_URL + reqId);
                                                }

                                            }
                                        }, 15000);
                                    }
                                }
                            }


                        }catch (JSONException je){
                            je.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
//            if(alertDialog1 != null){
//                alertDialog1.dismiss();
//            }

            super.onPostExecute(result);

        }

    }




    public void showDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrder.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.service_provider;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);

//        final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
        ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.sp_cancel);
        ListView spListView = (ListView) dialogView.findViewById(R.id.sp_listview);
        TextView doneBtn = (TextView) dialogView.findViewById(R.id.done_btn);
        final TextView filterDistance = (TextView) dialogView.findViewById(R.id.filter_distance);
        final TextView filterRate = (TextView) dialogView.findViewById(R.id.filter_rating);
        final TextView filterPrice = (TextView) dialogView.findViewById(R.id.filter_price);


        Collections.sort(spList, ServiceProvider.distanceSort);
        mAdapter = new ServiceProviderAdapter(TrackOrder.this, spList, "En", selectedDate, selectedTime);
        spListView.setAdapter(mAdapter);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                JSONObject parent = new JSONObject();
                if(ServiceProviderAdapter.selectedSP == null){
                    Toast.makeText(TrackOrder.this, "Please select a service provider", Toast.LENGTH_SHORT).show();
                }else {
                    JSONArray mainItem = new JSONArray();
                        try {

                            JSONObject mainObj = new JSONObject();
                            mainObj.put("ReqId", reqId);
                            mainObj.put("Comment", "re-order");
                            Log.i("TAG Service", ""+orderCnt);
                            if(orderCnt == 3){
                                mainObj.put("IsGlobal", true);
                            }else {
                                mainObj.put("IsGlobal", false);
                            }
                            mainObj.put("Spid", ServiceProviderAdapter.selectedSP);
                            mainItem.put(mainObj);


                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    try {
                        parent.put("UpdRequest", mainItem);
                    }catch (JSONException je){
                        je.printStackTrace();
                    }
                    Log.i("TAG", parent.toString());
                    requestNewOrder.setVisibility(View.GONE);
                    new UpdateOrder().execute(parent.toString());
                }
            }
        });

        filterDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDistance.setBackgroundResource(R.drawable.oval_shape);
                filterDistance.setTextColor(Color.parseColor("#FFFFFF"));
                filterRate.setBackgroundResource(R.drawable.round_rect);
                filterRate.setTextColor(Color.parseColor("#000000"));
                filterPrice.setBackgroundResource(R.drawable.round_rect);
                filterPrice.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.distanceSort);
                mAdapter.notifyDataSetChanged();
            }
        });

        filterRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterRate.setBackgroundResource(R.drawable.oval_shape);
                filterRate.setTextColor(Color.parseColor("#FFFFFF"));
                filterDistance.setBackgroundResource(R.drawable.round_rect);
                filterDistance.setTextColor(Color.parseColor("#000000"));
                filterPrice.setBackgroundResource(R.drawable.round_rect);
                filterPrice.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.ratingSort);
                mAdapter.notifyDataSetChanged();
            }
        });

        filterPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterPrice.setBackgroundResource(R.drawable.oval_shape);
                filterPrice.setTextColor(Color.parseColor("#FFFFFF"));
                filterRate.setBackgroundResource(R.drawable.round_rect);
                filterRate.setTextColor(Color.parseColor("#000000"));
                filterDistance.setBackgroundResource(R.drawable.round_rect);
                filterDistance.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.priceSort);
                mAdapter.notifyDataSetChanged();
            }
        });


        alertDialog = dialogBuilder.create();
        alertDialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }


    @Override
    protected void onResume() {
        super.onResume();
        isClosed = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isClosed = true;
//        countDownTimer.cancel();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
