package com.cs.ecoclean.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.ecoclean.Constants;
import com.cs.ecoclean.JSONParser;
import com.cs.ecoclean.R;
import com.cs.ecoclean.adapters.HistoryAdapter;
import com.cs.ecoclean.model.CarDetails;
import com.cs.ecoclean.model.HistoryCars;
import com.cs.ecoclean.model.ToDo;
import com.cs.ecoclean.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by CS on 01-12-2016.
 */
public class OrderHistory extends AppCompatActivity {

    ArrayList<ToDo> historyList = new ArrayList<>();
    ListView historyListView;
    ImageView backBtn;
//    TextView historyCnt;

    HistoryAdapter mAdapter;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId, rating;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_history);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        userId = userPrefs.getString("userId", null);

        historyListView = (ListView) findViewById(R.id.history_listview);
        backBtn = (ImageView) findViewById(R.id.back_btn);
//        historyCnt = (TextView) findViewById(R.id.history_cnt);

        mAdapter = new HistoryAdapter(OrderHistory.this, historyList);
        historyListView.setAdapter(mAdapter);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        historyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(OrderHistory.this, OrderDetails.class);
                i.putExtra("req_id", historyList.get(position).getReqId());
                i.putExtra("screen", "history");
                startActivity(i);
            }
        });

        new GetOrderHistory().execute(Constants.GET_ORDER_HISTORY+userId);
    }


    public class GetOrderHistory extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistory.this);
            dialog = new MaterialDialog.Builder(OrderHistory.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            historyList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderHistory.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderHistory.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject success = jo.getJSONObject("Success");
//                                JSONObject spDetails = success.getJSONObject("spDetails");


                                JSONArray ja = success.getJSONArray("Rdetails");
                                for(int i = 0; i< ja.length(); i++){
                                    ToDo todo = new ToDo();
                                    ArrayList<HistoryCars> carList = new ArrayList<>();
                                    ArrayList<String> imagesList = new ArrayList<>();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    todo.setAddress(jo1.getString("address"));
                                    todo.setExpDate(jo1.getString("ExpectedDT"));
                                    todo.setReqId(jo1.getString("ReqId"));
                                    todo.setFullName(jo1.getString("fullname"));
                                    todo.setMobile(jo1.getString("mobile"));
                                    todo.setTotalPrice(jo1.getString("TotalPrice"));
                                    todo.setLatitude(jo1.getString("latitude"));
                                    todo.setLongitude(jo1.getString("longitude"));
                                    todo.setPaymentMode(jo1.getString("pMode"));
                                    todo.setGrandTotal(jo1.getString("GrandTotal"));
                                    todo.setOldPenalty(jo1.getString("OldCancelPenalty"));
                                    todo.setRequestStatus(jo1.getString("RequestStatus"));
                                    todo.setSpName(jo1.getString("spname"));
                                    todo.setOrderNumber(jo1.getString("InvoiceNo"));
                                    JSONArray carsArray = jo1.getJSONArray("CarDetails");
                                    for(int j = 0; j < carsArray.length(); j++){
                                        HistoryCars c = new HistoryCars();
                                        ArrayList<CarDetails> carDetailsList = new ArrayList<>();
                                        JSONArray ja1 = carsArray.getJSONArray(j);
                                        for(int k = 0; k < ja1.length(); k++){
                                            JSONObject jo2 = ja1.getJSONObject(k);
                                            CarDetails cd = new CarDetails();
                                            cd.setSize(jo2.getString("size"));
                                            cd.setsType(jo2.getString("sType"));
                                            cd.setsSubType(jo2.getString("sSubtype"));
                                            cd.setBrandName(jo2.getString("brandname"));
                                            cd.setModelName(jo2.getString("modelname"));
                                            cd.setPrice(jo2.getString("price"));
                                            carDetailsList.add(cd);
                                        }
                                        c.setCarDetailsList(carDetailsList);
                                        carList.add(c);
                                    }

                                    JSONArray imagesArrey = jo1.getJSONArray("Imagedetails");
                                    for(int l = 0; l<imagesArrey.length(); l++){
                                        JSONObject jo3 = imagesArrey.getJSONObject(l);
                                        String image = Constants.IMAGE_URL+jo3.getString("ImageName");
                                        imagesList.add(image);
                                    }
                                    todo.setImagesList(imagesList);
                                    todo.setCarsList(carList);
                                    historyList.add(todo);
                                }



                            }catch (JSONException je){
                                je.printStackTrace();
                                Toast.makeText(OrderHistory.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(OrderHistory.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            mAdapter.notifyDataSetChanged();
//            historyCnt.setText(""+historyList.size());
            super.onPostExecute(result);

        }

    }
}
