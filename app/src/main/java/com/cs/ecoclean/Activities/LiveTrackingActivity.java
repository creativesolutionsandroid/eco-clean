package com.cs.ecoclean.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.ecoclean.Constants;
import com.cs.ecoclean.JSONParser;
import com.cs.ecoclean.R;
import com.cs.ecoclean.adapters.CarsAdapter;
import com.cs.ecoclean.model.CarDetails;
import com.cs.ecoclean.model.HistoryCars;
import com.cs.ecoclean.model.ToDo;
import com.cs.ecoclean.widgets.NetworkUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CS on 06-01-2017.
 */
public class LiveTrackingActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
    SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
    SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
    SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);

    ImageView backBtn;
    TextView spNameTxt, callNow, deliveryTime;
    String spPhone, driverPhone;
    Double storeLat, storeLong, userLat, userLong, spLat, spLong;
    String driverName, driverNumber, driverId, orderId, expectedTime;
    private TextView mapStandard, mapSatellite, mapHybrid;

    private String timeResponse = null;

    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private Timer timer = new Timer();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_tracking);

        driverName = getIntent().getStringExtra("sp_name");
        driverNumber = getIntent().getStringExtra("sp_number");
        orderId = getIntent().getStringExtra("req_id");


        deliveryTime = (TextView) findViewById(R.id.delivery_time);
        callNow = (TextView) findViewById(R.id.call_now);
        spNameTxt = (TextView) findViewById(R.id.driver_name);
        backBtn = (ImageView) findViewById(R.id.back_btn1);
        mapStandard = (TextView) findViewById(R.id.map_standard);
        mapSatellite = (TextView) findViewById(R.id.map_satellite);
        mapHybrid = (TextView) findViewById(R.id.map_hybrid);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(LiveTrackingActivity.this);
        spNameTxt.setText(WordUtils.capitalizeFully(driverName));

//        if(language.equalsIgnoreCase("En")){
//            expTime.setText("Arrival Time : "+expectedTime);
//        }else if(language.equalsIgnoreCase("Ar")){
//            expTime.setText("الوقت المتوقع  : "+expectedTime);
//        }

        callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +"+" +driverNumber));
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"  +"+" +driverNumber));
                    startActivity(intent);
                }
            }
        });

        timer.schedule(new MyTimerTask(), 60000, 60000);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new getTrackingDetails().execute(Constants.TRACK_ORDER_URL + orderId);
                }
            });
        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"  +"+" +driverNumber));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(LiveTrackingActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapHybrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapStandard.setBackgroundColor(0);
                mapSatellite.setBackgroundColor(0);
                mapHybrid.setBackgroundColor(Color.parseColor("#2196F3"));
                mapStandard.setTextColor(Color.parseColor("#2196F3"));
                mapSatellite.setTextColor(Color.parseColor("#2196F3"));
                mapHybrid.setTextColor(Color.parseColor("#FFFFFF"));
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            }
        });

        mapStandard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapStandard.setBackgroundColor(Color.parseColor("#2196F3"));
                mapSatellite.setBackgroundColor(0);
                mapHybrid.setBackgroundColor(0);
                mapStandard.setTextColor(Color.parseColor("#FFFFFF"));
                mapSatellite.setTextColor(Color.parseColor("#2196F3"));
                mapHybrid.setTextColor(Color.parseColor("#2196F3"));
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
        });

        mapSatellite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapStandard.setBackgroundColor(0);
                mapSatellite.setBackgroundColor(Color.parseColor("#2196F3"));
                mapHybrid.setBackgroundColor(0);
                mapStandard.setTextColor(Color.parseColor("#2196F3"));
                mapSatellite.setTextColor(Color.parseColor("#FFFFFF"));
                mapHybrid.setTextColor(Color.parseColor("#2196F3"));
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            }
        });
        new getTrackingDetails().execute(Constants.TRACK_ORDER_URL + orderId);
//        LatLng driver = new LatLng(driverLat, driverLong);
//        LatLng store = new LatLng(storeLat, storeLong);
//        LatLng user = new LatLng(userLat, userLong);
//
//        mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));
//        mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.store_marker)));
//        mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
    }

    public class getTrackingDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog dialog;
        String networkStatus;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);
            dialog = ProgressDialog.show(LiveTrackingActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser.getJSONFromUrl(params[0]);
                Log.i("TAG", "user response: " + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LiveTrackingActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(LiveTrackingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject success = jo.getJSONObject("Success");

                                JSONArray ja = success.getJSONArray("Rdetails");
                                for(int i = 0; i< ja.length(); i++){
                                    ToDo todo = new ToDo();
                                    ArrayList<HistoryCars> carList = new ArrayList<>();
                                    JSONObject jo1 = ja.getJSONObject(i);

                                    todo.setAddress(jo1.getString("address"));
                                    todo.setReqId(jo1.getString("ReqId"));
                                    todo.setExpDate(jo1.getString("ExpectedDT"));
                                    todo.setFullName(jo1.getString("fullname"));
                                    todo.setSpName(jo1.getString("spname"));
                                    todo.setMobile(jo1.getString("mobile"));
                                    todo.setTotalPrice(jo1.getString("TotalPrice"));
                                    todo.setLatitude(jo1.getString("latitude"));
                                    todo.setLongitude(jo1.getString("longitude"));
//                                    mLat = jo1.getString("latitude");
//                                    mLong = jo1.getString("longitude");
                                    todo.setPaymentMode(jo1.getString("pMode"));
                                    todo.setRequestStatus(jo1.getString("RequestStatus"));
                                    JSONArray carsArray = jo1.getJSONArray("CarDetails");

                                    userLat = jo1.getDouble("latitude");
                                    userLong = jo1.getDouble("longitude");
                                    spLat = jo1.getDouble("spLat");
                                    spLong = jo1.getDouble("spLog");
                                    spPhone = jo1.getString("spContact");
                                    if(mMap != null){
                                        LatLng driver = new LatLng(spLat, spLong);
                                        LatLng user = new LatLng(userLat, userLong);

                                        mMap.clear();

                                        mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.sp_marker)));
                                        mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
                                        mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
                                        mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
                                    }

                                }


                            }catch (JSONException je){
                                je.printStackTrace();
                                Toast.makeText(LiveTrackingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(LiveTrackingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
            new getTrafficTime().execute();
            super.onPostExecute(result);

        }

    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        String distanceResponse;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + userLat +","+ userLong +"&destinations="+ spLat +","+ spLong+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDbUS1BGaNS_-UlStOypbm_FhnCPKFHK7Q");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LiveTrackingActivity.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        String value = jo3.getString("value");
//                        if(language.equalsIgnoreCase("En")) {
                            deliveryTime.setText("Arriving in : "+secs);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
}
