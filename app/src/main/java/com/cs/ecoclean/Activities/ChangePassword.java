package com.cs.ecoclean.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.ecoclean.Constants;
import com.cs.ecoclean.R;
import com.cs.ecoclean.widgets.NetworkUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by CS on 12-07-2016.
 */
public class ChangePassword extends AppCompatActivity {

    private String response12 = null;
    MaterialDialog dialog;

    EditText oldPassword, newPassword, confirmPassword;
    RelativeLayout submit;
    ImageView backBtn;
    SharedPreferences userPrefs;
    String response, userId;
    SharedPreferences languagePrefs;
    String language;
    AlertDialog customDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
//        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.change_password);
//        }else if(language.equalsIgnoreCase("Ar")){
//            setContentView(R.layout.change_password_arabic);
//        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        submit = (RelativeLayout) findViewById(R.id.change_password_button);
        backBtn = (ImageView) findViewById(R.id.back_btn1);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPwd = oldPassword.getText().toString();
                String newPwd = newPassword.getText().toString();
                String confirmPwd = confirmPassword.getText().toString();
                if(oldPwd.length() == 0){
                    oldPassword.setError("Please enter Old Password");
                }else if(newPwd.length() == 0){
                    newPassword.setError("Please enter New Password");
                }else if(newPwd.length() < 8){
                    if (language.equalsIgnoreCase("En")) {
                        newPassword.setError("Password must be at least 8 characters");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        newPassword.setError("ثمانية حروف على الأقل");
                    }
                }else if(confirmPwd.length() == 0){
                    confirmPassword.setError("Please retype password");
                }else if(!newPwd.equals(confirmPwd)){
                    confirmPassword.setError("Passwords not match, please retype");
                }else{
                    new ChangePasswordResponse().execute(userId, oldPwd, newPwd);
                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class ChangePasswordResponse extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String  networkStatus;
        InputStream is = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ChangePassword.this);
            dialog = new MaterialDialog.Builder(ChangePassword.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                        // 2. make POST request to the given URL
//                        HttpGet httpPost = new HttpGet(Constants.CHANGE_PASSWORD_URL+arg0[0]+"?OldPsw="+arg0[1]+"&NewPsw="+arg0[2]);

                    // Making HTTP request
                    try {
                        // defaultHttpClient
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        HttpGet httpPost = new HttpGet(Constants.CHANGE_PASSWORD_URL+arg0[0]+"?OldPsw="+arg0[1]+"&NewPsw="+arg0[2]);

                        httpPost.setHeader("Accept", "application/xml");
                        httpPost.setHeader("Content-type", "application/xml");
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        is = httpEntity.getContent();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                is, "iso-8859-1"), 8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        is.close();
                        response12 = sb.toString();
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            }else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(result1 != null){
                if(result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangePassword.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    yes.setText("Ok");
                    desc.setText("Communication Error! Please check the internet connection?");

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);
//
//                    // set title
//                    alertDialogBuilder.setTitle("ECO Clean");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("Communication Error! Please check the internet connection?")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
                }else{
                    if (response12.contains("false")) {
                        dialog.dismiss();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangePassword.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        vert.setVisibility(View.GONE);
                        no.setVisibility(View.GONE);
                        yes.setText("Ok");
                        desc.setText("Communication Error! Please check the internet connection?");

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);
//
//                        // set title
//                        alertDialogBuilder.setTitle("ECO Clean");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("Invalid Old Password")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
                    } else if(response12.contains("true")){
                        dialog.dismiss();
                        Toast.makeText(ChangePassword.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                        onBackPressed();

                    }
                }
            }else {
                dialog.dismiss();
            }

            super.onPostExecute(result1);
        }
    }
}
