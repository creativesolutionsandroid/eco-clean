package com.cs.ecoclean.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.cs.ecoclean.R;

/**
 * Created by CS on 17-11-2016.
 */
public class CarProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_profile_popup);
    }
}
