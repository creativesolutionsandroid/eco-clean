package com.cs.ecoclean.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.ecoclean.Constants;
import com.cs.ecoclean.JSONParser;
import com.cs.ecoclean.R;
import com.cs.ecoclean.adapters.CountriesAdapter;
import com.cs.ecoclean.model.Countries;
import com.cs.ecoclean.widgets.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CS on 14-07-2016.
 */
public class ForgotPassword extends AppCompatActivity {

    ImageView backBtn;
    private String response = null;
    TextView mCancel;
    Button mSend;
    EditText mEmail;
    SharedPreferences languagePrefs;
    String language;
    AlertDialog customDialog;

    private ImageView mCountryFlag;
    private ArrayList<Countries> countriesList = new ArrayList<>();
    private Countries countries;
    private CountriesAdapter mCountriesAdapter;
    private String countryCode = "+966";
    private int countryFlag;
    private int phoneNumberLength;

    public static final String[] SMS_RECIEVER = {
            android.Manifest.permission.RECEIVE_SMS
    };
    public static final int SMS_REQUEST = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
//        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.forgot_password);
//        }else if(language.equalsIgnoreCase("Ar")){
//            setContentView(R.layout.forgot_password_arabic);
//        }

        backBtn = (ImageView) findViewById(R.id.back_btn1);

        mSend = (Button) findViewById(R.id.send_button);
        mEmail = (EditText) findViewById(R.id.forgot_email);
        mCountryFlag = (ImageView) findViewById(R.id.country_flag);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if(!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)){
                requestPermissions(SMS_RECIEVER, SMS_REQUEST);
            }
        }

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString();
                if(email.length() == 0){
                    mEmail.setError("Please Enter Phone Number");
                }else{
                    new GetVerificationDetails().execute(Constants.FORGOT_PASSWORD_URL+countryCode.replace("+","")+email);
                }

            }
        });

        mCountryFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog();
            }
        });

        prepareArrayLits();
        mCountriesAdapter = new CountriesAdapter(this,countriesList);
    }



    public class GetVerificationDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPassword.this);
            dialog = new MaterialDialog.Builder(ForgotPassword.this)
                    .title("ECO Clean")
                    .content("Sending OTP")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ForgotPassword.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(ForgotPassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {

                            JSONObject jo = new JSONObject(result);
                            try {
                                JSONObject jo1 = jo.getJSONObject("Success");
                                String otp = jo1.getString("OTP");
                                String mobile = jo1.getString("MobileNo");
                                dialog.dismiss();
                                Intent loginIntent = new Intent(ForgotPassword.this, VerifyOTP.class);
                                loginIntent.putExtra("phone_number", mobile);
                                loginIntent.putExtra("OTP", otp);
                                loginIntent.putExtra("forgot", true);
                                startActivity(loginIntent);
                            }catch (JSONException je){

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ForgotPassword.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                vert.setVisibility(View.GONE);
                                no.setVisibility(View.GONE);
                                yes.setText("Ok");
                                desc.setText("Mobile Number not registered");

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);

//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPassword.this);
//
//
////                            if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("ECO Clean");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("Mobile Number not registered")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                                onBackPressed();
//                                            }
//                                        });
////                            }else if(language.equalsIgnoreCase("Ar")){
////                                // set title
////                                alertDialogBuilder.setTitle("د. كيف");
////
////                                // set dialog message
////                                alertDialogBuilder
////                                        .setMessage("سوف يصلك منا قريباً بريد الكتروني موضح فيه كيف تفعل كلمة المرور")
////                                        .setCancelable(false)
////                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                            public void onClick(DialogInterface dialog, int id) {
////                                                dialog.dismiss();
////                                            }
////                                        });
////                            }
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(ForgotPassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    /* Method used to prepare the ArrayList,
    * Same way, you can also do looping and adding object into the ArrayList.
    */
    public void prepareArrayLits()
    {

        AddObjectToList(R.drawable.bh, "Bahrain", "+973", "18");
        AddObjectToList(R.drawable.bd, "Bangladesh", "+880", "19");
        AddObjectToList(R.drawable.cn, "China", "+86", "45");
        AddObjectToList(R.drawable.cy, "Cyprus", "+537" ,"57");
        AddObjectToList(R.drawable.eg, "Egypt", "+20", "64");
        AddObjectToList(R.drawable.fr, "France", "+33", "74");
        AddObjectToList(R.drawable.de, "Germany", "+49", "81");
        AddObjectToList(R.drawable.in, "India", "+91", "101");
        AddObjectToList(R.drawable.id, "Indonesia", "+62", "102");

        AddObjectToList(R.drawable.ir, "Iran", "+98", "103");
        AddObjectToList(R.drawable.iq, "Iraq", "+964", "104");
        AddObjectToList(R.drawable.jo, "Jordan", "+962", "112");
        AddObjectToList(R.drawable.kw, "Kuwait", "+965", "118");
        AddObjectToList(R.drawable.lb, "Lebanon", "+961", "122");
        AddObjectToList(R.drawable.my, "Malaysia", "+60", "133");
        AddObjectToList(R.drawable.ma, "Morocco", "+212", "147");
        AddObjectToList(R.drawable.np, "Nepal", "+977", "152");
        AddObjectToList(R.drawable.om, "Oman", "+968", "164");

        AddObjectToList(R.drawable.pk, "Pakistan", "+92", "165");
        AddObjectToList(R.drawable.ps, "Palestinian Territories", "", "167");
        AddObjectToList(R.drawable.ph, "Philippines", "+63", "172");
        AddObjectToList(R.drawable.qa, "Qatar", "+974", "177");
        AddObjectToList(R.drawable.sa, "Saudi Arabia", "+966", "190");
        AddObjectToList(R.drawable.sg, "Singapore", "+65", "195");
        AddObjectToList(R.drawable.es, "Spain", "+34", "202");
        AddObjectToList(R.drawable.lk, "Sri Lanka", "+94", "203");
        AddObjectToList(R.drawable.sd, "Sudan", "+249", "204");

        AddObjectToList(R.drawable.tr, "Syria", "+963", "210");
        AddObjectToList(R.drawable.tr, "Turkey", "+90", "221");
        AddObjectToList(R.drawable.ae, "United Arab Emirates", "+971", "227");
        AddObjectToList(R.drawable.gb, "United Kingdom", "+44", "228");
        AddObjectToList(R.drawable.us, "United States", "+1", "229");
        AddObjectToList(R.drawable.ye, "Yemen", "+967", "240");
    }

    // Add one item into the Array List
    public void AddObjectToList(int image, String countryName, String countryCode, String countrrId)
    {
        countries = new Countries();
        countries.setCountryName(countryName);
        countries.setCountryFlag(image);
        countries.setCountryCode(countryCode);
        countries.setCountryID(countrrId);
        countriesList.add(countries);
    }

    public void dialog() {

        final Dialog dialog2 = new Dialog(this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.setContentView(R.layout.countries_list);
        dialog2.setCanceledOnTouchOutside(true);
        ListView lv = (ListView) dialog2.findViewById(R.id.countries_list);
        lv.setAdapter(mCountriesAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                countryCode = countriesList.get(arg2).getCountryCode();
                countryFlag = countriesList.get(arg2).getCountryFlag();

                if(countryCode.equalsIgnoreCase("+966")){
                    int maxLength = 9;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mEmail.setFilters(fArray);
                    phoneNumberLength = 9;
                }else{
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    mEmail.setFilters(fArray);
                    phoneNumberLength = 10;
                }

                mCountryFlag.setImageResource(countriesList.get(arg2).getCountryFlag());
//                mCountryCode.setText("" + countriesList.get(arg2).getCountryCode());

                dialog2.dismiss();

            }
        });


        dialog2.show();

    }

}
