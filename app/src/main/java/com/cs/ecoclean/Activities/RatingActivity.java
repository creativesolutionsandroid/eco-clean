package com.cs.ecoclean.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.ecoclean.Constants;
import com.cs.ecoclean.R;
import com.cs.ecoclean.adapters.CommentsAdapter;
import com.cs.ecoclean.model.Rating;
import com.cs.ecoclean.model.RatingDetails;
import com.cs.ecoclean.widgets.CustomGridView;
import com.cs.ecoclean.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by CS on 28-02-2017.
 */

public class RatingActivity extends Activity {

    ArrayList<Rating> ratingsArrayList = new ArrayList<>();
    AlertDialog alertDialog2;
    LinearLayout feedback_layout;
    MaterialRatingBar rateBar;
    TextView  rate_text, wrong_text;
    public static EditText comment;
    ImageView close_dialog;
    Button submit;
    String userId, spId, reqId, comments;
    float rating;

    ArrayList<RatingDetails> rating1 = new ArrayList<>();
    ArrayList<RatingDetails> rating2 = new ArrayList<>();
    ArrayList<RatingDetails> rating3 = new ArrayList<>();
    ArrayList<RatingDetails> rating4 = new ArrayList<>();
    ArrayList<RatingDetails> rating5 = new ArrayList<>();
    CustomGridView gridView;

    public static ArrayList<Integer> pos = new ArrayList<>();
    public static ArrayList<String> str = new ArrayList<>();
    public static ArrayList<String> ratingId = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_layout);

        userId = getIntent().getStringExtra("userid");
        spId = getIntent().getStringExtra("spid");
        reqId = getIntent().getStringExtra("reqid");
        rating = getIntent().getFloatExtra("rating",0);
        ratingsArrayList = (ArrayList<Rating>) getIntent().getSerializableExtra(
        "array");

        feedback_layout = (LinearLayout) findViewById(R.id.what_went_wrong);
        submit = (Button) findViewById(R.id.submit);
        close_dialog = (ImageView) findViewById(R.id.rating_cancel);
        rate_text = (TextView)findViewById(R.id.rating_text);
        wrong_text = (TextView)findViewById(R.id.wrong_text);
        rateBar = (MaterialRatingBar) findViewById(R.id.ratingBar_layout);
        comment = (EditText) findViewById(R.id.comment);
        comment.setVisibility(View.GONE);

        gridView = (CustomGridView) findViewById(R.id.remarks_grid);

        try {
            str.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        LayerDrawable stars = (LayerDrawable) rateBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#76C8FC"), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.parseColor("#76C8FC"), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.parseColor("#76C8FC"), PorterDuff.Mode.SRC_ATOP);

        rateBar.setRating(rating);

        for(int i=0; i<ratingsArrayList.get(0).getRatingDetails().size(); i++) {

            if (ratingsArrayList.get(0).getRatingDetails().get(i).getRating().equals("1")) {
                RatingDetails r1 = new RatingDetails();
                r1.setCommentId(ratingsArrayList.get(0).getRatingDetails().get(i).getCommentId());
                r1.setRemarks(ratingsArrayList.get(0).getRatingDetails().get(i).getRemarks());
                r1.setRating(ratingsArrayList.get(0).getRatingDetails().get(i).getRating());
                rating1.add(r1);
            } else if (ratingsArrayList.get(0).getRatingDetails().get(i).getRating().equals("2")) {
                RatingDetails r1 = new RatingDetails();
                r1.setCommentId(ratingsArrayList.get(0).getRatingDetails().get(i).getCommentId());
                r1.setRemarks(ratingsArrayList.get(0).getRatingDetails().get(i).getRemarks());
                r1.setRating(ratingsArrayList.get(0).getRatingDetails().get(i).getRating());
                rating2.add(r1);
            } else if (ratingsArrayList.get(0).getRatingDetails().get(i).getRating().equals("3")) {
                RatingDetails r1 = new RatingDetails();
                r1.setCommentId(ratingsArrayList.get(0).getRatingDetails().get(i).getCommentId());
                r1.setRemarks(ratingsArrayList.get(0).getRatingDetails().get(i).getRemarks());
                r1.setRating(ratingsArrayList.get(0).getRatingDetails().get(i).getRating());
                rating3.add(r1);
            } else if (ratingsArrayList.get(0).getRatingDetails().get(i).getRating().equals("4")) {
                RatingDetails r1 = new RatingDetails();
                r1.setCommentId(ratingsArrayList.get(0).getRatingDetails().get(i).getCommentId());
                r1.setRemarks(ratingsArrayList.get(0).getRatingDetails().get(i).getRemarks());
                r1.setRating(ratingsArrayList.get(0).getRatingDetails().get(i).getRating());
                rating4.add(r1);
            } else if (ratingsArrayList.get(0).getRatingDetails().get(i).getRating().equals("5")) {
                RatingDetails r1 = new RatingDetails();
                r1.setCommentId(ratingsArrayList.get(0).getRatingDetails().get(i).getCommentId());
                r1.setRemarks(ratingsArrayList.get(0).getRatingDetails().get(i).getRemarks());
                r1.setRating(ratingsArrayList.get(0).getRatingDetails().get(i).getRating());
                rating5.add(r1);
            }
        }

        if(rating == 1){
            rate_text.setText("Terrible");
            wrong_text.setText("Tell us what went wrong ?");
            feedback_layout.setVisibility(View.VISIBLE);
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating1, "En");
            gridView.setAdapter(mAdapter);
        }
        else if(rating == 2){
            rate_text.setText("Bad");
            wrong_text.setText("Tell us what went wrong ?");
            feedback_layout.setVisibility(View.VISIBLE);
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating2, "En");
            gridView.setAdapter(mAdapter);
        }
        else if(rating == 3){
            rate_text.setText("Ok");
            wrong_text.setText("Tell us what went wrong ?");
            feedback_layout.setVisibility(View.VISIBLE);
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating3, "En");
            gridView.setAdapter(mAdapter);
        }
        else if(rating == 4){
            rate_text.setText("Good");
            wrong_text.setText("Where do you feel we should improve ?");
            feedback_layout.setVisibility(View.VISIBLE);
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating4, "En");
            gridView.setAdapter(mAdapter);
        }
        else if(rating == 5){
            rate_text.setText("Excellent");
            wrong_text.setText("What do you like best about us ?");
            feedback_layout.setVisibility(View.VISIBLE);
            CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating5, "En");
            gridView.setAdapter(mAdapter);
        }

        rateBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating_star, boolean fromUser) {

                if(rating_star<1){
                    rate_text.setText("Terrible");
                    wrong_text.setText("Tell us what went wrong ?");
                    rateBar.setRating(1);
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating1, "En");
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
                else if(rating_star == 1){
                    rate_text.setText("Terrible");
                    wrong_text.setText("Tell us what went wrong ?");
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating1, "En");
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
                else if(rating_star == 2){
                    rate_text.setText("Bad");
                    wrong_text.setText("Tell us what went wrong ?");
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating2, "En");
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
                else if(rating_star == 3){
                    rate_text.setText("Ok");
                    wrong_text.setText("Tell us what went wrong ?");
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating3, "En");
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
                else if(rating_star == 4){
                    rate_text.setText("Good");
                    wrong_text.setText("Where do you feel we should improve ?");
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating4, "En");
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
                else if(rating_star == 5){
                    rate_text.setText("Excellent");
                    wrong_text.setText("What do you like best about us ?");
                    try {
                        pos.clear();
                        str.clear();
                        ratingId.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comment.setText("");
                    comment.setVisibility(View.GONE);
                    CommentsAdapter mAdapter = new CommentsAdapter(RatingActivity.this, rating5, "En");
                    gridView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean vaild = false;
                if(rateBar.getRating() > 0) {
                    JSONObject parent = new JSONObject();
                    try {

                        JSONArray mainItem = new JSONArray();

                        if (str != null && str.size() > 0 ) {
                            for (int i = 0; i < str.size(); i++) {
                                String c;
                                if (str.get(i).equals("Other")) {
                                    if (comment.getText().toString().length() > 0) {
                                        c = comment.getText().toString();
                                    } else {
                                        c = str.get(i);
                                    }
                                } else {
                                    c = str.get(i);
                                }
                                if(comments!=null && comments.length()>0){

                                    comments = comments+","+c;
                                }
                                else{
                                    comments = c;
                                }
                            }
                                JSONObject mainObj = new JSONObject();
                                mainObj.put("UserId", userId);
                                mainObj.put("SpId", spId);
                                mainObj.put("Points", rateBar.getRating());
                                mainObj.put("SerReqId", reqId);
                                mainObj.put("Comments", comments);
                                mainItem.put(mainObj);
                                parent.put("spRating", mainItem);
                        }
                        else{
                            JSONObject mainObj = new JSONObject();
                            mainObj.put("UserId", userId);
                            mainObj.put("SpId", spId);
                            mainObj.put("Points", rateBar.getRating());
                            mainObj.put("SerReqId", reqId);
                            mainObj.put("Comments", "");
                            mainItem.put(mainObj);
                            parent.put("spRating", mainItem);
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    Log.i("TAG", "" + parent.toString());
                    new SubmitRating().execute(parent.toString());
                }else {

                }
            }
        });
    }
    public class SubmitRating extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        String response;
        MaterialDialog dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(RatingActivity.this);
            dialog = new MaterialDialog.Builder(RatingActivity.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.RATING_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(RatingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            Toast.makeText(RatingActivity.this, "Rating submitted successfully.", Toast.LENGTH_SHORT).show();
                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
