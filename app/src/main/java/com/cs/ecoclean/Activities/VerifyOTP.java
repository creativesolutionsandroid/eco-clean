package com.cs.ecoclean.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.ecoclean.Constants;
import com.cs.ecoclean.MainActivity;
import com.cs.ecoclean.R;
import com.cs.ecoclean.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

/**
 * Created by CS on 02-11-2016.
 */
public class VerifyOTP extends AppCompatActivity implements OTPListener {
    private String response = null;
    TextView mMobileNumber;
    EditText mActivationCode, mCountryCode;
    Button mVerifySubmit;
    String phoneNumber, userId, otp, userData;
    boolean forgot;
    int countryFlag;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    AlertDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_random_number);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        phoneNumber = getIntent().getExtras().getString("phone_number").replace(" ","");
        otp = getIntent().getStringExtra("OTP");
        userData = getIntent().getStringExtra("user_data");
        forgot = getIntent().getBooleanExtra("forgot", false);
        OtpReader.bind(this,"CS Test");
//        OtpReader.bind(this,"KACSMS");
        mMobileNumber = (TextView) findViewById(R.id.mobile_number);
        mActivationCode = (EditText) findViewById(R.id.verify_code);
        mVerifySubmit = (Button) findViewById(R.id.submit);

        mMobileNumber.setText(phoneNumber);

        if(!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(VerifyOTP.this, "SMS read permission denied, Please enter OTP manually", Toast.LENGTH_LONG).show();
        }

        mVerifySubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String activationCode = mActivationCode.getText().toString();

                if(activationCode.length()==0){
                    mActivationCode.setError("Please enter OTP");
                    mActivationCode.requestFocus();
                }else {

                    if (activationCode.equals(otp)) {
                            if(forgot){
                                Intent i = new Intent(VerifyOTP.this, ResetPassword.class);
                                i.putExtra("mobile", phoneNumber);
                                startActivity(i);
                                finish();
                            }else {
                                new RegisterUser().execute(userData);
                            }
                    }else{
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VerifyOTP.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        vert.setVisibility(View.GONE);
                        no.setVisibility(View.GONE);
                        yes.setText("Ok");
                        desc.setText("Enter Correct OTP");

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                }
            }
        });
    }
    @Override
    public void otpReceived(String smsText) {
        //Do whatever you want to do with the text
        Log.d("Otp",smsText);
        String[] str = null;
        try {
            str = smsText.split(":");
//            if(str[1].contains("'")){
//                str[1].replace("'","");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
            mActivationCode.setText(""+str[1]);
            mActivationCode.setSelection(mActivationCode.length());
        if (str[1].equals(otp)) {
            if (forgot) {
                Intent i = new Intent(VerifyOTP.this, ResetPassword.class);
                i.putExtra("mobile", phoneNumber);
                startActivity(i);
                finish();
            } else {
                new RegisterUser().execute(userData);
            }
        }

//        Toast.makeText(this,"Got "+smsText,Toast.LENGTH_LONG).show();
    }

    public class RegisterUser extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOTP.this);
            dialog = new MaterialDialog.Builder(VerifyOTP.this)
                    .title("ECO Clean")
                    .content("Verifying OTP")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.REGISTRATION_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(VerifyOTP.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(VerifyOTP.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");


                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

//                                    if(isVerified) {
                                        userPrefEditor.putString("login_status","loggedin");
                                        userPrefEditor.commit();
                                       Intent loginIntent = new Intent(VerifyOTP.this, MainActivity.class);
                                        loginIntent.putExtra("phone_number", mobile);
                                        startActivity(loginIntent);
                                    finish();
//                                    }



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(VerifyOTP.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("ECO Clean");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(result)
//                                            .setCancelable(false)
//                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("اوريجانو");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage(result)
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VerifyOTP.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                vert.setVisibility(View.GONE);
                                no.setVisibility(View.GONE);
                                yes.setText("Ok");
                                desc.setText(""+result);

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(VerifyOTP.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
