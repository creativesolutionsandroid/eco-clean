package com.cs.ecoclean.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.ecoclean.Constants;
import com.cs.ecoclean.JSONParser;
import com.cs.ecoclean.MainActivity;
import com.cs.ecoclean.R;
import com.cs.ecoclean.adapters.CarsAdapter;
import com.cs.ecoclean.adapters.ServiceProviderAdapter1;
import com.cs.ecoclean.model.CarDetails;
import com.cs.ecoclean.model.HistoryCars;
import com.cs.ecoclean.model.Rating;
import com.cs.ecoclean.model.RatingDetails;
import com.cs.ecoclean.model.ServiceProvider;
import com.cs.ecoclean.model.ToDo;
import com.cs.ecoclean.widgets.NetworkUtil;
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.cs.ecoclean.adapters.ServiceProviderAdapter1.sp;

/**
 * Created by CS on 05-01-2017.
 */
public class OrderDetails extends AppCompatActivity {
    TextView address, dateTime, price, spName;
    ListView carListView;
    ImageView backBtn;
    CarsAdapter carsAdapter;
//    Button submit;
    TextView  rateTitle;

    ArrayList<ToDo> todoList = new ArrayList<>();
    String reqId;
    boolean fromHistory = false, isBackPressed;

    TextView newRequest;
    int mDialogCount=0;
    String status;
    AlertDialog customDialog;
    Boolean cancel_clicked = false;
    String requestType;



    private DilatingDotsProgressBar mDilatingDotsProgressBar;
    private CountDownTimer countDownTimer;
    TextView timerText, spAction, orderNumber;
    Button cancelButton;
    LinearLayout liveTrackBtn, trackStatusLayout;
    RelativeLayout rateLayout;
    String reqStatus, spNumber = "", fromScreen = "";
    private String mSpResponse;
    boolean isClosed = false, isNewOrder = false;
    ArrayList<ServiceProvider> spList = new ArrayList<>();
    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefEditor;
//    int orderCnt = 1;
    String selectedDate, selectedTime;
    String userId, spId;
    String mSpName = "";

    ServiceProviderAdapter1 mAdapter;
    AlertDialog alertDialog, alertDialog1, alertDialog2;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    private Timer timer = new Timer();
    private Timer timer1 = new Timer();
    ArrayList<Rating> ratingsArray = new ArrayList<>();

    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel;
    TextView service_amt,rush_amt, cancellation_amt, net_total, done_btn, price_text, view_details, adjust, adjust_amt, rush_text;
    ImageView rush_close;
    LinearLayout reciept_layout, view_layout;
    boolean isFromTimer = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_details);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefEditor = orderPrefs.edit();
        mSpResponse = orderPrefs.getString("sp_response", null);
        selectedDate = orderPrefs.getString("sel_date","");
        selectedTime = orderPrefs.getString("sel_time","");

        try {
            reqId = getIntent().getExtras().getString("req_id");
            fromHistory = getIntent().getBooleanExtra("from_history",false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            fromScreen = getIntent().getExtras().getString("screen");
        } catch (Exception e) {
            e.printStackTrace();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("push"));

        if(reqId==null) {
            reqId = orderPrefs.getString("req_id", "");
        }
        carListView = (ListView) findViewById(R.id.car_listview);
        address = (TextView) findViewById(R.id.address);
        dateTime = (TextView) findViewById(R.id.exp_datetime);
        spName = (TextView) findViewById(R.id.service_provider);
        price = (TextView) findViewById(R.id.total_price);
        orderNumber = (TextView) findViewById(R.id.order_number);
//        trackOrder = (RelativeLayout) findViewById(R.id.track_btn);
        newRequest = (TextView) findViewById(R.id.new_request_btn);
        rush_text = (TextView) findViewById(R.id.rate_text);
        backBtn = (ImageView) findViewById(R.id.back_btn1);
        trackStatusLayout = (LinearLayout) findViewById(R.id.track_status_layout);
        ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBar1);
        cancel = (TextView) findViewById(R.id.skip_rating);
        rate_layout = (RelativeLayout) findViewById(R.id.rate_layout);
        rate_layout.setVisibility(View.GONE);
//        skipRating = (TextView) findViewById(R.id.skip_rating);
        rateTitle = (TextView) findViewById(R.id.dialog_title);
        service_amt = (TextView) findViewById(R.id.service_amt);
        adjust = (TextView) findViewById(R.id.adjust);
        adjust_amt = (TextView) findViewById(R.id.adjust_amount);
        rush_amt = (TextView) findViewById(R.id.rush_charge);
        cancellation_amt = (TextView) findViewById(R.id.cancellation_amount);
        net_total = (TextView) findViewById(R.id.net_total);
        done_btn = (TextView) findViewById(R.id.done_btn);
        price_text = (TextView) findViewById(R.id.price_text);
        view_details = (TextView) findViewById(R.id.view_details);
        rush_close = (ImageView) findViewById(R.id.receipt_close);
        reciept_layout = (LinearLayout) findViewById(R.id.receipt_layout);
        view_layout= (LinearLayout) findViewById(R.id.price);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fromScreen.equalsIgnoreCase("confirm") || (fromScreen.equalsIgnoreCase("history"))) {
                    isBackPressed = true;
                    finish();
                }
                else{
                    Intent intent = new Intent(OrderDetails.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }

            }
        });

        rush_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reciept_layout.setVisibility(View.GONE);
            }
        });

        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reciept_layout.setVisibility(View.GONE);
            }
        });

        view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reciept_layout.setVisibility(View.VISIBLE);
            }
        });

        view_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reciept_layout.setVisibility(View.VISIBLE);
            }
        });


        new GetNotification().execute(Constants.TRACK_ORDER_URL + reqId+"&userId="+userId);

        if(fromHistory){
            backBtn.setVisibility(View.VISIBLE);
        }

//        trackOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(todoList.size()>0) {
//                    isTrackClicked = true;
//                    Intent i = new Intent(OrderDetails.this, TrackOrder.class);
//                    i.putExtra("req_id", reqId);
//                    i.putExtra("sp_name", todoList.get(0).getSpName());
//                    i.putExtra("sp_number", todoList.get(0).getSpNumber());
//                    i.putExtra("order_status", todoList.get(0).getRequestStatus());
//                    i.putExtra("from_history", fromHistory);
//                    startActivity(i);
//                }
//            }
//        });
//
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                // This method will be executed once the timer is over
//                if(!isTrackClicked) {
//                    trackOrder.performClick();
//                }
//
//            }
//        }, 10000);

        newRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OrderDetails.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });






        mDilatingDotsProgressBar = (DilatingDotsProgressBar) findViewById(R.id.progress);

        timerText = (TextView) findViewById(R.id.timer_text);
        cancelButton = (Button) findViewById(R.id.cancel_button);
        spAction = (TextView) findViewById(R.id.sp_action);
        liveTrackBtn = (LinearLayout) findViewById(R.id.live_track_btn);

        liveTrackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OrderDetails.this, LiveTrackingActivity.class);
                i.putExtra("req_id", reqId);
                i.putExtra("sp_name", mSpName);
                i.putExtra("sp_number", spNumber);
                startActivity(i);
            }
        });


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                yes.setText("Yes");
                no.setText("No");

                String status = orderPrefs.getString("order_status", "new");
                if(status.equalsIgnoreCase("accepted")){
                    desc.setText("Your request already accepted\nDo you want to cancel request?");
                }
                else if(status.equalsIgnoreCase("Temporary")){
                    desc.setText("Your request already accepted\nDo you want to cancel request?");
                }
                else if(status.equalsIgnoreCase("StartService")){
                    desc.setText("Your service provider is already on the way\nDo you want to cancel request?");
                }
                else{
                    desc.setText("Do you want to cancel request?");
                }
//                desc.setText("No service providers in your area");

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                        cancel_clicked = true;
                        new GetNotification().execute(Constants.TRACK_ORDER_URL + reqId+"&userId="+userId);
//                        new GetStatus().execute(Constants.TRACK_ORDER_URL + reqId+"&userId="+userId);
//                        new CancelOrder().execute(Constants.CANCEL_ORDER_URL + reqId + "&comment=cacelled");
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

//                countDownTimer.cancel();
            }
        });


//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                // This method will be executed once the timer is over
//                String status = orderPrefs.getString("order_status", "new");
//                JSONObject parent = new JSONObject();
//                if (!isClosed && orderCnt<4 && status.equalsIgnoreCase("new")) {
//                    JSONArray mainItem = new JSONArray();
//                    try {
//
//                        orderCnt = 3;
//                        JSONObject mainObj = new JSONObject();
//                        mainObj.put("ReqId", reqId);
//                        mainObj.put("Comment", "re-order");
//                        Log.i("TAG Service", "" + orderCnt);
//                        mainObj.put("IsGlobal", true);
//                        mainObj.put("RequestStatus", "Temporary");
//                        mainObj.put("Spid", ServiceProviderAdapter.selectedSP);
//                        mainItem.put(mainObj);
//
//
//                    } catch (JSONException je) {
//                        je.printStackTrace();
//                    }
//                    try {
//                        parent.put("UpdRequest", mainItem);
//                    }catch (JSONException je){
//                        je.printStackTrace();
//                    }
//                    Log.i("TAG", parent.toString());
//                    new UpdateOrder().execute(parent.toString());
//                }
//
//            }
//        }, 120000);

//        if(orderCnt < 4) {
//            timer.schedule(new MyTimerTask(), 25000);
//        }

//        if(orderCnt >= 4){
//            isNewOrder = true;
//            timer1.schedule(new MyTimerTask(), 30000, 30000);
//            mDilatingDotsProgressBar.show(500);
//            timerText.setText("Request Accepted");
//            spAction.setText("Request accepted by Service provider`");
//            backBtn.setVisibility(View.VISIBLE);
//            liveTrackBtn.setVisibility(View.GONE);
//            newRequest.setVisibility(View.VISIBLE);
//        }


//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(rateBar.getRating() > 0) {
//                    slideUpDown(rateLayout);
//                    JSONObject parent = new JSONObject();
//                    try {
//
//                        JSONArray mainItem = new JSONArray();
//
//                        JSONObject mainObj = new JSONObject();
//                        mainObj.put("UserId", userId);
//                        mainObj.put("SpId", spId);
//                        mainObj.put("Points", rateBar.getRating());
//                        mainObj.put("SerReqId", reqId);
//                        mainObj.put("Comments", "");
//
//                        mainItem.put(mainObj);
//
//                        parent.put("spRating", mainItem);
//                    } catch (JSONException je) {
//                        je.printStackTrace();
//                    }
//                    Log.i("TAG", "" + parent.toString());
//                    new SubmitRating().execute(parent.toString());
//                }else {
//
//                }
//            }
//        });


        try {
            if (mSpResponse != null  && fromScreen.equalsIgnoreCase("confirm")) {
                try {
                    JSONObject jo = new JSONObject(mSpResponse);

                    try {
                        JSONArray ja = jo.getJSONArray("Success");
                        JSONArray priceArray = new JSONArray();
                        for (int i = 0; i < ja.length(); i++) {
                            ServiceProvider sp = new ServiceProvider();
                            JSONObject jo1 = ja.getJSONObject(i);

                            sp.setSpID(jo1.getString("sp_SPID"));
                            sp.setSpName(jo1.getString("SpName"));
                            sp.setSpDesc(jo1.getString("SpDesc"));
                            sp.setSpAddress(jo1.getString("SpAddress"));
                            sp.setStoreLat(jo1.getString("StoreLatitude"));
                            sp.setStoreLong(jo1.getString("StoreLongitude"));
                            sp.setSpLat(jo1.getString("SpLatitude"));
                            sp.setSpLong(jo1.getString("SpLongitude"));
                            sp.setDistance(jo1.getString("Distance"));
                            sp.setRating(jo1.getString("Points"));
                            sp.setPhone(jo1.getString("Phone"));
                            sp.setPrice(jo1.getString("TPrice"));

                            int fCharge = 0;
                            int j = 0;
                            try {
                                JSONArray rushArray = jo1.getJSONArray("RushHoursDetails");
                                for(int k =0; k<rushArray.length(); k++){
                                    JSONObject rushObj1 = rushArray.getJSONObject(k);
                                    int charge1 = rushObj1.getInt("RushCharge");
                                    if(charge1>fCharge){
                                        j=k;
                                        fCharge = charge1;
                                    }
                                }
                                JSONObject rushObj = rushArray.getJSONObject(j);
                                sp.setRushId(rushObj.getString("ID"));
                                sp.setRushCityId(rushObj.getString("CityId"));
                                sp.setRushStart(rushObj.getString("StartFrom"));
                                sp.setRushEnd(rushObj.getString("EndAt"));
                                sp.setRushCharge(rushObj.getString("RushCharge"));
                                sp.setRushType(rushObj.getString("ServiceType"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //                        sp.setRushChargeValue(jo1.getString("RushHourValue"));
    //                        sp.setGrandTotal(jo1.getString("GrandTotal"));
    //                        sp.setOldPenalty(jo1.getString("OldPenalty"));
                            sp.setSpPrices(jo1.getJSONArray("spPrice"));
                            sp.setOnJourney(jo1.getString("OnJourney"));
                            JSONArray ja1 = jo1.getJSONArray("SPWorkingDet");
                            JSONObject jo2 = ja1.getJSONObject(0);
                            sp.setStartTime(jo2.getString("StartTime"));
                            sp.setEndTime(jo2.getString("EndTime"));
                            sp.setIsShift(jo2.getString("IsInShifts"));
                            sp.setStartTime2(jo2.getString("StartTime2"));
                            sp.setEndTime2(jo2.getString("EndTime2"));
                            sp.setIsClose(jo2.getString("IsClose"));

                            int dcount = 0;
                            int pos1 = 0;
                            try {
                                JSONArray rushArray = jo1.getJSONArray("Discounts");
                                for(int k =0; k<rushArray.length(); k++){
                                    JSONObject rushObj1 = rushArray.getJSONObject(k);
                                    int charge1 = rushObj1.getInt("DiscountPercentage");
                                    if(charge1>dcount){
                                        pos1=k;
                                        dcount = charge1;
                                    }
                                }
                                JSONObject rushObj = rushArray.getJSONObject(pos1);
                                sp.setDisId(rushObj.getString("id"));
                                sp.setDisStart(rushObj.getString("StartDate"));
                                sp.setDisEnd(rushObj.getString("EndDate"));
                                sp.setDisPercent(rushObj.getString("DiscountPercentage"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if(jo2.getString("IsClose").equals("false")) {
                                if ((jo2.getString("IsInShifts").equals("true"))) {
                                    if (isWorkingTime(jo2.getString("StartTime"), jo2.getString("EndTime"))) {
                                        spList.add(sp);
                                    } else {
                                        if (isWorkingTime(jo2.getString("StartTime2"), jo2.getString("EndTime2"))) {
                                            spList.add(sp);
                                        }
                                    }
                                } else if ((jo2.getString("IsInShifts").equals("false"))) {
                                    if (isWorkingTime(jo2.getString("StartTime"), jo2.getString("EndTime"))) {
                                        spList.add(sp);
                                    }
                                }
                            }
                        }

                        if (spList.size() > 0) {
                            Collections.sort(spList, ServiceProvider.distanceSort);

                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
    //
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!isClosed) {
            if (fromScreen.equalsIgnoreCase("confirm")) {
                isNewOrder = true;
            }
        }
        if(fromScreen.equalsIgnoreCase("confirm")){
            timer.schedule(new MyTimerTask(), 15000, 15000);
            timer1.schedule(new MyTimerTask1(), 150000);
        }else {
            backBtn.setVisibility(View.VISIBLE);
            timer.schedule(new MyTimerTask(), 15000, 15000);
        }


    }


    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isClosed) {
                        if(fromScreen.equalsIgnoreCase("confirm")) {
                            isNewOrder = true;
                        }
                        new GetNotification().execute(Constants.TRACK_ORDER_URL + reqId+"&userId="+userId);
                    }
                }
            });
        }
    }


    private class MyTimerTask1 extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String status = orderPrefs.getString("order_status", "new");
                    JSONObject parent = new JSONObject();
                    if (!isClosed && status.equalsIgnoreCase("new")) {
                        JSONArray mainItem = new JSONArray();
                        try {

//                            orderCnt = 3;
                            JSONObject mainObj = new JSONObject();
                            mainObj.put("ReqId", reqId);
                            mainObj.put("Comment", "re-order");
                            mainObj.put("IsGlobal", true);
                            mainObj.put("RequestStatus", "Temporary");
                            mainObj.put("Spid", spId);
                            mainItem.put(mainObj);

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                        try {
                            parent.put("UpdRequest", mainItem);
                        }catch (JSONException je){
                            je.printStackTrace();
                        }
                        Log.i("TAG", parent.toString());
                        if(alertDialog1 != null){
                            alertDialog1.dismiss();
                        }
                        isFromTimer = true;
                        new UpdateOrder().execute(parent.toString());
                    }
                }
            });
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            if(message.contains("Not Responding")) {
                String[] ids = message.split(":");
                Log.i("TAG", "mDialogCount " + mDialogCount);
                if (ids[1].equalsIgnoreCase(reqId)) {
                    if (mDialogCount == 0) {
                        showDialog1();
                        mDialogCount = mDialogCount + 1;
                    }
                }
            }
            else{
                new GetNotification().execute(Constants.TRACK_ORDER_URL + reqId+"&userId="+userId);
            }
        }
    };

    public class CancelOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        MaterialDialog dialog;
        String  networkStatus;
        String distanceResponse;
        InputStream inputStream = null;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetails.this);
            dialog = new MaterialDialog.Builder(OrderDetails.this)
                    .title("ECO Clean")
                    .content("Please wait")
                    .progress(true, 0)
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(params[0]);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
//                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", ""+response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderDetails.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    try {
                        cancel_clicked = false;
                        orderPrefEditor.putString("order_status", "cancel");
                        orderPrefEditor.commit();
                        JSONObject jo= new JSONObject(result);
                        String sResult = jo.getString("Success");
                        Toast.makeText(OrderDetails.this, "Request cancelled successfully.", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(OrderDetails.this, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(OrderDetails.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    }
                }

            }
            if(dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);

        }

    }



    public class GetNotification extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
//        ProgressDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetails.this);
//            dialog = ProgressDialog.show(OrderDetails.this, "",
//                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            todoList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    timerText.setText("Connection Error!");
                    spAction.setText("Please check the internet connection");
                    Toast.makeText(OrderDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject success = jo.getJSONObject("Success");

                                JSONArray ja = success.getJSONArray("Rdetails");
                                for(int i = 0; i< ja.length(); i++){
                                    ToDo todo = new ToDo();
                                    ArrayList<HistoryCars> carList = new ArrayList<>();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    userId = jo1.getString("UserID");
                                    spId = jo1.getString("Sp_ID");
                                    String[] orderNo = jo1.getString("InvoiceNo").split("-");
                                    orderNumber.setText(""+orderNo[1]);
                                    todo.setAddress(jo1.getString("address"));
                                    todo.setReqId(jo1.getString("ReqId"));
                                    todo.setExpDate(jo1.getString("ExpectedDT"));
                                    todo.setFullName(jo1.getString("fullname"));
                                    todo.setSpName(jo1.getString("spname"));
                                    todo.setRushChargeValue(jo1.getString("RushHourValue"));
                                    todo.setGrandTotal(jo1.getString("GrandTotal"));
                                    todo.setOldPenalty(jo1.getString("OldPenalty"));
                                    mSpName = jo1.getString("spname");
                                    rateTitle.setText(WordUtils.capitalizeFully("Rate "+mSpName));
                                    todo.setMobile(jo1.getString("mobile"));
                                    todo.setSpNumber(jo1.getString("spContact"));
                                    spNumber = jo1.getString("spContact");
                                    todo.setTotalPrice(jo1.getString("TotalPrice"));
                                    todo.setAgreementPrice(jo1.getString("AgreementPrice"));
                                    todo.setLatitude(jo1.getString("latitude"));
                                    todo.setLongitude(jo1.getString("longitude"));
                                    todo.setRequestType(jo1.getString("RequestType"));
                                    requestType = jo1.getString("RequestType");
//                                    mLat = jo1.getString("latitude");
//                                    mLong = jo1.getString("longitude");
                                    todo.setPaymentMode(jo1.getString("pMode"));
                                    todo.setRequestStatus(jo1.getString("RequestStatus"));
                                    JSONArray carsArray = jo1.getJSONArray("CarDetails");
                                    for(int j = 0; j < carsArray.length(); j++){
                                        HistoryCars c = new HistoryCars();
                                        ArrayList<CarDetails> carDetailsList = new ArrayList<>();
                                        JSONArray ja1 = carsArray.getJSONArray(j);
                                        for(int k = 0; k < ja1.length(); k++){
                                            JSONObject jo2 = ja1.getJSONObject(k);
                                            CarDetails cd = new CarDetails();
                                            cd.setSize(jo2.getString("size"));
                                            cd.setsType(jo2.getString("sType"));
                                            cd.setsSubType(jo2.getString("sSubtype"));
                                            cd.setBrandName(jo2.getString("brandname"));
                                            cd.setModelName(jo2.getString("modelname"));
                                            cd.setPrice(jo2.getString("price"));
                                            carDetailsList.add(cd);
                                        }
                                        c.setCarDetailsList(carDetailsList);
                                        carList.add(c);
                                    }

                                    todo.setCarsList(carList);
                                        todoList.add(todo);


                                    status = jo1.getString("RequestStatus");
                                    if(status.equalsIgnoreCase("new")){
                                        if(isNewOrder) {
//                                            if(mDialogCount==0) {
////                                                showDialog1();
//                                            }
//                                            mDialogCount= mDialogCount +1;
                                            mDilatingDotsProgressBar.show(500);
                                            timerText.setText("Service Requested");
                                            newRequest.setVisibility(View.GONE);
                                        }else{
                                            mDilatingDotsProgressBar.show(500);
                                            timerText.setText("Service Requested");
                                            newRequest.setVisibility(View.GONE);
                                        }
                                        if(cancel_clicked){
                                            new CancelOrder().execute(Constants.CANCEL_ORDER_URL + reqId + "&comment=Cancel");
                                        }
                                    }else if(status.equalsIgnoreCase("cancel")){
                                        orderPrefEditor.putString("order_status", "cancel");
                                        orderPrefEditor.commit();
//                                        timer.cancel();
                                        if(alertDialog1 != null){
                                            alertDialog1.dismiss();
                                        }
                                        liveTrackBtn.setVisibility(View.GONE);
                                        cancelButton.setVisibility(View.GONE);
                                        backBtn.setVisibility(View.VISIBLE);
                                        timerText.setText("Request Cancelled");
                                        spAction.setText("Request has been cancelled");

                                        newRequest.setVisibility(View.VISIBLE);
                                    }else if(status.equalsIgnoreCase("Temporary")){
                                        orderPrefEditor.putString("order_status", "accept");
                                        orderPrefEditor.commit();
//                                        timer.cancel();
                                        if(alertDialog1 != null){
                                            alertDialog1.dismiss();
                                        }
                                        mDilatingDotsProgressBar.show(500);
                                        trackStatusLayout.setBackgroundResource(R.drawable.oval_shape3);
                                        liveTrackBtn.setVisibility(View.GONE);
                                        backBtn.setVisibility(View.VISIBLE);
                                        timerText.setText("Request Accepted");
                                        spAction.setText("Request accepted by Service provider");

                                        newRequest.setVisibility(View.VISIBLE);
                                        if(cancel_clicked){
                                            new CancelOrder().execute(Constants.CANCEL_ORDER_URL + reqId + "&comment=Cancel");
                                        }
                                    }else if(status.equalsIgnoreCase("accepted")){
                                        orderPrefEditor.putString("order_status", "accept");
                                        orderPrefEditor.commit();
//                                        timer.cancel();
                                        if(alertDialog1 != null){
                                            alertDialog1.dismiss();
                                        }
                                        mDilatingDotsProgressBar.show(500);
                                        trackStatusLayout.setBackgroundResource(R.drawable.oval_shape3);
                                        liveTrackBtn.setVisibility(View.GONE);
                                        backBtn.setVisibility(View.VISIBLE);
                                        timerText.setText("Request Accepted");
                                        spAction.setText("Request accepted by Service provider");

                                        newRequest.setVisibility(View.VISIBLE);
                                        if(cancel_clicked){
                                            new CancelOrder().execute(Constants.CANCEL_ORDER_URL + reqId + "&comment=Cancel");
                                        }
                                    }else if(status.equalsIgnoreCase("StartJourney")){
                                        orderPrefEditor.putString("order_status", "StartJourney");
                                        orderPrefEditor.commit();
//                                        timer.cancel();
                                        if(alertDialog1 != null){
                                            alertDialog1.dismiss();
                                        }
                                        mDilatingDotsProgressBar.show(500);
                                        liveTrackBtn.setVisibility(View.VISIBLE);
                                        cancelButton.setVisibility(View.VISIBLE);
                                        backBtn.setVisibility(View.VISIBLE);
                                        trackStatusLayout.setBackgroundResource(R.drawable.oval_shape3);
                                        timerText.setText("Request On the way");
                                        spAction.setText("Service provider is on the way to your address");

                                        newRequest.setVisibility(View.VISIBLE);
                                        if(cancel_clicked){
                                            new CancelOrder().execute(Constants.CANCEL_ORDER_URL + reqId + "&comment=Cancel");
                                        }
                                    }
                                    else if(status.equalsIgnoreCase("StartService")){
                                        orderPrefEditor.putString("order_status", "StartService");
                                        orderPrefEditor.commit();
//                                        timer.cancel();
                                        if(alertDialog1 != null){
                                            alertDialog1.dismiss();
                                        }
                                        mDilatingDotsProgressBar.show(500);
                                        liveTrackBtn.setVisibility(View.GONE);
                                        cancelButton.setVisibility(View.GONE);
                                        backBtn.setVisibility(View.VISIBLE);
                                        trackStatusLayout.setBackgroundResource(R.drawable.oval_shape3);
                                        timerText.setText("Service Started");
                                        spAction.setText("Service provider has started your Service");

                                        newRequest.setVisibility(View.VISIBLE);

                                        if(cancel_clicked){
                                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
                                            // ...Irrelevant code for customizing the buttons and title
                                            LayoutInflater inflater = getLayoutInflater();
                                            int layout = R.layout.alert_dialog;
                                            View dialogView = inflater.inflate(layout, null);
                                            dialogBuilder.setView(dialogView);
                                            dialogBuilder.setCancelable(false);

                                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                                            vert.setVisibility(View.GONE);
                                            no.setVisibility(View.GONE);
                                            yes.setText("Ok");

                                            desc.setText("Your service already started can't be cancel");

//                desc.setText("No service providers in your area");

                                            yes.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    customDialog.dismiss();
                                                    cancel_clicked = false;
                                                    }
                                            });

                                            customDialog = dialogBuilder.create();
                                            customDialog.show();
                                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                            Window window = customDialog.getWindow();
                                            lp.copyFrom(window.getAttributes());
                                            //This makes the dialog take up the full width
                                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                            window.setAttributes(lp);
                                        }
                                    }
                                    else if(status.equalsIgnoreCase("close")){
                                        if(cancel_clicked){
                                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
                                            // ...Irrelevant code for customizing the buttons and title
                                            LayoutInflater inflater = getLayoutInflater();
                                            int layout = R.layout.alert_dialog;
                                            View dialogView = inflater.inflate(layout, null);
                                            dialogBuilder.setView(dialogView);
                                            dialogBuilder.setCancelable(false);

                                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                                            vert.setVisibility(View.GONE);
                                            no.setVisibility(View.GONE);
                                            yes.setText("Ok");

                                            desc.setText("Your service already completed can't be cancel");

//                desc.setText("No service providers in your area");

                                            yes.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    customDialog.dismiss();
                                                    cancel_clicked = false;
                                                }
                                            });

                                            customDialog = dialogBuilder.create();
                                            customDialog.show();
                                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                            Window window = customDialog.getWindow();
                                            lp.copyFrom(window.getAttributes());
                                            //This makes the dialog take up the full width
                                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                            window.setAttributes(lp);
                                        }
                                        if(jo1.getInt("Ratings") == 0){


//                                            JSONArray RatingArray = jo1.getJSONArray("RatingDetails");
//                                            for(int j = 0; j < carsArray.length(); j++){
                                                Rating rating = new Rating();
                                                JSONObject obj = jo1.getJSONObject("RatingDetails");
                                                rating.setSpId(obj.getString("sp_id"));
                                                rating.setReqId(obj.getString("req_id"));
                                                rating.setUserId(obj.getString("user_id"));

//                                            RatingDetails rd = new RatingDetails();
                                            JSONObject jA = obj.getJSONObject("sampleComments");
//                                            JSONObject obj = jA.getJSONObject(0);
                                            rating.setGivenrating("1");
                                            JSONArray ratingArray = jA.getJSONArray("1");
                                            ArrayList<RatingDetails> ratingsArrayList = new ArrayList<>();
                                            for (int j = 0; j < ratingArray.length(); j++) {
                                                JSONObject object1 = ratingArray.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);

                                            rating.setGivenrating("2");
                                            JSONArray ratingArray1 = jA.getJSONArray("2");
                                            for (int j = 0; j < ratingArray1.length(); j++) {
                                                JSONObject object1 = ratingArray1.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);

                                            rating.setGivenrating("3");
                                            JSONArray ratingArray2 = jA.getJSONArray("3");
                                            for (int j = 0; j < ratingArray2.length(); j++) {
                                                JSONObject object1 = ratingArray2.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);

                                            rating.setGivenrating("4");
                                            JSONArray ratingArray3 = jA.getJSONArray("4");
                                            for (int j = 0; j < ratingArray3.length(); j++) {
                                                JSONObject object1 = ratingArray3.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);

                                            rating.setGivenrating("5");
                                            JSONArray ratingArray4 = jA.getJSONArray("5");
                                            for (int j = 0; j < ratingArray4.length(); j++) {
                                                JSONObject object1 = ratingArray4.getJSONObject(j);
                                                RatingDetails ratingDetails = new RatingDetails();
                                                ratingDetails.setCommentId(object1.getString("id"));
                                                ratingDetails.setRemarks(object1.getString("remark"));
                                                ratingDetails.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(ratingDetails);
                                            }
                                            rating.setRatingDetails(ratingsArrayList);
                                            ratingsArray.add(rating);

//                                                ArrayList<RatingDetails> ratingDetailsList = new ArrayList<>();
//                                                JSONArray ja1 = obj.getJSONArray("sampleComments");
//                                                for(int k = 0; k < ja1.length(); k++){
//                                                    JSONObject jo2 = ja1.getJSONObject(k);
//                                                    RatingDetails rd = new RatingDetails();
//                                                    rd.setCommentId(jo2.getString("id"));
//                                                    rd.setRemarks(jo2.getString("remark"));
//                                                    ratingDetailsList.add(rd);
//                                                }
//                                                rating.setRatingDetails(ratingDetailsList);
//                                                ratingsArrayList.add(rating);

                                            rate_layout.setVisibility(View.VISIBLE);
                                            cancel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    rate_layout.setVisibility(View.GONE);
                                                }
                                            });

                                            ratingBar.setRating(0);
                                            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                @Override
                                                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                    if(fromUser) {
                                                        Intent intent = new Intent(OrderDetails.this, RatingActivity.class);
                                                        intent.putExtra("rating", rating);
                                                        intent.putExtra("array", ratingsArray);
                                                        intent.putExtra("userid", ratingsArray.get(0).getUserId());
                                                        intent.putExtra("spid", ratingsArray.get(0).getSpId());
                                                        intent.putExtra("reqid", ratingsArray.get(0).getReqId());
                                                        startActivity(intent);
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                }
                                            });

//                                            showDialog2();
//                                            slideUpDown(rateLayout);
                                        }
                                        orderPrefEditor.putString("order_status", "close");
                                        orderPrefEditor.commit();
                                        timer.cancel();
                                        if(alertDialog1 != null){
                                            alertDialog1.dismiss();
                                        }
                                        mDilatingDotsProgressBar.setVisibility(View.INVISIBLE);
                                        trackStatusLayout.setBackgroundResource(R.drawable.oval_shape3);
                                        liveTrackBtn.setVisibility(View.GONE);
                                        cancelButton.setVisibility(View.GONE);
                                        backBtn.setVisibility(View.VISIBLE);
                                        timerText.setText("Request Completed");
                                        spAction.setText("Your Request had completed.");

                                        newRequest.setVisibility(View.VISIBLE);
                                    }

                                }


                            }catch (JSONException je){
                                je.printStackTrace();
                                Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                timerText.setText("Connection Error!");
                spAction.setText("Please check the internet connection");
                Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }
            if(todoList.size()>0) {
                updateNotification(todoList.get(0));
                carsAdapter = new CarsAdapter(OrderDetails.this, todoList.get(0).getCarsList(), "En");
                carListView.setAdapter(carsAdapter);
            }



            super.onPostExecute(result);

        }

    }

    public class GetStatus extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
//        ProgressDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetails.this);
//            dialog = ProgressDialog.show(OrderDetails.this, "",
//                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            todoList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    timerText.setText("Connection Error!");
                    spAction.setText("Please check the internet connection");
                    Toast.makeText(OrderDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject success = jo.getJSONObject("Success");

                                JSONArray ja = success.getJSONArray("Rdetails");
                                for(int i = 0; i< ja.length(); i++) {
                                    ToDo todo = new ToDo();
                                    ArrayList<HistoryCars> carList = new ArrayList<>();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    status = jo1.getString("RequestStatus");
                                    if (status.equalsIgnoreCase("StartService") || status.equalsIgnoreCase("close")) {
                                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
                                        // ...Irrelevant code for customizing the buttons and title
                                        LayoutInflater inflater = getLayoutInflater();
                                        int layout = R.layout.alert_dialog;
                                        View dialogView = inflater.inflate(layout, null);
                                        dialogBuilder.setView(dialogView);
                                        dialogBuilder.setCancelable(false);

                                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                                        vert.setVisibility(View.GONE);
                                        no.setVisibility(View.GONE);
                                        yes.setText("Ok");

                                        desc.setText("Your request is already accepted by "+mSpName);

//                desc.setText("No service providers in your area");

                                        yes.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                customDialog.dismiss();
                                            }
                                        });

                                        customDialog = dialogBuilder.create();
                                        customDialog.show();
                                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                        Window window = customDialog.getWindow();
                                        lp.copyFrom(window.getAttributes());
                                        //This makes the dialog take up the full width
                                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                        window.setAttributes(lp);
                                    }
                                    else{
                                        new CancelOrder().execute(Constants.CANCEL_ORDER_URL + reqId + "&comment=Cancel");
                                    }
                                }

                            }catch (JSONException je){
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }

            super.onPostExecute(result);
        }

    }




    public class UpdateOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
//        ProgressDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetails.this);
//            dialog = ProgressDialog.show(OrderDetails.this, "",
//                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.UPDATE_ORDER_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {
                        String order_number = "";
                        try{
                            JSONObject jo= new JSONObject(result);
                            order_number = jo.getString("Success");

//                            orderCnt = orderCnt+1;
//                            isNewOrder = false;
//                            Log.i("TAG Service run", ""+orderCnt);
                            if(isFromTimer){
                                timer1.cancel();

                                new GetNotification().execute(Constants.TRACK_ORDER_URL + reqId+"&userId="+userId);
                            }else {
//                                new GetNotification().execute(Constants.TRACK_ORDER_URL + reqId);
                                Toast.makeText(OrderDetails.this, "Request sent successfully", Toast.LENGTH_SHORT).show();

                                    mDilatingDotsProgressBar.show(500);
                                    timerText.setText("Service Requested");
                                    try {
                                        spName.setText(WordUtils.capitalizeFully(sp.getSpName()));
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
//                                    if(orderCnt < 4) {
                                        if (!isClosed) {
                                            isNewOrder = true;
                                            new GetNotification().execute(Constants.TRACK_ORDER_URL + reqId+"&userId="+userId);
                                        }
//                                        new Handler().postDelayed(new Runnable() {
//
//                                            @Override
//                                            public void run() {
//                                                // This method will be executed once the timer is over
//
//
//                                            }
//                                        }, 33000);
//                                    }
                                }

                        }catch (JSONException je){
                            je.printStackTrace();
                        }
                    }
                }

            }else {
                Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }
//            if(alertDialog1 != null){
//                alertDialog1.dismiss();
//            }

            super.onPostExecute(result);

        }

    }



    public void showDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.service_provider;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);

//        final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
        ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.sp_cancel);
        ListView spListView = (ListView) dialogView.findViewById(R.id.sp_listview);
        TextView doneBtn = (TextView) dialogView.findViewById(R.id.done_btn);
        final TextView filterDistance = (TextView) dialogView.findViewById(R.id.filter_distance);
        final TextView filterRate = (TextView) dialogView.findViewById(R.id.filter_rating);
        final TextView filterPrice = (TextView) dialogView.findViewById(R.id.filter_price);


        Collections.sort(spList, ServiceProvider.distanceSort);


        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mDialogCount=0;
            }
        });


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                String status = orderPrefs.getString("order_status", "new");
                if(status.equals("new")) {
                    JSONObject parent = new JSONObject();
                    if (ServiceProviderAdapter1.selectedSP == null) {
                        Toast.makeText(OrderDetails.this, "Please select a service provider", Toast.LENGTH_SHORT).show();
                    } else {
                        JSONArray mainItem = new JSONArray();
                        try {

                            JSONObject mainObj = new JSONObject();
                            mainObj.put("ReqId", reqId);
                            mainObj.put("Comment", "re-order");
                            mainObj.put("Spid", ServiceProviderAdapter1.selectedSP);
                            mainObj.put("IsGlobal", false);
                            mainObj.put("TotalPrice", ServiceProviderAdapter1.total);
                            mainObj.put("GrandTotal", ServiceProviderAdapter1.grandtotal);

                            ServiceProviderAdapter1.total = 0;
                            ServiceProviderAdapter1.grandtotal = 0;
                            mainItem.put(mainObj);


                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                        try {
                            parent.put("UpdRequest", mainItem);
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                        Log.i("TAG", parent.toString());
                        mDialogCount = 0;
//                        timer.cancel();
//                        timer = new Timer();
//                        timer.schedule(new MyTimerTask(), 33000, 33000);
                        new UpdateOrder().execute(parent.toString());
                    }
                }
                else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderDetails.this, android.R.style.Theme_Material_Light_Dialog));
                    alertDialogBuilder.setTitle("ECO Clean");
                    alertDialogBuilder
                            .setMessage("Your request is already accepted by "+mSpName)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

//                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
//                    // ...Irrelevant code for customizing the buttons and title
//                    LayoutInflater inflater = getLayoutInflater();
//                    int layout = R.layout.alert_dialog;
//                    View dialogView = inflater.inflate(layout, null);
//                    dialogBuilder.setView(dialogView);
//                    dialogBuilder.setCancelable(false);
//
//                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                    View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//                    vert.setVisibility(View.GONE);
//                    no.setVisibility(View.GONE);
//                    yes.setText("Ok");
//                    desc.setText("Your request is already accepted by "+mSpName);
//
//                    yes.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            customDialog.dismiss();
//                        }
//                    });
//
//                    customDialog = dialogBuilder.create();
//                    customDialog.show();
//                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                    Window window = customDialog.getWindow();
//                    lp.copyFrom(window.getAttributes());
//                    //This makes the dialog take up the full width
//                    Display display = getWindowManager().getDefaultDisplay();
//                    Point size = new Point();
//                    display.getSize(size);
//                    int screenWidth = size.x;
//
//                    double d = screenWidth*0.85;
//                    lp.width = (int) d;
//                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                    window.setAttributes(lp);
                }
            }
        });

        filterDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDistance.setBackgroundResource(R.drawable.oval_shape);
                filterDistance.setTextColor(Color.parseColor("#FFFFFF"));
                filterRate.setBackgroundResource(R.drawable.round_rect);
                filterRate.setTextColor(Color.parseColor("#000000"));
                filterPrice.setBackgroundResource(R.drawable.round_rect);
                filterPrice.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.distanceSort);
                mAdapter.notifyDataSetChanged();
            }
        });

        filterRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterRate.setBackgroundResource(R.drawable.oval_shape);
                filterRate.setTextColor(Color.parseColor("#FFFFFF"));
                filterDistance.setBackgroundResource(R.drawable.round_rect);
                filterDistance.setTextColor(Color.parseColor("#000000"));
                filterPrice.setBackgroundResource(R.drawable.round_rect);
                filterPrice.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.ratingSort);
                mAdapter.notifyDataSetChanged();
            }
        });

        filterPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterPrice.setBackgroundResource(R.drawable.oval_shape);
                filterPrice.setTextColor(Color.parseColor("#FFFFFF"));
                filterRate.setBackgroundResource(R.drawable.round_rect);
                filterRate.setTextColor(Color.parseColor("#000000"));
                filterDistance.setBackgroundResource(R.drawable.round_rect);
                filterDistance.setTextColor(Color.parseColor("#000000"));
                Collections.sort(spList, ServiceProvider.priceSort);
                mAdapter.notifyDataSetChanged();
            }
        });

//        if(spList.size()>0) {
            alertDialog = dialogBuilder.create();
            alertDialog.show();
//        }

        if(spList.size()>0) {
            mAdapter = new ServiceProviderAdapter1(OrderDetails.this, spList, "En", selectedDate, selectedTime);
            spListView.setAdapter(mAdapter);
        }
        else{
            if(alertDialog!=null) {
                alertDialog.dismiss();
            }
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderDetails.this, android.R.style.Theme_Material_Light_Dialog));

            // set title
            alertDialogBuilder.setTitle("ECO Clean");

            // set dialog message
            alertDialogBuilder
                    .setMessage("No service providers in your area")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

//            AlertDialog.Builder dialogBuilder1 = new AlertDialog.Builder(OrderDetails.this);
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater1 = getLayoutInflater();
//            int layout1 = R.layout.alert_dialog;
//            View dialogView1 = inflater1.inflate(layout1, null);
//            dialogBuilder1.setView(dialogView1);
//            dialogBuilder1.setCancelable(false);
//
//            TextView desc = (TextView) dialogView1.findViewById(R.id.desc);
//            TextView yes = (TextView) dialogView1.findViewById(R.id.pos_btn);
//            TextView no = (TextView) dialogView1.findViewById(R.id.ngt_btn);
//            View vert = (View) dialogView1.findViewById(R.id.vert_line);
//
//            vert.setVisibility(View.GONE);
//            no.setVisibility(View.GONE);
//            yes.setText("Ok");
//            desc.setText("No service providers in your area");
//
//            yes.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    customDialog.dismiss();
//                }
//            });
//
//            customDialog = dialogBuilder.create();
//            customDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = customDialog.getWindow();
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }





    public void showDialog1(){
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.new_request_layout;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);


            Button yesBtn = (Button) dialogView.findViewById(R.id.yes_btn);
            Button noBtn = (Button) dialogView.findViewById(R.id.no_btn);

            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(alertDialog1!=null) {
                        alertDialog1.dismiss();
                    }
                    String status = orderPrefs.getString("order_status", "new");
                    if(status.equals("new")) {
//                        new UpdateLog().execute(Constants.UPDATE_LOG+reqId+"&spid="+spId);
                    showDialog();
                }
                else{
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderDetails.this, android.R.style.Theme_Material_Light_Dialog));
//                    alertDialogBuilder.setTitle("ECO Clean");
//                    alertDialogBuilder
//                            .setMessage("Your request is already accepted by "+mSpName)
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//                    alertDialog.show();

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        vert.setVisibility(View.GONE);
                        no.setVisibility(View.GONE);
                        yes.setText("Ok");
                        desc.setText("Your request is already accepted by "+mSpName);

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                }
                }
            });

            noBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialogCount=0;
                    alertDialog1.dismiss();
                }
            });


            alertDialog1 = dialogBuilder.create();
            alertDialog1.setCanceledOnTouchOutside(false);
            alertDialog1.show();

            //Grab the window of the dialog, and change the width
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = alertDialog1.getWindow();
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);

    }



    public void updateNotification(ToDo todo){
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date expDateTime = null;
        String date = "", time = "";
        try {
            expDateTime = formatter.parse(todo.getExpDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(expDateTime!= null) {
            time = timeFormat.format(expDateTime);
            date = dateFormat.format(expDateTime);
        }

        if(status.equalsIgnoreCase("cancel")){
            float old = Float.parseFloat(todo.getOldPenalty());
            cancellation_amt.setText(Math.round(old)+".00");
            service_amt.setText(todo.getTotalPrice()+".00");
            float rush_rate = Float.parseFloat(todo.getRushChargeValue());
            float r = Float.parseFloat(todo.getTotalPrice())*rush_rate;
            float rush_price = r - Float.parseFloat(todo.getTotalPrice());
            rush_amt.setText(Math.round(rush_price)+".00");
            float gT = Float.parseFloat(todo.getGrandTotal());
            price.setText(""+Math.round(old));
            net_total.setText("SAR "+Math.round(old)+".00");
            adjust_amt.setText("-"+Math.round(gT)+".00");
        }
        else {
            float gT = Float.parseFloat(todo.getGrandTotal());
            service_amt.setText(todo.getTotalPrice() +".00");
            float rush_rate = Float.parseFloat(todo.getRushChargeValue());
            float r = Float.parseFloat(todo.getTotalPrice()) * rush_rate;
            float rush_price = r - Float.parseFloat(todo.getTotalPrice());
            rush_amt.setText(Math.round(rush_price) +".00");
            float old = Float.parseFloat(todo.getOldPenalty());
            net_total.setText("SAR " + Math.round(gT+old)+".00");
            price.setText("" + Math.round(gT+old));
            if(todo.getRequestType().equalsIgnoreCase("GLOBAL")){
                adjust_amt.setText(Math.round(Float.parseFloat(todo.getAgreementPrice()))+".00");
                adjust.setText("Agreed Adjusted Amount");
            }
            else{
                adjust.setVisibility(View.GONE);
                adjust_amt.setVisibility(View.GONE);
            }
            if(old>0) {
                price_text.setVisibility(View.VISIBLE);
                cancellation_amt.setVisibility(View.VISIBLE);
                cancellation_amt.setText(Math.round(old)+".00");
            }
            else{
                price_text.setVisibility(View.GONE);
                cancellation_amt.setVisibility(View.GONE);
            }

        }

        rush_text.setText("Rush Hour Amount("+todo.getRushChargeValue()+"x)");
        address.setText(WordUtils.capitalizeFully(todo.getAddress()));
        dateTime.setText(date+"\n"+time);
        spName.setText(WordUtils.capitalizeFully(todo.getSpName()));
    }




    public boolean isWorkingTime(String stTime, String etTime){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat ExptimeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        String expDateString = selectedDate+" "+selectedTime;
        expDateString = expDateString.replace("-", "/");

        String startTime = stTime.replace(" ","");
        String endTime = etTime.replace(" ","");

        if (endTime.equals("12:00AM")) {
            endTime = "11:59PM";
        }

        Date serverDate = null;
        Date end24Date = null;
        Date start24Date = null;
        Date current24Date = null;
        Date dateToday = null;
        Calendar dateStoreClose = Calendar.getInstance();
        try {
            end24Date = timeFormat.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            start24Date = timeFormat.parse(startTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            serverDate = dateFormat.parse(expDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            dateToday = dateFormat1.parse(expDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dateStoreClose.setTime(dateToday);
        dateStoreClose.add(Calendar.DATE, 1);
        String current24 = timeFormat1.format(serverDate);
        String end24 =timeFormat1.format(end24Date);
        String start24 = timeFormat1.format(start24Date);
        String startDateString = dateFormat1.format(dateToday);
        String endDateString = dateFormat1.format(dateToday);
        String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
        dateStoreClose.add(Calendar.DATE, -2);
        String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

        Date startDate = null;
        Date endDate = null;

        try {
            end24Date = timeFormat1.parse(end24);
            start24Date = timeFormat1.parse(start24);
            current24Date = timeFormat1.parse(current24);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] parts2 = start24.split(":");
        int startHour = Integer.parseInt(parts2[0]);
        int startMinute = Integer.parseInt(parts2[1]);

        String[] parts = end24.split(":");
        int endHour = Integer.parseInt(parts[0]);
        int endMinute = Integer.parseInt(parts[1]);

        String[] parts1 = current24.split(":");
        int currentHour = Integer.parseInt(parts1[0]);
        int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


        if(startTime.contains("AM") && endTime.contains("AM")){
            if(startHour < endHour){
                startDateString = startDateString+ " "+ startTime;
                endDateString = endDateString+"  " + endTime;
                try {
                    startDate = dateFormat2.parse(startDateString);
                    endDate = dateFormat2.parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else if(startHour > endHour){
                if(expDateString.contains("AM")){
                    if(currentHour > endHour){
                        startDateString = startDateString + " " + startTime;
                        endDateString = endDateTomorrow + "  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        startDateString = endDateYesterday + " " + startTime;
                        endDateString = endDateString + "  " + endTime;
                        try {
                            startDate = dateFormat2.parse(startDateString);
                            endDate = dateFormat2.parse(endDateString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    startDateString = startDateString + " " + startTime;
                    endDateString = endDateTomorrow + "  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else if(startTime.contains("AM") && endTime.contains("PM")){
            startDateString = startDateString+ " "+ startTime;
            endDateString = endDateString+"  " + endTime;
            try {
                startDate = dateFormat2.parse(startDateString);
                endDate = dateFormat2.parse(endDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else if(startTime.contains("PM") && endTime.contains("AM")){
            if(expDateString.contains("AM")){
                if(currentHour <= endHour){
                    startDateString = endDateYesterday+ " "+ startTime;
                    endDateString = endDateString+"  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }else{
                    startDateString = startDateString+ " "+ startTime;
                    endDateString = endDateTomorrow+"  " + endTime;
                    try {
                        startDate = dateFormat2.parse(startDateString);
                        endDate = dateFormat2.parse(endDateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }else{

                startDateString = startDateString+ " "+ startTime;
                endDateString = endDateTomorrow+"  " + endTime;
                try {
                    startDate = dateFormat2.parse(startDateString);
                    endDate = dateFormat2.parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }else if(startTime.contains("PM") && endTime.contains("PM")){
            startDateString = startDateString+ " "+ startTime;
            endDateString = endDateString+"  " + endTime;
            try {
                startDate = dateFormat2.parse(startDateString);
                endDate = dateFormat2.parse(endDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        String serverDateString = dateFormat2.format(serverDate);

        try {
            serverDate = dateFormat2.parse(serverDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.i("TAG DATE" , ""+ startDate);
        Log.i("TAG DATE1" , ""+ endDate);
        Log.i("TAG DATE2" , ""+ serverDate);
        Log.i("TAG DATE2" , ""+ startTime);
        Log.i("TAG DATE2" , ""+ endTime);

        if(serverDate.after(startDate) && serverDate.before(endDate)) {
            Log.i("TAG Visible", "true");
            return true;
        }else {
            return false;
        }
    }




    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public void onBackPressed() {
        if(isBackPressed){
//            super.onBackPressed();
            if (fromScreen.equalsIgnoreCase("confirm") || (fromScreen.equalsIgnoreCase("history"))) {
                isBackPressed = true;
                finish();
            }
            else{
                Intent intent = new Intent(OrderDetails.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isClosed = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        isClosed = true;
        timer.cancel();
        timer1.cancel();
//        countDownTimer.cancel();
    }


    public void showDialog2(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.rating_dialog;
//        if(language.equalsIgnoreCase("En")){
//            layout = R.layout.insert_fav_order_dialog;
//        }else if(language.equalsIgnoreCase("Ar")){
//            layout = R.layout.insert_fav_order_dialog_arabic;
//        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        Button submit = (Button) dialogView.findViewById(R.id.submit_btn);
        final RatingBar rateBar = (RatingBar) dialogView.findViewById(R.id.ratingBar1);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rateBar.getRating() > 0) {
                    alertDialog2.dismiss();
                    JSONObject parent = new JSONObject();
                    try {

                        JSONArray mainItem = new JSONArray();

                        JSONObject mainObj = new JSONObject();
                        mainObj.put("UserId", userId);
                        mainObj.put("SpId", spId);
                        mainObj.put("Points", rateBar.getRating());
                        mainObj.put("SerReqId", reqId);
                        mainObj.put("Comments", "");

                        mainItem.put(mainObj);

                        parent.put("spRating", mainItem);
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    Log.i("TAG", "" + parent.toString());
                    new SubmitRating().execute(parent.toString());
                }else {

                }
            }
        });





        alertDialog2 = dialogBuilder.create();
        alertDialog2.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alertDialog2.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }



    public class SubmitRating extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        String response;
        ProgressDialog dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetails.this);
            dialog = ProgressDialog.show(OrderDetails.this, "",
                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.RATING_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MainActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);
                            Toast.makeText(OrderDetails.this, "Rating submitted successfully.", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(OrderDetails.this, MainActivity.class);
                            startActivity(i);
                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

//
//    public void slideUpDown(final RelativeLayout view) {
//        if (!isPanelShown()) {
//            // Show the panel
//            Animation bottomUp = AnimationUtils.loadAnimation(this,
//                    R.anim.bottom_up);
//
//            rateLayout.startAnimation(bottomUp);
//            rateLayout.setVisibility(View.VISIBLE);
//        }
//        else {
//            // Hide the Panel
//            Animation bottomDown = AnimationUtils.loadAnimation(this,
//                    R.anim.bottom_down);
//
//            rateLayout.startAnimation(bottomDown);
//            rateLayout.setVisibility(View.GONE);
//        }
//    }

    private boolean isPanelShown() {
        return rateLayout.getVisibility() == View.VISIBLE;
    }

    public class UpdateLog extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;

        String response;
        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetails.this);
            dialog = ProgressDialog.show(OrderDetails.this, "",
                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OrderDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                String s = jo.getString("Success");

                            }catch (JSONException je){
                                je.printStackTrace();
//                                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
//                Toast.makeText(MainActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
//            Intent i = new Intent(MainActivity.this, MainActivity.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(i);

            super.onPostExecute(result);
        }

    }


}
