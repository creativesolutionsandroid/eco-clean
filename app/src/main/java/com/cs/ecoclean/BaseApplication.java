package com.cs.ecoclean;

import android.app.Application;

import com.cs.ecoclean.widgets.FontsOverride;

/**
 * Created by CS on 31-10-2016.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FontsOverride.setDefaultFont(this, "SERIF", "Roboto-Regular.ttf");
    }
}
